import { LayoutActions, LayoutActionTypes } from './layout.actions';


export interface ILayoutState {
  isMobile: boolean;
  menuState: boolean;
}

export const initialState: ILayoutState = {
  isMobile: false,
  menuState: false
};

export function reducer(state = initialState, action: LayoutActions): ILayoutState {
  switch (action.type) {

    case LayoutActionTypes.DEVICE_SET_SUCCESS:
      return {
        ...state,
        isMobile: action.isMobile
      }
    case LayoutActionTypes.TOGGLE_MENU:
      return {
        ...state,
        menuState: !state.menuState
      }
    default:
      return state;
  }
}
