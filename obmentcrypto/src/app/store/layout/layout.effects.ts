import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DeviceSetSuccess, LayoutActionTypes, OpenMobileList } from './layout.actions';
import { map, switchMap } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatBottomSheet } from '@angular/material';
import { Store } from '@ngrx/store';
import { IAppState } from '../reducer';
import { MobileListComponent } from '../../components/mobile-list/mobile-list.component';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';

@Injectable()
export class LayoutEffects {

  mode: ECurrencyBlockTypes;

  @Effect()
  deviceSet$ = this.actions$.pipe(
    ofType(LayoutActionTypes.DEVICE_SET),
    switchMap(() => this.breakpointObserver.observe([Breakpoints.HandsetLandscape, Breakpoints.HandsetPortrait])),
    map(({matches}) => new DeviceSetSuccess(matches))
  );

  @Effect({dispatch: false})
  openMobileList$ = this.actions$.pipe(
    ofType<OpenMobileList>(LayoutActionTypes.OPEN_MOBILE_LIST),
    map(({mode}) => this.mode = mode),
    switchMap(() => {
      return this.bottomList.open(MobileListComponent, {
        data: {
          mode: this.mode
        }
      }).afterDismissed();
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<IAppState>,
    private breakpointObserver: BreakpointObserver,
    private bottomList: MatBottomSheet
  ) {
  }

}
