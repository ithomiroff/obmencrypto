import { Action } from '@ngrx/store';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';

export enum LayoutActionTypes {
  DEVICE_SET = '[Layout] DEVICE SET',
  DEVICE_SET_SUCCESS = '[Layout] DEVICE SET SUCCESS',

  OPEN_MOBILE_LIST = '[Layout] OPEN MOBILE LIST',

  TOGGLE_MENU = '[Layout] TOGGLE MENU',
}

export class DeviceSet implements Action {
  readonly type = LayoutActionTypes.DEVICE_SET;
}

export class DeviceSetSuccess implements Action {
  readonly type = LayoutActionTypes.DEVICE_SET_SUCCESS;
  constructor(public isMobile: boolean) {}
}

export class OpenMobileList implements Action {
  readonly type = LayoutActionTypes.OPEN_MOBILE_LIST;
  constructor(public mode: ECurrencyBlockTypes) {}
}

export class ToggleMenu implements Action {
  readonly type = LayoutActionTypes.TOGGLE_MENU;
}


export type LayoutActions =
  | DeviceSet
  | DeviceSetSuccess
  | OpenMobileList
  | ToggleMenu
  ;
