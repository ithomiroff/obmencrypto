import { IAppState } from '@app/store/reducer';
import { createSelector } from '@ngrx/store';

export const layout = (state: IAppState) => state.layout;

export const isMobile = createSelector(
  layout,
  (items) => items.isMobile
);


export const isMenuOpen = createSelector(
  layout,
  (items) => items.menuState
);
