import { Action } from '@ngrx/store';
import { IArticle, IArticleDescription } from '@app/core/interfaces/article.interface';

export enum ArticlesActionTypes {
  LoadArticle = '[Articles] Load Article',
  LoadArticle_SUCCESS = '[Articles] Load Article SUCCESS',
  LoadArticle_FAIL = '[Articles] Load Article FAIL',

  LoadArticleDescriptions = '[Articles] Load Article Descriptions',
  LoadArticleDescriptions_SUCCESS = '[Articles] Load Article Descriptions SUCCESS',
  LoadArticleDescriptions_FAIL = '[Articles] Load Article Descriptions FAIL',
}

export class LoadArticle implements Action {
  readonly type = ArticlesActionTypes.LoadArticle;

  constructor(public url: string) {}
}
export class LoadArticleSuccess implements Action {
  readonly type = ArticlesActionTypes.LoadArticle_SUCCESS;

  constructor(public article: IArticle) {}
}
export class LoadArticleFail implements Action {
  readonly type = ArticlesActionTypes.LoadArticle_FAIL;
}


export class LoadArticleDescriptions implements Action {
  readonly type = ArticlesActionTypes.LoadArticleDescriptions;

  constructor(public useFirst: boolean = false) {}
}
export class LoadArticleDescriptionsSuccess implements Action {
  readonly type = ArticlesActionTypes.LoadArticleDescriptions_SUCCESS;

  constructor(public descriptions: IArticleDescription[]) {}
}
export class LoadArticleDescriptionsFail implements Action {
  readonly type = ArticlesActionTypes.LoadArticleDescriptions_FAIL;
}


export type ArticlesActions =
  LoadArticle |
  LoadArticleSuccess |
  LoadArticleFail |

  LoadArticleDescriptions |
  LoadArticleDescriptionsFail |
  LoadArticleDescriptionsSuccess;
