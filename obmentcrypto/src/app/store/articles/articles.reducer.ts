import { Action } from '@ngrx/store';
import { IArticleDescription, IArticle } from '@app/core/interfaces/article.interface';
import { ArticlesActionTypes, ArticlesActions } from './articles.actions';


export const articlesFeatureKey = 'articles';

export interface ArticlesState {
  loading: boolean;
  descriptions: IArticleDescription[];
  currentArticle: IArticle;
}

const defaultArticle: IArticle = {
  urlKey: 'notfound',
  title: 'Страница не найдена',
  htmlText: '<h1>Страница не найдена</h1>'
};

export const initialState: ArticlesState = {
  loading : true,
  descriptions: [],
  currentArticle: defaultArticle
};

export function reducer(state = initialState, action: ArticlesActions): ArticlesState {
  switch (action.type) {
    case ArticlesActionTypes.LoadArticle:
      return {
        ...state,
        loading: true
      };
    case ArticlesActionTypes.LoadArticle_SUCCESS:
      return {
        ...state,
        loading: false,
        currentArticle: action.article
      };
    case ArticlesActionTypes.LoadArticle_FAIL:
      return {
        ...state,
        loading: false,
        currentArticle: defaultArticle
      };

    case ArticlesActionTypes.LoadArticleDescriptions_SUCCESS:
      return {
        ...state,
        descriptions: action.descriptions
      };

    case ArticlesActionTypes.LoadArticleDescriptions:
    case ArticlesActionTypes.LoadArticleDescriptions_FAIL:
    default:
      return state;
  }
}
