import { IAppState } from '../reducer';
import { createSelector } from '@ngrx/store';

const articles = (state: IAppState) => state.articles;


export const getArticleDescriptions = createSelector(
  articles,
  (state) => state.descriptions
);

export const isCurrentArticleLoading = createSelector(
  articles,
  (state) => state.loading
);

export const getCurrentArticle = createSelector(
  articles,
  (state) => state.currentArticle
);

export const getCurrentArticleHtmlText = createSelector(

  articles,
  (state) => state.currentArticle.htmlText
)
