import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { ArticlesActions, LoadArticleDescriptions, ArticlesActionTypes,
  LoadArticle, LoadArticleDescriptionsSuccess, LoadArticleDescriptionsFail, LoadArticleSuccess, LoadArticleFail } from './articles.actions';
import { switchMap, map, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpService } from '@app/core/services/http.service';
import { EApiUrls } from '@app/core/enums/api-urls.enum';
import { IArticleDescription, IArticle } from '@app/core/interfaces/article.interface';
import { Store } from '@ngrx/store';
import { IAppState } from '../reducer';
import { Router } from '@angular/router';

@Injectable()
export class ArticlesEffects {

  @Effect()
  loadArticleDescriptions$ = this.actions$.pipe(
    ofType<LoadArticleDescriptions>(ArticlesActionTypes.LoadArticleDescriptions),
    switchMap(({useFirst}) => {
      return this.http.get(EApiUrls.GET_ALL_ARTICLES)
        .pipe(
          map((articles: IArticleDescription[]) => {
            return new LoadArticleDescriptionsSuccess(articles);
          }),
          catchError((error) => of(new LoadArticleDescriptionsFail()))
        );
    })
  );

  @Effect()
  loadArticle$ = this.actions$.pipe(
    ofType<LoadArticle>(ArticlesActionTypes.LoadArticle),
    switchMap(({url}) => {
      return this.http.get(`${EApiUrls.GET_ARTICLE}/${url}`)
      .pipe(
        map((article: IArticle) => new LoadArticleSuccess(article)),
        catchError((error) => of(new LoadArticleFail()))
      );
    })
  );

  @Effect()
  loadArticleFail$ = this.actions$.pipe(
    ofType<LoadArticleFail>(ArticlesActionTypes.LoadArticle_FAIL),
    switchMap(() => {
      this.router.navigateByUrl('/notfound');
      return of();
    })
  );

  constructor(
    private actions$: Actions<ArticlesActions>,
    private store: Store<IAppState>,
    private router: Router,
    private http: HttpService) {}

}
