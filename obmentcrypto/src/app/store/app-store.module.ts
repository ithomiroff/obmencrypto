import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { appReducer } from '@app/store/reducer';
import { effects } from './effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [],
  providers: [ CookieService ],
  imports: [
    CommonModule,
    StoreModule.forRoot(appReducer),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({
      maxAge: 20,
      logOnly: !environment.production,
    }),
  ]
})
export class AppStoreModule { }
