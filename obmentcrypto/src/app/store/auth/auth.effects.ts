import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, delay, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import {
  AlivePassword,
  AlivePasswordFail,
  AlivePasswordSuccess,
  AuthActions,
  AuthActionTypes,
  ConfirmEmail,
  ConfirmEmailFail,
  ConfirmEmailSuccess,
  GetUserInfo,
  GetUserInfoFail,
  Logging,
  LoggingFail,
  LoggingSuccess,
  Logout,
  Registration,
  RegistrationFail,
  RegistrationSuccess,
  ResetPassword,
  ResetPasswordFail,
  ResetPasswordSuccess,
  SetCheckUserLoader,
  SetUser,
  LoadAntiforgery,
  LoadAntiforgerySuccess,
  LoadAntiforgeryFail,
  CheckSession
} from './auth.actions';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { HttpService } from '@app/core/services/http.service';
import { EApiUrls } from '@app/core/enums/api-urls.enum';
import { of, pipe } from 'rxjs';
import { Router } from '@angular/router';
import { EAppUrls } from '../../core/enums/app-urls.enum';
import { IAuthResponse } from '../../core/interfaces/auth-response.interface';
import { InfoSnackService } from '@app/core/services/error-snack.service';
import { MatDialog } from '@angular/material';
import { showDefaultModal } from '@app/core/utils/modal-configs';
import {IUser} from '../../core/interfaces/user.interface';
import { CookieService } from 'ngx-cookie-service';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';


@Injectable()
export class AuthEffects {

  @Effect()
  checkSession$ = this.actions$.pipe(
    ofType(AuthActionTypes.CHECK_SESSION),
    map(() => {
      if (isPlatformBrowser(this.platformId)) {
        const token = localStorage.getItem(this.tokenName);
        if (token) {
          return new GetUserInfo(token);
        }
      }
      return new SetUser(null);
    })
  );

  @Effect()
  getUserInfo$ = this.actions$.pipe(
    ofType<GetUserInfo>(AuthActionTypes.GET_USER_INFO),
    switchMap(() => {
      return this.http.get(EApiUrls.GET_USER_INFO).pipe(
        mergeMap((response: IUser) => [
          new SetUser(response)
        ]),
        catchError(() => of(new GetUserInfoFail()))
      );
    })
  );

  @Effect()
  registration$ = this.actions$.pipe(
    ofType<Registration>(AuthActionTypes.REGISTRATION),
    switchMap(({regData}) => {
      return this.http.post(EApiUrls.REG, regData).pipe(
        map(() => new RegistrationSuccess()),
        catchError((err) => {
          this.errorSnack.showError(err);
          return of(new RegistrationFail(err));
        })
      );
    })
  );

  @Effect()
  logging$ = this.actions$.pipe(
    ofType<Logging>(AuthActionTypes.LOGGING),
    switchMap(({logginData}) => {
      console.log('logging');
      const staticParam = {
        grant_type: 'password'
      };
      return this.http.postForm(EApiUrls.AUTH, {
        ...logginData,
        ...staticParam
      }).pipe(
        mergeMap((response: IAuthResponse) => {
          localStorage.setItem(this.tokenName, response.token);
          return [
            new LoggingSuccess(response),
          ];
        }),
        catchError((err) => {
          this.errorSnack.showError(err);
          return of(new LoggingFail());
        })
      );
    })
  );

  @Effect({dispatch: false})
  loggingSuccess$ = this.actions$.pipe(
    ofType<LoggingSuccess>(AuthActionTypes.LOGGING_SUCCESS),
    map(() => {
      this.store.dispatch(new LoadAntiforgery());
      this.router.navigateByUrl(EAppUrls.USER + '/' + EAppUrls.CABINET);
    })
  );

  @Effect({dispatch: false})
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.LOGOUT),
    tap(() => this.store.dispatch(new SetCheckUserLoader(true))),
    delay(800),
    switchMap(() => {
      return this.http.post(EApiUrls.ACCOUNT + EApiUrls.LOGOUT, {}).pipe(
        map(() => {
          this.store.dispatch(new LoadAntiforgery());
          this.store.dispatch(new SetCheckUserLoader(false));
          localStorage.removeItem(this.tokenName);
          this.router.navigateByUrl('/');
        })
      );
    }),
  );

  @Effect()
  alivePassword$ = this.actions$.pipe(
    ofType<AlivePassword>(AuthActionTypes.ALIVE_PASSWORD),
    switchMap(({email}) => {
      return this.http.post(EApiUrls.ACCOUNT + EApiUrls.FORGOT_PASS, {email}).pipe(
        switchMap((response) => {
          return showDefaultModal(this.dialog, {
            title: 'Восстановление пароля',
            message: response.message,
          }).pipe(map(() => this.router.navigateByUrl(`/${EAppUrls.USER}/${EAppUrls.AUTH}`)));
        }),
        catchError((err) => {
          this.errorSnack.showError(err);
          return of(new AlivePasswordFail());
        })
      );
    }),
    map(() => new AlivePasswordSuccess())
  );


  @Effect()
  resetPassword$ = this.actions$.pipe(
    ofType<ResetPassword>(AuthActionTypes.RESET_PASSWORD),
    switchMap((action) => {
      return this.http.post(EApiUrls.ACCOUNT + EApiUrls.RESET_PASS, action.data).pipe(
        switchMap((response) => showDefaultModal(this.dialog, {
          title: 'Восстановление пароля',
          message: response,
        }).pipe(map(() => {
          this.router.navigateByUrl(`/${EAppUrls.USER}/${EAppUrls.AUTH}`);
          return new ResetPasswordSuccess();
        }))),
        catchError((err) => {
          this.errorSnack.showError(err);
          return of(new ResetPasswordFail());
        })
      );
    })
  );

  // @Effect()
  // resetPasswordSuccess$ = this.actions$.pipe(
  //   ofType<ResetPasswordSuccess>(AuthActionTypes.RESET_PASSWORD_SUCCESS),
  //   switchMap(() => {
  //     return ;
  //   })
  // );

  @Effect()
  confirmEmail$ = this.actions$.pipe(
    ofType<ConfirmEmail>(AuthActionTypes.CONFIRM_EMAIL),
    switchMap(({data}) => {
      return this.http.get(`${EApiUrls.ACCOUNT}${EApiUrls.CONFIRM_EMAIL}?userId=${data.userId}&token=${data.code}`).pipe(
        map(() => new ConfirmEmailSuccess()),
        catchError((err) => of(new ConfirmEmailFail(err)))
      );
    })
  );

  @Effect({dispatch: false})
  confirmSuccess$ = this.actions$.pipe(
    ofType<ConfirmEmailSuccess>(AuthActionTypes.CONFIRM_EMAIL_SUCCESS),
    switchMap(() => {
      return showDefaultModal(this.dialog, {
        title: 'Email подтверждён',
        message: 'Вы успешно подтвердили email',
      }).pipe(
        map(() => this.router.navigateByUrl(EAppUrls.USER + '/' + EAppUrls.AUTH))
      );
    })
  );

  @Effect({dispatch: false})
  confirmFail$ = this.actions$.pipe(
    ofType<ConfirmEmailFail>(AuthActionTypes.CONFIRM_EMAIL_FAIL),
    switchMap(({error}) => {
      const message = error && error.error ? error.error.errorDescription : 'Ошибка сервера';
      this.errorSnack.showError(error);
      return showDefaultModal(this.dialog, {
        title: 'Подтверждение пароля',
        message,
      }).pipe(
        map(() => this.router.navigateByUrl('/'))
      );
    })
  );


  @Effect()
  loadAntiforgery$ = this.actions$.pipe(
    ofType<LoadAntiforgery>(AuthActionTypes.LOAD_ANTIFORGERY),
    switchMap(() => {
      return this.http.get(EApiUrls.CHECK_SESSION).pipe(
        mergeMap(() => [
          new LoadAntiforgerySuccess(),
          new CheckSession()
        ]),
        catchError((err) => of(new LoadAntiforgeryFail(err)))
      );
    })
  );

  @Effect({dispatch: false})
  loadAntiforgeryFail$ = this.actions$.pipe(
    ofType<LoadAntiforgeryFail>(AuthActionTypes.LOAD_ANTIFORGERY_FAIL),
    switchMap(({error}) => {
      console.log('load antiforgery error');
      console.log(error);
      const message = error && error.error ? error.error.errorDescription : 'Ошибка сервера';
      this.errorSnack.showError(error);
      return showDefaultModal(this.dialog, {
        title: 'Ошибка.',
        message,
      });
    })
  );
  tokenName = 'cryptoToken';

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private actions$: Actions<AuthActions>,
    private store: Store<IAppState>,
    private http: HttpService,
    private router: Router,
    private errorSnack: InfoSnackService,
    private dialog: MatDialog
  ) {
  }

}
