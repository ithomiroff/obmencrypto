import { AuthActions, AuthActionTypes } from './auth.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { IUser } from '../../core/interfaces/user.interface';

export interface IAuthState {
  isRegister: boolean;
  token: string;
  user: IUser;
  loadingLogin: boolean;
  loadingCheckUser: boolean;
  loadingReset: boolean;
  loadingAntiforgery: boolean;
  registrationError: HttpErrorResponse;
}

export const initialState: IAuthState = {
  isRegister: null,
  token: null,
  user: null,
  loadingLogin: false,
  loadingCheckUser: false,
  loadingAntiforgery: true,
  registrationError: null,
  loadingReset: false
};

export function reducer(state = initialState, action: AuthActions): IAuthState {
  switch (action.type) {
    case AuthActionTypes.REGISTRATION:
      return {
        ...state,
        loadingLogin: true,
        registrationError: null
      };
    case AuthActionTypes.REGISTRATION_FAIL:
      return {
        ...state,
        loadingLogin: false,
        isRegister: false,
        registrationError: action.error
      };

    case AuthActionTypes.REGISTRATION_SUCCESS:
      return {
        ...state,
        loadingLogin: false,
        isRegister: true
      };

    case AuthActionTypes.SET_TOKEN:
      return {
        ...state,
        token: action.token,
        loadingCheckUser: false
      };
    case AuthActionTypes.SET_USER:
      return {
        ...state,
        user: action.user,
        loadingCheckUser: false
      };

    case AuthActionTypes.LOGGING:
      return {
        ...state,
        loadingLogin: true
      };
    case AuthActionTypes.LOGGING_SUCCESS:
      return {
        ...state,
        loadingLogin: false,
        user: action.user
      };
    case AuthActionTypes.LOGGING_FAIL:
      return {
        ...state,
        loadingLogin: false
      };
    case AuthActionTypes.GET_USER_INFO:
      return {
        ...state,
        loadingCheckUser: true
      };

    case AuthActionTypes.GET_USER_INFO_FAIL:
      return {
        ...state,
        loadingCheckUser: false
      };

    case AuthActionTypes.SET_CHECK_USER_LOADER:
      return {
        ...state,
        loadingCheckUser: action.value
      };

    case AuthActionTypes.LOGOUT:
      return {
        ...state,
        user: null,
        token: null
      };

    case AuthActionTypes.RESET_PASSWORD:
    case AuthActionTypes.ALIVE_PASSWORD:
      return {
        ...state,
        loadingReset: true
      };

    case AuthActionTypes.RESET_PASSWORD_SUCCESS:
    case AuthActionTypes.RESET_PASSWORD_FAIL:
    case AuthActionTypes.ALIVE_PASSWORD_SUCCESS:
    case AuthActionTypes.ALIVE_PASSWORD_FAIL:
      return {
        ...state,
        loadingReset: false
      };

    case AuthActionTypes.LOAD_ANTIFORGERY:
      return {
        ...state,
        loadingAntiforgery: true
      };
    case AuthActionTypes.LOAD_ANTIFORGERY_SUCCESS:
    case AuthActionTypes.LOAD_ANTIFORGERY_FAIL:
      return {
        ...state,
        loadingAntiforgery: false
      };
    default:
      return state;
  }
}
