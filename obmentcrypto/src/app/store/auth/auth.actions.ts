import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {IUser} from '../../core/interfaces/user.interface';

export enum AuthActionTypes {
  SET_TOKEN = '[Auth] SET TOKEN',
  SET_USER = '[Auth] SET USER',

  CHECK_SESSION = '[Auth] CHECK SESSION',

  GET_USER_INFO = '[Auth] GET USER INFO',
  GET_USER_INFO_FAIL = '[Auth] GET USER INFO FAIL',

  LOGGING = '[Auth] LOGGING',
  LOGGING_SUCCESS = '[Auth] LOGGING SUCCESS',
  LOGGING_FAIL = '[Auth] LOGGING FAIL',

  REGISTRATION = '[Auth] REGISTRATION',
  REGISTRATION_SUCCESS = '[Auth] REGISTRATION SUCCESS',
  REGISTRATION_FAIL = '[Auth] REGISTRATION FAIL',


  LOGOUT = '[Auth] LOGOUT',

  SET_CHECK_USER_LOADER = '[Auth] SET CHECK USER LOADER',

  ALIVE_PASSWORD = '[Auth] ALIVE PASSWORD',
  ALIVE_PASSWORD_SUCCESS = '[Auth] ALIVE PASSWORD SUCCESS',
  ALIVE_PASSWORD_FAIL = '[Auth] ALIVE PASSWORD FAIL',

  RESET_PASSWORD = '[Auth] RESET PASSWORD',
  RESET_PASSWORD_SUCCESS = '[Auth] RESET PASSWORD SUCCESS',
  RESET_PASSWORD_FAIL = '[Auth] RESET PASSWORD FAIL',

  CONFIRM_EMAIL = '[Auth] CONFIRM EMAIL',
  CONFIRM_EMAIL_SUCCESS = '[Auth] CONFIRM EMAIL SUCCESS',
  CONFIRM_EMAIL_FAIL = '[Auth] CONFIRM EMAIL FAIL',

  LOAD_ANTIFORGERY = '[Auth] LOAD ANTIFORGERY',
  LOAD_ANTIFORGERY_SUCCESS = '[Auth] LOAD ANTIFORGERY SUCCESS',
  LOAD_ANTIFORGERY_FAIL = '[Auth] LOAD ANTIFORGERY FAIL'
}

export class Registration implements Action {
  readonly type = AuthActionTypes.REGISTRATION;

  constructor(
    public regData: {
      username: string,
      email: string,
      password: string,
      passwordConfirm: string,
      phone: string,
      telegram: string,
    }
  ) {
  }
}

export class RegistrationSuccess implements Action {
  readonly type = AuthActionTypes.REGISTRATION_SUCCESS;
}

export class RegistrationFail implements Action {
  readonly type = AuthActionTypes.REGISTRATION_FAIL;
  constructor(public error: HttpErrorResponse) {}
}

export class Logging implements Action {
  readonly type = AuthActionTypes.LOGGING;

  constructor(public logginData: { email: string; password: string }) {
  }
}

export class LoggingSuccess implements Action {
  readonly type = AuthActionTypes.LOGGING_SUCCESS;

  constructor(
    public user: IUser
  ) {
  }
}

export class LoggingFail implements Action {
  readonly type = AuthActionTypes.LOGGING_FAIL;
}

export class SetToken implements Action {
  readonly type = AuthActionTypes.SET_TOKEN;

  constructor(public token: string) {
  }
}

export class SetUser implements Action {
  readonly type = AuthActionTypes.SET_USER;

  constructor(
    public user: IUser
  ) {
  }
}

export class CheckSession implements Action {
  readonly type = AuthActionTypes.CHECK_SESSION;
}

export class GetUserInfo implements Action {
  readonly type = AuthActionTypes.GET_USER_INFO;

  constructor(public token: string) {
  }
}

export class GetUserInfoFail implements Action {
  readonly type = AuthActionTypes.GET_USER_INFO_FAIL;
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class SetCheckUserLoader implements Action {
  readonly type = AuthActionTypes.SET_CHECK_USER_LOADER;

  constructor(public value: boolean) {
  }
}

export class AlivePassword implements Action {
  readonly type = AuthActionTypes.ALIVE_PASSWORD;

  constructor(
    public email: string
  ) {
  }
}

export class AlivePasswordSuccess implements Action {
  readonly type = AuthActionTypes.ALIVE_PASSWORD_SUCCESS;
}

export class AlivePasswordFail implements Action {
  readonly type = AuthActionTypes.ALIVE_PASSWORD_FAIL;
}

export class ResetPassword implements Action {
  readonly type = AuthActionTypes.RESET_PASSWORD;

  constructor(
    public data: {
      code: string;
      email: string;
      password: string;
      confirmPassword: string;
    }
  ) {
  }
}

export class ResetPasswordSuccess implements Action {
  readonly type = AuthActionTypes.RESET_PASSWORD_SUCCESS;
}


export class ResetPasswordFail implements Action {
  readonly type = AuthActionTypes.RESET_PASSWORD_FAIL;
}

export class ConfirmEmail implements Action {
  readonly type = AuthActionTypes.CONFIRM_EMAIL;

  constructor(
    public data: {
      code: string;
      userId: string;
    }
  ) {
  }
}

export class ConfirmEmailSuccess implements Action {
  readonly type = AuthActionTypes.CONFIRM_EMAIL_SUCCESS;
}


export class ConfirmEmailFail implements Action {
  readonly type = AuthActionTypes.CONFIRM_EMAIL_FAIL;

  constructor(
    public error: HttpErrorResponse
  ) {
  }
}

export class LoadAntiforgery implements Action {
  readonly type = AuthActionTypes.LOAD_ANTIFORGERY;

  constructor() {}
}

export class LoadAntiforgerySuccess implements Action {
  readonly type = AuthActionTypes.LOAD_ANTIFORGERY_SUCCESS;

  constructor() {}
}

export class LoadAntiforgeryFail implements Action {
  readonly type = AuthActionTypes.LOAD_ANTIFORGERY_FAIL;

  constructor(
    public error: HttpErrorResponse
    ) {}
}

export type AuthActions =
  | SetToken
  | SetUser
  | SetCheckUserLoader

  | CheckSession

  | GetUserInfo
  | GetUserInfoFail

  | Logging
  | LoggingSuccess
  | LoggingFail

  | Registration
  | RegistrationSuccess
  | RegistrationFail

  | Logout

  | AlivePassword
  | AlivePasswordSuccess
  | AlivePasswordFail

  | ResetPassword
  | ResetPasswordSuccess
  | ResetPasswordFail

  | ConfirmEmail
  | ConfirmEmailSuccess
  | ConfirmEmailFail

  | LoadAntiforgery
  | LoadAntiforgerySuccess
  | LoadAntiforgeryFail
  ;
