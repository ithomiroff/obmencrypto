import { IAppState } from '../reducer';
import { createSelector } from '@ngrx/store';

const auth = (state: IAppState) => state.auth;

export const getAntiforgeryLoading = createSelector(
  auth,
  (state) => state.loadingAntiforgery
);

export const getLoadingLogin = createSelector(
  auth,
  (state) => state.loadingLogin
);

export const getAuthUser = createSelector(
  auth,
  (state) => state.user
);

export const getToken = createSelector(
  auth,
  (state) => state.token
);

export const getMainLoading = createSelector(
  auth,
  (state) => state.loadingCheckUser
);

export const getRegisterStatus = createSelector(
  auth,
  (state) => state.isRegister
);

export const isUserLoggedIn = createSelector(
  auth,
  (state) => {
    if (!state.user) {
      return false;
    }
    const {username, email} = state.user;
    return Boolean(username && email);
  }
);

export const getLoadingReset = createSelector(
  auth,
  (state) => state.loadingReset
);
