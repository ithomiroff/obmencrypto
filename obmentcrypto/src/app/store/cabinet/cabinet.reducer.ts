import { CabinetActions, CabinetActionTypes } from './cabinet.actions';
import { IUserApplication } from '../../core/interfaces/cabinet/user-application.interface';
import { IAffiliateInfo } from '../../core/interfaces/cabinet/affiliate-info.interface';
import { IWithdrawalApplication } from '../../core/interfaces/cabinet/withdrawal-application.interface';

export interface ICabinetState {
  userApplications: IUserApplication[];
  myApplicationsLoading: boolean;

  affiliateApplications: IUserApplication[];
  affiliateInfo: IAffiliateInfo;
  affiliateLoading: boolean;

  withdrawalApplication: IWithdrawalApplication[];

  loading: boolean;
  passwordChanged: boolean;
}

export const initialState: ICabinetState = {
  userApplications: [],
  myApplicationsLoading: false,

  affiliateApplications: [],
  affiliateInfo: null,
  affiliateLoading: false,

  withdrawalApplication: [],

  loading: false,
  passwordChanged: false,
};

export function reducer(state = initialState, action: CabinetActions): ICabinetState {
  switch (action.type) {

    case CabinetActionTypes.LOAD_APPLICATIONS:
      return {
        ...state,
        myApplicationsLoading: true
      };
    case CabinetActionTypes.LOAD_APPLICATIONS_FAIL:
      return {
        ...state,
        myApplicationsLoading: false
      };
    case CabinetActionTypes.LOAD_APPLICATIONS_SUCCESS:
      return {
        ...state,
        userApplications: action.apps,
        myApplicationsLoading: false
      };


    case CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE:
      return {
        ...state,
        affiliateLoading: true
      };
    case CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE_FAIL:
      return {
        ...state,
        affiliateLoading: false
      };
    case CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE_SUCCESS:
      return {
        ...state,
        affiliateApplications: action.apps,
        affiliateLoading: false
      };


    case CabinetActionTypes.LOAD_AFFILIATE_INFO_SUCCESS:
      return {
        ...state,
        affiliateInfo: action.info
      };

    case CabinetActionTypes.LOAD_WITHDRAWAL_APPLICATIONS_SUCCESS:
      return {
        ...state,
        withdrawalApplication: action.apps
      };


    case CabinetActionTypes.CHANGE_USER_DATA:
      return {
        ...state,
        loading: true
      };

    case CabinetActionTypes.CHANGE_USER_DATA_FAIL:
      return {
        ...state,
        loading: false
      };

    case CabinetActionTypes.CHANGE_USER_DATA_SUCCESS:
      return {
        ...state,
        loading: false
      };


    case CabinetActionTypes.CHANGE_PASSWORD:
      return {
        ...state,
        loading: true,
      };

    case CabinetActionTypes.CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        loading: false
      };

    case CabinetActionTypes.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        passwordChanged: true
      };
    default:
      return state;
  }
}
