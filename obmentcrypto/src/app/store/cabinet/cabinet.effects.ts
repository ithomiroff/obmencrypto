import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, switchMap, mergeMap, tap } from 'rxjs/operators';
import { of, pipe } from 'rxjs';
import {
  CabinetActions,
  CabinetActionTypes,
  ChangePassword,
  ChangePasswordFail,
  ChangePasswordSuccess,
  LoadAffiliateInfo,
  LoadAffiliateInfoFail,
  LoadAffiliateInfoSuccess,
  LoadApplications,
  LoadApplicationsAffiliate,
  LoadApplicationsAffiliateFail,
  LoadApplicationsAffiliateSuccess,
  LoadApplicationsFail,
  LoadApplicationsSuccess,
  LoadWithdrawalApplications,
  LoadWithdrawalApplicationsFail,
  LoadWithdrawalApplicationsSuccess,
  ChangeUserDataFailed,
  ChangeUserData,
  ChangeUserDataSuccess,
  CreateWithdrawal,
  CreateWithdrawalFail,
  CreateWithdrawalSuccess
} from './cabinet.actions';
import { HttpService } from '../../core/services/http.service';
import { EApiUrls } from '../../core/enums/api-urls.enum';
import { IUserApplication } from '../../core/interfaces/cabinet/user-application.interface';
import { IAffiliateInfo } from '../../core/interfaces/cabinet/affiliate-info.interface';
import { IWithdrawalApplication } from '../../core/interfaces/cabinet/withdrawal-application.interface';
import { Router } from '@angular/router';
import { InfoSnackService } from '@app/core/services/error-snack.service';
import * as authActions from '../auth/auth.actions';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { showDefaultModal } from '@app/core/utils/modal-configs';
import { MatDialog } from '@angular/material';
const succesChangePasswordMsg = 'Вы изменили пароль. Для пользования кабинетом, пожалуйста, авторизируйтесь с новым паролем.';

@Injectable()
export class CabinetEffects {

  @Effect()
  loadApplications$ = this.actions$.pipe(
    ofType<LoadApplications>(CabinetActionTypes.LOAD_APPLICATIONS),
    switchMap(() => {
      return this.http.get(EApiUrls.USER_APPLICATIONS).pipe(
        map((res: IUserApplication[]) => new LoadApplicationsSuccess(res)),
        catchError(() => of(new LoadApplicationsFail()))
      );
    })
  );

  @Effect()
  loadApplicationsAffiliate$ = this.actions$.pipe(
    ofType<LoadApplicationsAffiliate>(CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE),
    switchMap(() => {
      return this.http.get(EApiUrls.USER_APPLICATIONS + EApiUrls.AFFILIATE).pipe(
        map((res: IUserApplication[]) => new LoadApplicationsAffiliateSuccess(res)),
        catchError(() => of(new LoadApplicationsAffiliateFail()))
      );
    })
  );

  @Effect()
  loadAffiliateInfo$ = this.actions$.pipe(
    ofType<LoadAffiliateInfo>(CabinetActionTypes.LOAD_AFFILIATE_INFO),
    switchMap(() => {
      return this.http.get(EApiUrls.INFO + EApiUrls.AFFILIATE).pipe(
        map((res: IAffiliateInfo) => new LoadAffiliateInfoSuccess(res)),
        catchError(() => of(new LoadAffiliateInfoFail()))
      );
    })
  );

  @Effect()
  loadWithdrawalApplications$ = this.actions$.pipe(
    ofType<LoadWithdrawalApplications>(CabinetActionTypes.LOAD_WITHDRAWAL_APPLICATIONS),
    switchMap(() => {
      return this.http.get(EApiUrls.USER_APPLICATIONS + EApiUrls.WITHDRAWAL).pipe(
        map((res: IWithdrawalApplication[]) => new LoadWithdrawalApplicationsSuccess(res)),
        catchError(() => of(new LoadWithdrawalApplicationsFail()))
      );
    })
  );

  @Effect()
  changePassword$ = this.actions$.pipe(
    ofType<ChangePassword>(CabinetActionTypes.CHANGE_PASSWORD),
    switchMap(({data}) => {
      return this.http.post(EApiUrls.ACCOUNT + EApiUrls.CHANGE_PASS, data).pipe(
        map(() => new ChangePasswordSuccess()),
        catchError((error) => of(new ChangePasswordFail(error)))
      );
    })
  );

  @Effect()
  changePasswordSuccess$ = this.actions$.pipe(
    ofType<ChangePasswordSuccess>(CabinetActionTypes.CHANGE_PASSWORD_SUCCESS),
    switchMap(() => {
      return showDefaultModal(this.dialog, {
        title: 'Смена пароля',
        message: 'Вы успешно изменили пароль.',
      });
    })
  );

  @Effect({dispatch: false})
  changePasswordFail$ = this.actions$.pipe(
    ofType<ChangePasswordFail>(CabinetActionTypes.CHANGE_PASSWORD_FAIL),
    map(({error}) => this.infoSnackService.showError(error))
  );

  @Effect()
  changeUserData$ = this.actions$.pipe(
    ofType<ChangeUserData>(CabinetActionTypes.CHANGE_USER_DATA),
    switchMap(({user}) => {
      return this.http.post(EApiUrls.ACCOUNT + EApiUrls.GET_USER_INFO, user).pipe(
        mergeMap(() => [
          new authActions.SetUser(user),
          new ChangeUserDataSuccess()
        ]),
        catchError((error) => of(new ChangeUserDataFailed(error)))
      );
    })
  );

  @Effect()
  changeUserDataSuccess$ = this.actions$.pipe(
    ofType<ChangeUserDataSuccess>(CabinetActionTypes.CHANGE_USER_DATA_SUCCESS),
    switchMap(() => {
      return showDefaultModal(this.dialog, {
        title: 'Настройки',
        message: 'Вы успешно обновили данные',
      }).pipe(map(() => new authActions.CheckSession()));
    })
  );

  @Effect({dispatch: false})
  changeUserDataFailed$ = this.actions$.pipe(
    ofType<ChangeUserDataFailed>(CabinetActionTypes.CHANGE_USER_DATA_FAIL),
    map(({error}) => this.infoSnackService.showError(error))
  );


  @Effect()
  createWithdrawalApplication$ = this.actions$.pipe(
    ofType<CreateWithdrawal>(CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION),
    switchMap((action) => {
      return this.http.post(EApiUrls.CREATE_WITHDRAWAL, { wallet: action.wallet}).pipe(
        map(() => new CreateWithdrawalSuccess()),
        catchError((error) => of(new ChangeUserDataFailed(error)))
      );
    })
  );

  @Effect()
  createWithdrawalSuccess$ = this.actions$.pipe(
    ofType<CreateWithdrawalSuccess>(CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION_SUCCESS),
    switchMap(() => {
      return showDefaultModal(this.dialog, {
        title: 'Заявка на вывод',
        message: 'Заявка на вывод успешно создана.',
      }).pipe(() => of(new LoadWithdrawalApplications()));
    })
  );

  @Effect({dispatch: false})
  createWithdrawalFail$ = this.actions$.pipe(
    ofType<CreateWithdrawalFail>(CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION_FAIL),
    map(({error}) => this.infoSnackService.showError(error))
  );

  constructor(
    private actions$: Actions<CabinetActions>,
    private http: HttpService,
    private router: Router,
    private infoSnackService: InfoSnackService,
    private dialog: MatDialog
  ) {
  }

}
