import { IAppState } from '../reducer';
import { createSelector } from '@ngrx/store';

const cabinet = (state: IAppState) => state.cabinet;

export const userApplications = createSelector(
  cabinet,
  (state) => state.userApplications
);

export const affiliateUserApplications = createSelector(
  cabinet,
  (state) => state.affiliateApplications
);

export const lockCreateWithdrawal = createSelector(
  cabinet,
  (state) => state.affiliateInfo ? state.affiliateInfo.availableAffiliate.value == 0 : true
);

export const personLink = createSelector(
  cabinet,
  (state) => state.affiliateInfo ? state.affiliateInfo.personLink : null
);
export const partnerText = createSelector(
  cabinet,
  (state) => state.affiliateInfo ? state.affiliateInfo.partnerText : null
);

export const affiliateWithdral = createSelector(
  cabinet,
  (state) => state.affiliateInfo ? state.affiliateInfo.availableAffiliate : null
);

export const withdrawalApplication = createSelector(
  cabinet,
  (state) => state.withdrawalApplication
);



export const loadingMyApplications = createSelector(
  cabinet,
  (state) => state.myApplicationsLoading
);

export const loadingAffiliate = createSelector(
  cabinet,
  (state) => state.affiliateLoading
);

export const getLoading = createSelector(
  cabinet,
  (state) => state.loading
);

