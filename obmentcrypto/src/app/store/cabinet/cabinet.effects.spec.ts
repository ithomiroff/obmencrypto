import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CabinetEffects } from './cabinet.effects';

describe('CabinetEffects', () => {
  let actions$: Observable<any>;
  let effects: CabinetEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CabinetEffects,
        provideMockActions(() => actions$)
      ]
    });

    // effects = TestBed.get<CabinetEffects>(CabinetEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
