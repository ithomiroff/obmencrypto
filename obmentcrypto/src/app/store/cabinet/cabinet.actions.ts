import { Action } from '@ngrx/store';
import { IUserApplication } from '../../core/interfaces/cabinet/user-application.interface';
import { IAffiliateInfo } from '../../core/interfaces/cabinet/affiliate-info.interface';
import { IWithdrawalApplication } from '../../core/interfaces/cabinet/withdrawal-application.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { IUser } from '@app/core/interfaces/user.interface';

export enum CabinetActionTypes {
  LOAD_APPLICATIONS = '[Cabinet] LOAD APPLICATIONS',
  LOAD_APPLICATIONS_SUCCESS = '[Cabinet] LOAD APPLICATIONS SUCCESS',
  LOAD_APPLICATIONS_FAIL = '[Cabinet] LOAD APPLICATIONS FAIL',

  LOAD_APPLICATIONS_AFFILIATE = '[Cabinet] LOAD APPLICATIONS AFFILIATE',
  LOAD_APPLICATIONS_AFFILIATE_SUCCESS = '[Cabinet] LOAD APPLICATIONS AFFILIATE SUCCESS',
  LOAD_APPLICATIONS_AFFILIATE_FAIL = '[Cabinet] LOAD APPLICATIONS AFFILIATE FAIL',

  LOAD_AFFILIATE_INFO = '[Cabinet] LOAD AFFILIATE INFO',
  LOAD_AFFILIATE_INFO_SUCCESS = '[Cabinet] LOAD AFFILIATE INFO SUCCESS',
  LOAD_AFFILIATE_INFO_FAIL = '[Cabinet] LOAD AFFILIATE INFO FAIL',

  LOAD_WITHDRAWAL_APPLICATIONS = '[Cabinet] LOAD WITHDRAWAL APPLICATIONS',
  LOAD_WITHDRAWAL_APPLICATIONS_SUCCESS = '[Cabinet] LOAD WITHDRAWAL APPLICATIONS SUCCESS',
  LOAD_WITHDRAWAL_APPLICATIONS_FAIL = '[Cabinet] LOAD WITHDRAWAL APPLICATIONS FAIL',

  CHANGE_PASSWORD = '[Cabinet] CHANGE PASSWORD',
  CHANGE_PASSWORD_SUCCESS = '[Cabinet] CHANGE PASSWORD SUCCESS',
  CHANGE_PASSWORD_FAIL = '[Cabinet] CHANGE PASSWORD FAIL',

  CHANGE_USER_DATA = '[Cabinet] СHANGE USER DATA',
  CHANGE_USER_DATA_SUCCESS = '[Cabinet] CHANGE USER DATA SUCCESS',
  CHANGE_USER_DATA_FAIL = '[Cabinet] CHANGE USER DATA FAIL',

  CREATE_WITHDRAWAL_APPLICATION = '[Cabinet] CREATE WITHDRAWAL APPLICATION',
  CREATE_WITHDRAWAL_APPLICATION_SUCCESS = '[Cabinet] CREATE WITHDRAWAL APPLICATION SUCCESS',
  CREATE_WITHDRAWAL_APPLICATION_FAIL = '[Cabinet] CREATE WITHDRAWAL APPLICATION FAIL',
}

export class LoadApplications implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS;
}

export class LoadApplicationsSuccess implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS_SUCCESS;

  constructor(public apps: IUserApplication[]) {
  }
}

export class LoadApplicationsFail implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS_FAIL;
}


export class LoadApplicationsAffiliate implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE;
}

export class LoadApplicationsAffiliateSuccess implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE_SUCCESS;

  constructor(public apps: IUserApplication[]) {
  }
}

export class LoadApplicationsAffiliateFail implements Action {
  readonly type = CabinetActionTypes.LOAD_APPLICATIONS_AFFILIATE_FAIL;
}


export class LoadAffiliateInfo implements Action {
  readonly type = CabinetActionTypes.LOAD_AFFILIATE_INFO;
}

export class LoadAffiliateInfoSuccess implements Action {
  readonly type = CabinetActionTypes.LOAD_AFFILIATE_INFO_SUCCESS;

  constructor(public info: IAffiliateInfo) {
  }
}

export class LoadAffiliateInfoFail implements Action {
  readonly type = CabinetActionTypes.LOAD_AFFILIATE_INFO_FAIL;
}

export class LoadWithdrawalApplications implements Action {
  readonly type = CabinetActionTypes.LOAD_WITHDRAWAL_APPLICATIONS;
}

export class LoadWithdrawalApplicationsSuccess implements Action {
  readonly type = CabinetActionTypes.LOAD_WITHDRAWAL_APPLICATIONS_SUCCESS;

  constructor(public apps: IWithdrawalApplication[]) {
  }
}

export class LoadWithdrawalApplicationsFail implements Action {
  readonly type = CabinetActionTypes.LOAD_WITHDRAWAL_APPLICATIONS_FAIL;
}

export class ChangePassword implements Action {
  readonly type = CabinetActionTypes.CHANGE_PASSWORD;

  constructor(
    public data: {
      currentPassword: string,
      newPassword: string,
      confirmPassword: string,
    }
  ) {
  }

}

export class ChangePasswordSuccess implements Action {
  readonly type = CabinetActionTypes.CHANGE_PASSWORD_SUCCESS;
}


export class ChangePasswordFail implements Action {
  readonly type = CabinetActionTypes.CHANGE_PASSWORD_FAIL;

  constructor(
    public error: HttpErrorResponse
  ) {
  }
}
export class ChangeUserData implements Action {
  readonly type = CabinetActionTypes.CHANGE_USER_DATA;

  constructor(
    public user: IUser
  ) {
  }
}

export class ChangeUserDataSuccess implements Action {
  readonly type = CabinetActionTypes.CHANGE_USER_DATA_SUCCESS;

  constructor() {}
}

export class ChangeUserDataFailed implements Action {
  readonly type = CabinetActionTypes.CHANGE_USER_DATA_FAIL;

  constructor(public error: HttpErrorResponse) {}
}

export class CreateWithdrawal implements Action {
  readonly type = CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION;

  constructor(public wallet: string) {}
}

export class CreateWithdrawalSuccess implements Action {
  readonly type = CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION_SUCCESS;

  constructor() {}
}

export class CreateWithdrawalFail implements Action {
  readonly type = CabinetActionTypes.CREATE_WITHDRAWAL_APPLICATION_FAIL;

  constructor(public error: HttpErrorResponse) {}
}





export type CabinetActions =
  | LoadApplications
  | LoadApplicationsSuccess
  | LoadApplicationsFail

  | LoadApplicationsAffiliate
  | LoadApplicationsAffiliateSuccess
  | LoadApplicationsAffiliateFail

  | LoadAffiliateInfo
  | LoadAffiliateInfoSuccess
  | LoadAffiliateInfoFail

  | LoadWithdrawalApplications
  | LoadWithdrawalApplicationsSuccess
  | LoadWithdrawalApplicationsFail

  | ChangePassword
  | ChangePasswordSuccess
  | ChangePasswordFail


  | ChangeUserData
  | ChangeUserDataSuccess
  | ChangeUserDataFailed

  | CreateWithdrawal
  | CreateWithdrawalSuccess
  | CreateWithdrawalFail
  ;
