import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { combineLatest, of } from 'rxjs';
import {
  CurrencyActions,
  CurrencyActionTypes,
  LoadCurrency,
  LoadGiveCurrencyFail,
  LoadGiveCurrencySuccess,
  LoadReceiveCurrencyFail,
  LoadReceiveCurrencySuccess,
  LoadToolbarCurrency,
  LoadToolbarCurrencyFail,
  LoadToolbarCurrencySuccess
} from './currency.actions';
import { HttpService } from '@app/core/services/http.service';
import { EApiUrls } from '@app/core/enums/api-urls.enum';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';
import { IToolbarCurrency } from '../../core/interfaces/toolbar-currency.interface';
import * as exchangeSelectors from '../exchange/exchange.selectors';
import { ActivatedRoute } from '@angular/router';
import { IAppState } from '../reducer';
import { Store, select } from '@ngrx/store';


@Injectable()
export class CurrencyEffects {


  @Effect()
  loadCurrency$ = this.actions$.pipe(
    ofType<LoadCurrency>(CurrencyActionTypes.LOAD_CURRENCY),
    withLatestFrom(this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.GIVE))),
    switchMap(([_, giveCurrency]) => {
      //console.log('load currency');
      return combineLatest(
        this.http.get(EApiUrls.GET_EXHANGE_CURRENCY, {mode: ECurrencyBlockTypes.GIVE, id: giveCurrency.id}),
        this.http.get(EApiUrls.GET_EXHANGE_CURRENCY, {mode: ECurrencyBlockTypes.RECEIVE, id: giveCurrency.id})
      ).pipe(
        mergeMap(([give, receive]) => [
          new LoadGiveCurrencySuccess(give),
          new LoadReceiveCurrencySuccess(receive)
        ]),
        catchError(error => {
          // console.log('Currency error');
          // console.log(error);
          return of(new LoadGiveCurrencyFail());
        })
      );
    }),
  );

  @Effect()
  loadToolbarCurrency$ = this.actions$.pipe(
    ofType<LoadToolbarCurrency>(CurrencyActionTypes.LOAD_TOOLBAR_CURRENCY),
    switchMap(() => {
      return this.http.get(EApiUrls.TOOLBAT_CURRENCY).pipe(
        map((res: IToolbarCurrency[]) => new LoadToolbarCurrencySuccess(res)),
        catchError(() => of(new LoadToolbarCurrencyFail()))
      );
    })
  );


  constructor(
    private actions$: Actions<CurrencyActions>,
    private http: HttpService,
    private route: ActivatedRoute,
    private store: Store<IAppState>
  ) {
  }

}
