import { Action } from '@ngrx/store';
import { IFilters } from '@app/core/interfaces/filters.interface';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IToolbarCurrency } from '../../core/interfaces/toolbar-currency.interface';

export enum CurrencyActionTypes {
  SET_ACTIVE_FILTER = '[Currency] SET ACTIVE FILTER',
  SET_ACTIVE_GIVE_FILTER = '[Currency] SET ACTIVE GIVE FILTER',
  SET_ACTIVE_RECEIVE_FILTER = '[Currency] SET ACTIVE RECEIVE FILTER',
  SET_GIVE_SEARCH_STR = '[Currency] SET GIVE SEARCH STR',
  SET_RECEIVE_SEARCH_STR = '[Currency] SET RECEIVE SEARCH STR',


  LOAD_CURRENCY = '[Currency] LOAD CURRENCY',

  LOAD_GIVE_CURRENCY_SUCCESS = '[Currency] LOAD GIVE CURRENCY SUCCESS',
  LOAD_GIVE_CURRENCY_FAIL = '[Currency] LOAD GIVE CURRENCY FAIL',

  LOAD_RECEIVE_CURRENCY_SUCCESS = '[Currency] LOAD RECEIVE CURRENCY SUCCESS',
  LOAD_RECEIVE_CURRENCY_FAIL = '[Currency] LOAD RECEIVE CURRENCY FAIL',

  LOAD_TOOLBAR_CURRENCY = '[Currency] LOAD TOOLBAR CURRENCY',
  LOAD_TOOLBAR_CURRENCY_SUCCESS = '[Currency] LOAD TOOLBAR CURRENCY SUCCESS',
  LOAD_TOOLBAR_CURRENCY_FAIL = '[Currency] LOAD TOOLBAR CURRENCY FAIL',
}

export class SetActiveFilter implements Action {
  readonly type = CurrencyActionTypes.SET_ACTIVE_FILTER;

  constructor(
    public filter: IFilters,
    public mode: ECurrencyBlockTypes
  ) {
  }
}

export class SetActiveGiveFilter implements Action {
  readonly type = CurrencyActionTypes.SET_ACTIVE_GIVE_FILTER;

  constructor(public filter: IFilters) {
  }
}
export class SetGiveSearchStr implements Action {
  readonly type = CurrencyActionTypes.SET_GIVE_SEARCH_STR;

  constructor(public searchStr: string) {
  }
}

export class SetActiveReceiveFilter implements Action {
  readonly type = CurrencyActionTypes.SET_ACTIVE_RECEIVE_FILTER;

  constructor(public filter: IFilters) {
  }
}
export class SetReceiveSearchStr implements Action {
  readonly type = CurrencyActionTypes.SET_RECEIVE_SEARCH_STR;

  constructor(public searchStr: string) {
  }
}

export class LoadCurrency implements Action {
  readonly type = CurrencyActionTypes.LOAD_CURRENCY;
}


export class LoadGiveCurrencySuccess implements Action {
  readonly type = CurrencyActionTypes.LOAD_GIVE_CURRENCY_SUCCESS;

  constructor(public currencies: IExchangeCurrency[]) {
  }
}

export class LoadGiveCurrencyFail implements Action {
  readonly type = CurrencyActionTypes.LOAD_GIVE_CURRENCY_FAIL;
}


export class LoadReceiveCurrencySuccess implements Action {
  readonly type = CurrencyActionTypes.LOAD_RECEIVE_CURRENCY_SUCCESS;

  constructor(public currencies: IExchangeCurrency[]) {
  }
}

export class LoadReceiveCurrencyFail implements Action {
  readonly type = CurrencyActionTypes.LOAD_RECEIVE_CURRENCY_FAIL;
}

export class LoadToolbarCurrency implements Action {
  readonly type = CurrencyActionTypes.LOAD_TOOLBAR_CURRENCY;
}

export class LoadToolbarCurrencySuccess implements Action {
  readonly type = CurrencyActionTypes.LOAD_TOOLBAR_CURRENCY_SUCCESS;

  constructor(public currencies: IToolbarCurrency[]) {
  }
}

export class LoadToolbarCurrencyFail implements Action {
  readonly type = CurrencyActionTypes.LOAD_TOOLBAR_CURRENCY_FAIL;
}


export type CurrencyActions =
  | SetActiveFilter
  | SetActiveReceiveFilter
  | SetActiveGiveFilter
  | SetGiveSearchStr
  | SetReceiveSearchStr

  | LoadCurrency

  | LoadReceiveCurrencySuccess
  | LoadReceiveCurrencyFail

  | LoadGiveCurrencySuccess
  | LoadGiveCurrencyFail

  | LoadToolbarCurrency
  | LoadToolbarCurrencySuccess
  | LoadToolbarCurrencyFail

  ;
