import { CurrencyActions, CurrencyActionTypes } from './currency.actions';
import { IFilters } from '@app/core/interfaces/filters.interface';
import { FILTERS } from '@app/core/constants';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IToolbarCurrency } from '../../core/interfaces/toolbar-currency.interface';


export interface ICurrencyState {
  loading: boolean;

  giveFilters: IFilters[];
  activeGiveFilter: IFilters;
  giveSearchStr: string;

  receiveFilters: IFilters[];
  activeReceiveFilter: IFilters;
  receiveSearchStr: string;

  receiveCurrencies: IExchangeCurrency[];
  giveCurrencies: IExchangeCurrency[];

  toolbarCurrencies: IToolbarCurrency[];
}

export const initialState: ICurrencyState = {
  loading: false,

  giveFilters: FILTERS,
  activeGiveFilter: FILTERS[0],
  giveSearchStr: '',

  receiveFilters: FILTERS,
  activeReceiveFilter: FILTERS[0],
  receiveSearchStr: '',

  receiveCurrencies: [],
  giveCurrencies: [],

  toolbarCurrencies: []
};

export function reducer(state = initialState, action: CurrencyActions): ICurrencyState {
  switch (action.type) {

    case CurrencyActionTypes.SET_ACTIVE_RECEIVE_FILTER:
      return {
        ...state,
        activeReceiveFilter: state.receiveFilters.filter(item => item.code === action.filter.code)[0]
      };
    case CurrencyActionTypes.SET_ACTIVE_GIVE_FILTER:
      return {
        ...state,
        activeGiveFilter: state.giveFilters.filter(item => item.code === action.filter.code)[0]
      };
    case CurrencyActionTypes.SET_GIVE_SEARCH_STR:
      return {
        ...state,
        giveSearchStr: action.searchStr
      };
    case CurrencyActionTypes.SET_RECEIVE_SEARCH_STR:
      return {
        ...state,
        receiveSearchStr: action.searchStr
      };
    case CurrencyActionTypes.LOAD_CURRENCY:
      return {
        ...state,
        loading: true
      };
    case CurrencyActionTypes.LOAD_RECEIVE_CURRENCY_FAIL:
    case CurrencyActionTypes.LOAD_GIVE_CURRENCY_FAIL:
      return {
        ...state,
        loading: false
      };
    case CurrencyActionTypes.LOAD_RECEIVE_CURRENCY_SUCCESS:
      return {
        ...state,
        receiveCurrencies: action.currencies
      };
    case CurrencyActionTypes.LOAD_GIVE_CURRENCY_SUCCESS:
      return {
        ...state,
        giveCurrencies: action.currencies
      };

    case CurrencyActionTypes.SET_ACTIVE_FILTER:
      return {
        ...state,
        [getKeyFilter(action.mode)]: action.filter
      };

    case CurrencyActionTypes.LOAD_TOOLBAR_CURRENCY_SUCCESS:
      return {
        ...state,
        toolbarCurrencies: action.currencies
      };

    default:
      return state;
  }
}


const getKeyFilter = (mode: ECurrencyBlockTypes) => {
  return mode === ECurrencyBlockTypes.GIVE ? 'activeGiveFilter' : 'activeReceiveFilter';
};
