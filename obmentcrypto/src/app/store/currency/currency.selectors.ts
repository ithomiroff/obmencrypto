import { createSelector } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { EFilters } from '@app/core/enums/filters.enum';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';
import { ICurrencyState } from './currency.reducer';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';

const currency = (state: IAppState) => state.currency;

export const getFilters = createSelector(
  currency,
  (state, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.giveFilters : state.receiveFilters;
  }
);

export const getActiveFilters = createSelector(
  currency,
  (state, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.activeGiveFilter : state.activeReceiveFilter;
  }
);

export const getToolbarCurrencies = createSelector(
  currency,
  (state) => state.toolbarCurrencies
);

export const getCurrencies = createSelector(
  currency,
  (state: ICurrencyState, mode: ECurrencyBlockTypes) => {
    const curs = mode === ECurrencyBlockTypes.GIVE ? state.giveCurrencies : state.receiveCurrencies;
    const activeFilter = mode === ECurrencyBlockTypes.GIVE ? state.activeGiveFilter : state.activeReceiveFilter;
    const currentSearchStr = mode === ECurrencyBlockTypes.GIVE ? state.giveSearchStr : state.receiveSearchStr;
    if (isNonEmptyString(currentSearchStr)) {
      return filterBySearchStr(curs, currentSearchStr);
    }
    return filterByType(curs, activeFilter.code);
  }
);

export const getSearchStr = createSelector(
  currency,
  (state: ICurrencyState, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.giveSearchStr : state.receiveSearchStr;
  }
);

function isNonEmptyString(str: string) {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
}

function filterBySearchStr(curs: IExchangeCurrency[], searchStr: string) {
  searchStr = searchStr.toUpperCase();
  return curs.filter(item =>
    item.currencyCode.toUpperCase().indexOf(searchStr) >= 0 ||
    item.name.toUpperCase().indexOf(searchStr) >= 0);
}

function filterByType(curs: IExchangeCurrency[], activeFilter: EFilters) {
  if (activeFilter === EFilters.ALL) {
    return curs;
  }
  return curs.filter(item => item.currencyType === activeFilter);
}
