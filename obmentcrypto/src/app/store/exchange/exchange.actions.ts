import { Action } from '@ngrx/store';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IExchangeCurrency, ICurrencyLimit } from '@app/core/interfaces/exchange-currency.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { IPairLimits } from '@app/core/interfaces/pair-limits.interface';

export enum ExchangeActionTypes {
  SELECT_DEFAULT_VALUES = '[Exchange] SELECT DEFAULT VALUES',

  SET_ACTIVE_CURRENCY = '[Exchange] SET CURRENCY',
  SET_CURRENCY_CANCEL = '[Exchange] SET CURRENCY CANCEL',
  DROP_ACTIVE_CURRENCY = '[Exchange] DROP ACTIVE CURRENCY',
  UPDATE_ACTIVE_CURRENCY_LIMITS = '[Exchange] UPDATE ACTIVE CURRENCY LIMITS',
  UPDATE_ACTIVE_CURRENCY_LIMITS_SUCCESS = '[Exchange] UPDATE ACTIVE CURRENCY LIMITS SUCCESS',

  SET_AMOUNT_VALUE = '[Exchange] SET AMOUNT VALUE',

  CALCULATE_VALUE_BY_INTERVAL = '[Exchange] CALCULATE VALUE BY INTERVAL',
  CALCULATE_VALUE_BY_INTERVAL_SUCCESS = '[Exchange] CALCULATE VALUE BY INTERVAL SUCCESS',
  CALCULATE_VALUE_BY_INTERVAL_FAIL = '[Exchange] CALCULATE VALUE BY INTERVAL FAIL',

  CALCULATE_VALUE = '[Exchange] CALCULATE VALUE',
  CALCULATE_VALUE_SUCCESS = '[Exchange] CALCULATE VALUE SUCCESS',
  CALCULATE_VALUE_FAIL = '[Exchange] CALCULATE VALUE FAIL',

  SET_CALCULATED_GIVE_VALUE = '[Exchange] SET CALCULATED GIVE VALUE',
  SET_CALCULATED_RECEIVE_VALUE = '[Exchange] SET CALCULATED RECEIVE VALUE',

  SET_CALCULATED_GIVE_VALUE_FAIL = '[Exchange] SET CALCULATED GIVE VALUE',
  SET_CALCULATED_RECEIVE_VALUE_FAIL = '[Exchange] SET CALCULATED RECEIVE VALUE',

  SET_WARNING_TEXT = '[Exchange] SET WARNING TEXT',
}

export class SelectDefaultValues implements Action {
  readonly type = ExchangeActionTypes.SELECT_DEFAULT_VALUES;
}


export class SetActiveCurrency implements Action {
  readonly type = ExchangeActionTypes.SET_ACTIVE_CURRENCY;

  constructor(
    public mode: ECurrencyBlockTypes,
    public currency: IExchangeCurrency
  ) {
  }
}

export class DropActiveCurrency implements Action {
  readonly type = ExchangeActionTypes.DROP_ACTIVE_CURRENCY;

  constructor(
    public mode: ECurrencyBlockTypes
  ) {
  }
}

export class SetCurrencyCancel implements Action {
  readonly type = ExchangeActionTypes.SET_CURRENCY_CANCEL;
}


export class SetAmountValue implements Action {
  readonly type = ExchangeActionTypes.SET_AMOUNT_VALUE;

  constructor(
    public value: number,
    public mode: ECurrencyBlockTypes
  ) {
  }
}

export class CalculateValue implements Action {
  readonly type = ExchangeActionTypes.CALCULATE_VALUE;

  constructor(
    public payload: {
      mode: ECurrencyBlockTypes,
      giveCurrencyId: number,
      receiveCurrencyId: number,
      currencyValue: number
    }
  ) {
  }
}

export class CalculateValueFail implements Action {
  readonly type = ExchangeActionTypes.CALCULATE_VALUE_FAIL;

  constructor(
    public error: HttpErrorResponse
  ) {
  }
}


export class CalculateValueByInterval implements Action {
  readonly type = ExchangeActionTypes.CALCULATE_VALUE_BY_INTERVAL;
}

// export class CalculateValueByIntervalSuccess implements Action {
//   readonly type = ExchangeActionTypes.CALCULATE_VALUE_BY_INTERVAL_SUCCESS;
// }
//
// export class CalculateValueByIntervalSuccess implements Action {
//   readonly type = ExchangeActionTypes.CALCULATE_VALUE_BY_INTERVAL_SUCCESS;
// }

export class SetCalculateGiveValue implements Action {
  readonly type = ExchangeActionTypes.SET_CALCULATED_GIVE_VALUE;

  constructor(
    public value: number
  ) {
  }
}

export class SetCalculateReceiveValue implements Action {
  readonly type = ExchangeActionTypes.SET_CALCULATED_RECEIVE_VALUE;

  constructor(
    public value: number
  ) {
  }
}

export class SetCalculateGiveValueFail implements Action {
  readonly type = ExchangeActionTypes.SET_CALCULATED_GIVE_VALUE_FAIL;
}

export class SetCalculateReceiveValueFail implements Action {
  readonly type = ExchangeActionTypes.SET_CALCULATED_RECEIVE_VALUE_FAIL;
}

export class SetWarningText implements Action {
  readonly type = ExchangeActionTypes.SET_WARNING_TEXT;

  constructor(
    public mode: ECurrencyBlockTypes,
    public text: string
  ) {
  }
}


export class UpdateActiveCurrencyLimits implements Action {
  readonly type = ExchangeActionTypes.UPDATE_ACTIVE_CURRENCY_LIMITS;

  constructor() {
  }
}


export class UpdateActiveCurrencyLimitsSuccess implements Action {
  readonly type = ExchangeActionTypes.UPDATE_ACTIVE_CURRENCY_LIMITS_SUCCESS;
  constructor(public data: IPairLimits) {
  }
}


export type ExchangeActions =
  | SelectDefaultValues
  | SetActiveCurrency
  | SetCurrencyCancel
  | DropActiveCurrency
  | SetAmountValue
  | SetWarningText
  | UpdateActiveCurrencyLimits
  | UpdateActiveCurrencyLimitsSuccess

  | CalculateValue
  | CalculateValueFail
  | SetCalculateGiveValue
  | SetCalculateReceiveValue
  | SetCalculateGiveValueFail
  | SetCalculateReceiveValueFail


  | CalculateValueByInterval



  ;
