import { IAppState } from '@app/store/reducer';
import { createSelector } from '@ngrx/store';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';

export const exchange = (state: IAppState) => state.exchange;

export const activeCurrency = createSelector(
  exchange,
  (state, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.giveCurrency : state.receiveCurrency;
  }
);

export const activeAmountValue = createSelector(
  exchange,
  (state, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.amountValueGive : state.amountValueReceive;
  }
);

export const warningAmountField = createSelector(
  exchange,
  (state, mode: ECurrencyBlockTypes) => {
    return mode === ECurrencyBlockTypes.GIVE ? state.warningValueGive : state.warningValueReceive;
  }
);

export const showFormExchange = createSelector(
  exchange,
  (state) => {
    return state.giveCurrency.id && state.receiveCurrency.id;
    // return state.receiveFields.length > 0 && state.giveFields.length > 0;
  }
);

export const validateField = createSelector(
  exchange,
  (state, mode: ECurrencyBlockTypes) => {
    if (mode === ECurrencyBlockTypes.GIVE) {
      const {amountValueGive, giveCurrency} = state;
      if (Number(amountValueGive) === 0) {
        return true;
      }
      return Number(amountValueGive) < Number(giveCurrency.maxValue)
        && Number(amountValueGive) >= Number(giveCurrency.minValue);
    } else {
      const {amountValueReceive, receiveCurrency} = state;
      if (Number(amountValueReceive) === 0) {
        return true;
      }
      return Number(amountValueReceive) < Number(receiveCurrency.maxValue)
        && Number(amountValueReceive) >= Number(receiveCurrency.minValue);
    }
  }
);

