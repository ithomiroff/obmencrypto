import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  CalculateValue,
  CalculateValueByInterval,
  CalculateValueFail,
  DropActiveCurrency,
  ExchangeActions,
  ExchangeActionTypes,
  SetActiveCurrency,
  SetAmountValue,
  SetCalculateGiveValue,
  SetCalculateReceiveValue,
  UpdateActiveCurrencyLimits,
  UpdateActiveCurrencyLimitsSuccess
} from '@app/store/exchange/exchange.actions';
import { select, Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { activeAmountValue, activeCurrency } from './exchange.selectors';
import { ECurrencyBlockTypes, GetReversedBlockType } from '../../core/enums/currency-block-types.enum';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpService } from '@app/core/services/http.service';
import * as formExchangeActions from '../form-exchange/form-exchange.actions';
import * as formExchange from '../form-exchange/form-exchange.actions';
import { EApiUrls } from '../../core/enums/api-urls.enum';
import { ICalculateCurrencyResponse } from '../../core/interfaces/calculate-currency-response.interface';
import { IExchangeCurrency } from '../../core/interfaces/exchange-currency.interface';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { IPairLimits } from '@app/core/interfaces/pair-limits.interface';

@Injectable()
export class ExchangeEffects {


  @Effect({dispatch: false})
  setAmountValue$ = this.actions$.pipe(
    ofType<SetAmountValue>(ExchangeActionTypes.SET_AMOUNT_VALUE),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    map(([[{mode, value}, give], receive]) => {
      if (!give.id || !receive.id) {
        return of();
      }
      const params = {
        mode: mode === ECurrencyBlockTypes.GIVE ? ECurrencyBlockTypes.RECEIVE : ECurrencyBlockTypes.GIVE,
        giveCurrencyId: give.id,
        receiveCurrencyId: receive.id,
        currencyValue: value
      };

      this.store.dispatch(new CalculateValue(params));
      return mode === ECurrencyBlockTypes.GIVE
        ? this.store.dispatch(new formExchange.SetGiveCurrency({...give, value}))
        : this.store.dispatch(new formExchange.SetReceiveCurrency({...receive, value}));
    })
  );

  @Effect()
  setActiveCurrency$ = this.actions$.pipe(
    ofType<SetActiveCurrency>(ExchangeActionTypes.SET_ACTIVE_CURRENCY),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    mergeMap(([[{mode, currency}, give], receive]):any  => {
      this.addUrlParams(give, receive);
      if (give.id === receive.id) {
        return [new DropActiveCurrency(GetReversedBlockType(mode)),
        new formExchangeActions.DropText(),
        new formExchangeActions.DropField(GetReversedBlockType(mode))
        ];
      } else if (!!!currency.isActive) {
        return [
          new DropActiveCurrency(mode),
          new formExchangeActions.DropText(),
          new formExchangeActions.DropField(mode),
        ];
      }
      //console.log('Set currency');


      return [
        new UpdateActiveCurrencyLimits()
      ];
    })
  );

  @Effect()
  calculateValue$ = this.actions$.pipe(
    ofType<CalculateValue>(ExchangeActionTypes.CALCULATE_VALUE),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    switchMap(([[{payload}, give], receive]) => {
      return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, payload).pipe(
        mergeMap(({value}: ICalculateCurrencyResponse) => {
          let actions = [];
          if (payload.mode === ECurrencyBlockTypes.RECEIVE) {
            actions = [
              new SetCalculateReceiveValue(value),
              new formExchange.SetReceiveCurrency({...receive, value})
            ];
          } else {
            actions = [
              new SetCalculateGiveValue(value),
              new formExchange.SetGiveCurrency({...receive, value})
            ];
          }
          return actions;
        }),
        catchError((err) => of(new CalculateValueFail(err)))
      );
    })
  );

  @Effect()
  updateCurrencyLimits$ = this.actions$.pipe(
    ofType<UpdateActiveCurrencyLimits>(ExchangeActionTypes.UPDATE_ACTIVE_CURRENCY_LIMITS),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    switchMap(([[_, giveCurrency], receiveCurrency]) => {
      //console.log('update limits');
      if (!giveCurrency.id || !receiveCurrency.id) { return of(); }

      //console.log('send limits request');
      return this.http.get(EApiUrls.GET_CURRENCY_LIMITS, {
        giveCurrencyId: giveCurrency.id,
        receiveCurrencyId: receiveCurrency.id
      }).pipe(
        map((res: IPairLimits) => {
          //console.log('limits result');
          //console.log(res);
          return new UpdateActiveCurrencyLimitsSuccess(res);
        }),
        catchError(() => {
          //console.log('Cant update limits');
          return of();
        })
      );
    })
  );



  @Effect({dispatch: false})
  calculateValueInterval$ = this.actions$.pipe(
    ofType<CalculateValueByInterval>(ExchangeActionTypes.CALCULATE_VALUE_BY_INTERVAL),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    withLatestFrom(this.store.pipe(select(activeAmountValue, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeAmountValue, ECurrencyBlockTypes.RECEIVE))),
    switchMap(([[[[_, giveCur], receiveCur], giveValue], receiveValue]) => {
      if (!giveValue || !receiveValue) {
        return;
      }
      const mode = giveValue ? ECurrencyBlockTypes.GIVE : ECurrencyBlockTypes.RECEIVE;
      const params = {
        mode,
        giveCurrencyId: giveCur.id,
        receiveCurrencyId: receiveCur.id,
        currencyValue: giveValue ? receiveValue : giveValue
      };

      return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, params).pipe(
        mergeMap(({value}: ICalculateCurrencyResponse): any => {
          if (mode === ECurrencyBlockTypes.RECEIVE) {
            return [
              new SetCalculateReceiveValue(value),
              new formExchange.SetReceiveCurrency({...receiveCur, value})
            ];
          } else {
            return [
              new SetCalculateGiveValue(value),
              new formExchange.SetGiveCurrency({...giveCur, value})
            ];
          }
        }),
        catchError((err) => of(new CalculateValueFail(err)))
      );
    })
  );


  constructor(
    private actions$: Actions<ExchangeActions>,
    private store: Store<IAppState>,
    private http: HttpService,
    private route: ActivatedRoute
  ) {
  }


  url = '';

  addUrlParams(giveCur: IExchangeCurrency, receiveCur: IExchangeCurrency): void {
    this.url = `${giveCur.currencyCode}-${receiveCur.currencyCode}`;
    if (giveCur.currencyCode && receiveCur.currencyCode) {
      window.history.replaceState(null, null, this.url);
    }
    if (giveCur.currencyCode === receiveCur.currencyCode) {
      window.history.replaceState(null, null, `/`);
    }
    this.url = '';
  }

}
