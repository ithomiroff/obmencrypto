import { ExchangeActions, ExchangeActionTypes } from './exchange.actions';
import { IExchangeCurrency, ICurrencyLimit } from '@app/core/interfaces/exchange-currency.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { HttpErrorResponse } from '@angular/common/http';


export interface IExchangeState {
  amountValueGive: number;
  amountValueReceive: number;

  warningValueGive: string;
  warningValueReceive: string;

  giveCurrency: Partial<IExchangeCurrency>;
  receiveCurrency: Partial<IExchangeCurrency>;

  giveFieldLoading: boolean;
  receiveFieldLoading: boolean;

  calculateError: HttpErrorResponse;
}

export const initialState: IExchangeState = {
  amountValueGive: null,
  amountValueReceive: null,

  warningValueGive: null,
  warningValueReceive: null,

  giveCurrency: {id: null},
  receiveCurrency: {id: null},

  giveFieldLoading: false,
  receiveFieldLoading: false,

  calculateError: null,
};
const getKey = (type: ECurrencyBlockTypes) => type === ECurrencyBlockTypes.GIVE ? 'giveCurrency' : 'receiveCurrency';

const getKeyValue = (type: ECurrencyBlockTypes) => type === ECurrencyBlockTypes.GIVE ? 'amountValueGive' : 'amountValueReceive';

export function reducer(state = initialState, action: ExchangeActions): IExchangeState {
  switch (action.type) {
    case ExchangeActionTypes.SET_ACTIVE_CURRENCY:
      return {
        ...state,
        [getKey(action.mode)]: {
          ...state[getKey(action.mode)],
          ...action.currency,
        },
      };
    case ExchangeActionTypes.DROP_ACTIVE_CURRENCY:
      return {
        ...state,
        [getKey(action.mode)]: { id: null },
      };

    case ExchangeActionTypes.SET_AMOUNT_VALUE:
      return {
        ...state,
        [getKeyValue(action.mode)]: action.value,
      };

    case ExchangeActionTypes.SET_WARNING_TEXT:
      return {
        ...state,
        warningValueGive:
          action.mode === ECurrencyBlockTypes.GIVE
            ? action.text
            : state.warningValueGive,
        warningValueReceive:
          action.mode === ECurrencyBlockTypes.RECEIVE
            ? action.text
            : state.warningValueReceive,
      };

    case ExchangeActionTypes.SET_CALCULATED_GIVE_VALUE:
      return {
        ...state,
        // @ts-ignore
        amountValueGive: action.value,
      };
    case ExchangeActionTypes.SET_CALCULATED_RECEIVE_VALUE:
      return {
        ...state,
        // @ts-ignore
        amountValueReceive: action.value,
      };

    case ExchangeActionTypes.CALCULATE_VALUE:
      return {
        ...state,
        calculateError: null,
      };
    case ExchangeActionTypes.UPDATE_ACTIVE_CURRENCY_LIMITS_SUCCESS:
      return {
        ...state,
        giveCurrency: {
          ...state.giveCurrency,
          ...action.data.giveLimit,
        },
        receiveCurrency: {
          ...state.receiveCurrency,
          ...action.data.receiveLimit,
        },
      };

    default:
      return state;
  }
}

