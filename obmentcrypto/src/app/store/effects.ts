import { CurrencyEffects } from '@app/store/currency/currency.effects';
import { ExchangeEffects } from '@app/store/exchange/exchange.effects';
import { LayoutEffects } from './layout/layout.effects';
import { FormExchangeEffects } from './form-exchange/form-exchange.effects';
import { ApplicationEffects } from './application/application.effects';
import { CabinetEffects } from './cabinet/cabinet.effects';
import { AuthEffects } from '@app/store/auth/auth.effects';
import { ArticlesEffects } from './articles/articles.effects';

export const effects = [
  CurrencyEffects,
  ExchangeEffects,
  LayoutEffects,
  ExchangeEffects,
  FormExchangeEffects,
  ApplicationEffects,
  CabinetEffects,
  AuthEffects,
  ArticlesEffects
];
