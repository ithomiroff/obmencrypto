import { ApplicationActions, ApplicationActionTypes } from './application.actions';
import { IApplicationInfo } from '../../core/interfaces/application-info.interface';

export interface IApplicationState {
  key: string;
  loading: boolean;
  application: IApplicationInfo;
  buttonDecline: boolean;
  buttonPaid: boolean;
  error: string;
}

export const initialState: IApplicationState = {
  key: null,
  loading: false,
  application: null,
  buttonDecline: false,
  buttonPaid: false,
  error: null
};

export function reducer(state = initialState, action: ApplicationActions): IApplicationState {
  switch (action.type) {

    case ApplicationActionTypes.SET_KEY_APPLICATION:
      return {
        ...state,
        key: action.key
      };
    case ApplicationActionTypes.GET_APPLICATION_INFO:
      return {
        ...state,
        loading: true,
        error: null
      };
    case ApplicationActionTypes.GET_APPLICATION_INFO_SUCCESS:
      return {
        ...state,
        loading: false,
        application: action.application,
      };
    case ApplicationActionTypes.GET_APPLICATION_INFO_FAIL:
      return {
        ...state,
        loading: false
      };

    case ApplicationActionTypes.OPERATION_APPLICATION_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error && action.error.error ? action.error.error : 'Ошибка при отправке данных, попробуйте позже'
      };


    case ApplicationActionTypes.PAID_APPLICATION:
    case ApplicationActionTypes.CANCEL_APPLICATION:
      return {
        ...state,
        loading: true
      };

    case ApplicationActionTypes.DROP_APPLICATION:
      return {
        ...state,
        ...initialState
      };

    default:
      return state;
  }
}
