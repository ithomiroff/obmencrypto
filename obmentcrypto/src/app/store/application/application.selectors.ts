import { IAppState } from '../reducer';
import { createSelector } from '@ngrx/store';

const application = (state: IAppState) => state.application;

export const getApplicationKey = createSelector(
  application,
  (state) => state.application.key
);

export const getApplication = createSelector(
  application,
  (state) => state.application
);

export const getLoading = createSelector(
    application,
  (state) => state.loading
);

export const getStatus = createSelector(
  application,
  (state) => state.application.status
);

export const getSecretKey = createSelector(
  application,
  (state) => state.key
);

export const getError = createSelector(
  application,
  (state) => state.error
);
