import { MatDialog } from '@angular/material';
import { EApiUrls } from '@app/core/enums/api-urls.enum';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  ApplicationActions,
  ApplicationActionTypes,
  GetApplicationInfo,
  GetApplicationInfoFail,
  GetApplicationInfoSuccess, OperationApplicationFail,
  PaidApplication, SetKeyApplication, SetPromocodeApplication
} from './application.actions';
import { HttpService } from '../../core/services/http.service';
import { IApplicationInfo } from '../../core/interfaces/application-info.interface';
import { IAppState } from '../reducer';
import { select, Store } from '@ngrx/store';
import { showDefaultModal } from '@app/core/utils/modal-configs';


@Injectable()
export class ApplicationEffects {


  @Effect()
  getApplicationInfo$ = this.actions$.pipe(
    ofType<GetApplicationInfo>(ApplicationActionTypes.GET_APPLICATION_INFO),
    switchMap(({key}) => {
      const params = {key};
      return this.http.get(EApiUrls.APPLICATION, params).pipe(
        mergeMap((res: IApplicationInfo) => [
          new GetApplicationInfoSuccess(res),
          new SetKeyApplication(key)
        ]),
        catchError(() => of(new GetApplicationInfoFail()))
      );
    })
  );

  @Effect()
  paidApplication$ = this.actions$.pipe(
    ofType<PaidApplication>(ApplicationActionTypes.PAID_APPLICATION),
    withLatestFrom(this.store.pipe(select(state => state.application.key))),
    switchMap(([_, key]) => {
      return this.http.post(EApiUrls.ORDER + EApiUrls.USER_PAID, {key}).pipe(
        map(() => new GetApplicationInfo(key)),
        catchError((err) => of(new OperationApplicationFail(err)))
      );
    }),
  );

  @Effect()
  cancelApplication$ = this.actions$.pipe(
    ofType<PaidApplication>(ApplicationActionTypes.CANCEL_APPLICATION),
    withLatestFrom(this.store.pipe(select(state => state.application.key))),
    switchMap(([_, key]) => {
      return this.http.post(EApiUrls.ORDER + EApiUrls.DECLINE, {key}).pipe(
        map(() => new GetApplicationInfo(key)),
        catchError((err) => of(new OperationApplicationFail(err)))
      );
    }),
  );

  @Effect()
  setPromoCodeApplication$ = this.actions$.pipe(
    ofType<SetPromocodeApplication>(ApplicationActionTypes.SET_PROMOCODE_APPLICATION),
    withLatestFrom(this.store.pipe(select(state => state.application.key))),
    switchMap(([_, key]) => {
      const code = _.code;
      return this.http.post(EApiUrls.ORDER + EApiUrls.SET_PROMO, {key, code}).pipe(
        map(() => new GetApplicationInfo(key)),
        catchError((err) => of(new OperationApplicationFail(err)))
      );
    })
  );

  @Effect()
  operationApplicationFail$ = this.actions$.pipe(
    ofType<OperationApplicationFail>(ApplicationActionTypes.OPERATION_APPLICATION_FAIL),
    switchMap(({error}) => {
      const message = error && error.error ? error.error.errorDescription : 'Ошибка сервера';
      return showDefaultModal(this.dialog, {
        title: 'Ошибка',
        message,
      });
    })
  );

  constructor(
    private actions$: Actions<ApplicationActions>,
    private http: HttpService,
    private store: Store<IAppState>,
    private dialog: MatDialog
  ) {}

}
