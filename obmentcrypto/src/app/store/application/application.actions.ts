import { Action } from '@ngrx/store';
import { IApplicationInfo } from '../../core/interfaces/application-info.interface';
import { HttpErrorResponse } from '@angular/common/http';

export enum ApplicationActionTypes {

  SET_KEY_APPLICATION = '[Application] SET KEY APPLICATION',

  GET_APPLICATION_INFO = '[Application] GET APPLICATION INFO',
  GET_APPLICATION_INFO_SUCCESS = '[Application] GET APPLICATION INFO SUCCESS',
  GET_APPLICATION_INFO_FAIL = '[Application] GET APPLICATION INFO FAIL',

  CANCEL_APPLICATION = '[Application] CANCEL_APPLICATION',
  PAID_APPLICATION = '[Application] PAID APPLICATION',
  MAKE_PAYMENT_APPLICATION = '[Application] MAKE PAYMENT APPLICATION',

  OPERATION_APPLICATION_FAIL = '[Application] OPERATION APPLICATION FAIL',
  DROP_APPLICATION = '[Application] DROP APPLICATION',

  SET_PROMOCODE_APPLICATION = '[Application] SET PROMOCODE APPLICATION',
}

export class SetKeyApplication implements Action {
  readonly type = ApplicationActionTypes.SET_KEY_APPLICATION;

  constructor(public key: string) {
  }

}

export class GetApplicationInfo implements Action {
  readonly type = ApplicationActionTypes.GET_APPLICATION_INFO;

  constructor(public key: string) {
  }

}

export class GetApplicationInfoSuccess implements Action {
  readonly type = ApplicationActionTypes.GET_APPLICATION_INFO_SUCCESS;

  constructor(public application: IApplicationInfo) {
  }

}

export class GetApplicationInfoFail implements Action {
  readonly type = ApplicationActionTypes.GET_APPLICATION_INFO_FAIL;
}


export class CancelApplication implements Action {
  readonly type = ApplicationActionTypes.CANCEL_APPLICATION;
}

export class PaidApplication implements Action {
  readonly type = ApplicationActionTypes.PAID_APPLICATION;
}

export class MakePaymentApplication implements Action {
  readonly type = ApplicationActionTypes.MAKE_PAYMENT_APPLICATION;
}

export class OperationApplicationFail implements Action {
  readonly type = ApplicationActionTypes.OPERATION_APPLICATION_FAIL;

  constructor(public error: HttpErrorResponse) {
  }
}

export class DropApplication implements Action {
  readonly type = ApplicationActionTypes.DROP_APPLICATION;
}

export class SetPromocodeApplication implements Action {
  readonly type = ApplicationActionTypes.SET_PROMOCODE_APPLICATION;

  constructor(public code: string) {
  }
}


export type ApplicationActions =
  | SetKeyApplication

  | GetApplicationInfo
  | GetApplicationInfoSuccess
  | GetApplicationInfoFail

  | CancelApplication
  | PaidApplication
  | MakePaymentApplication
  | OperationApplicationFail

  | DropApplication

  | SetPromocodeApplication
  ;
