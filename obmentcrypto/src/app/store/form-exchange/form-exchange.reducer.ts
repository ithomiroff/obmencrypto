import { FormExchangeActions, FormExchangeActionTypes } from './form-exchange.actions';
import { ITextExchange } from '../../core/interfaces/text-exchange.interface';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';
import { IExchangeForm, IPersonInfo } from '../../core/interfaces/exchange-make.interface';
import { IFieldType } from '../../core/interfaces/field-type.interface';

export interface IFormExchangeState {
  textForm: ITextExchange;
  loading: boolean;

  loadingForm: boolean;
  form: IExchangeForm;


  formValues;
  exchangeError: any;


  // ********
  giveValue: string;
  receiveValue: string;
  giveFields: IFieldType[];
  receiveFields: IFieldType[];
  personInfo: IPersonInfo;
  calculateError: string;

  giveTransactionData: IFieldType[];
  receiveTransactionData: IFieldType[];
}

export const initialState: IFormExchangeState = {
  textForm: null,
  loading: false,

  loadingForm: false,
  form: {
    giveCurrency: null,
    receiveCurrency: null,
    giveTransactionData: [],
    receiveTransactionData: [],
    personInfo: {
      email: null,
      phone: null,
      telegram: null
    }
  },

  formValues: null,
  exchangeError: null,

  giveFields: [],
  receiveFields: [],

  giveValue: null,
  receiveValue: null,
  personInfo: null,
  giveTransactionData: [],
  receiveTransactionData: [],

  calculateError: null

};

export function reducer(state = initialState, action: FormExchangeActions): IFormExchangeState {
  switch (action.type) {

    case FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE_SUCCESS:
      return {
        ...state,
        receiveFields: action.fields
      };

    case FormExchangeActionTypes.GET_FORM_FIELD_GIVE_SUCCESS:
      return {
        ...state,
        giveFields: action.fields
      };

    case FormExchangeActionTypes.GET_TEXT_SUCCESS:
      return {
        ...state,
        textForm: action.text
      };

    case FormExchangeActionTypes.GET_TEXT:
      return {
        ...state,
        loading: true
      };

    case FormExchangeActionTypes.GET_TEXT_FAIL:
      return {
        ...state,
        loading: false
      };

    case FormExchangeActionTypes.DROP_TEXT:
      return {
        ...state,
        textForm: null
      };

    case FormExchangeActionTypes.DROP_FIELD:
      return {
        ...state,
        giveFields: action.mode === ECurrencyBlockTypes.GIVE ? [] : state.giveFields,
        receiveFields: action.mode === ECurrencyBlockTypes.RECEIVE ? [] : state.receiveFields
      };


    case FormExchangeActionTypes.SET_TRANSACTION_DATA_GIVE:
      let itemGive = state.giveFields.find(item => item.id === action.data.id);
      itemGive = {
        ...itemGive,
        ...action.data
      };
      const fieldsGive = state.form.giveTransactionData.filter(item => item.id !== action.data.id);
      fieldsGive.push(itemGive);
      return {
        ...state,
        form: {
          ...state.form,
          giveTransactionData: fieldsGive
        }
      };

    case FormExchangeActionTypes.SET_TRANSACTION_DATA_RECEIVE:
      let itemReceive = state.receiveFields.find(item => item.id === action.data.id);
      itemReceive = {
        ...itemReceive,
        ...action.data
      };
      const fieldsRecive = state.form.receiveTransactionData.filter(item => item.id !== action.data.id);
      fieldsRecive.push(itemReceive);
      return {
        ...state,
        form: {
          ...state.form,
          receiveTransactionData: fieldsRecive
        }
      };

    case FormExchangeActionTypes.SET_GIVE_CURRENCY:
      return {
        ...state,
        form: {
          ...state.form,
          giveCurrency: {
            ...state.form.giveCurrency,
            ...action.currency
          }
        }
      };

    case FormExchangeActionTypes.SET_RECEIVE_CURRENCY:
      return {
        ...state,
        form: {
          ...state.form,
          receiveCurrency: {
            ...state.form.receiveCurrency,
            ...action.currency
          }
        }
      };

    case FormExchangeActionTypes.SET_PERSON_DATA:
      return {
        ...state,
        personInfo: action.info
      };

    case FormExchangeActionTypes.EXCHANGE_CURRENCY:
      return {
        ...state,
        loadingForm: true
      };
    case FormExchangeActionTypes.EXCHANGE_CURRENCY_FAIL:
      return {
        ...state,
        loadingForm: false
      };
    case FormExchangeActionTypes.EXCHANGE_CURRENCY_SUCCESS:
      return {
        ...state,
        loadingForm: false
      };


    case FormExchangeActionTypes.SET_FORM_VALUE:
      return {
        ...state,
        formValues: action.value
      };

    case FormExchangeActionTypes.VALIDATE_FORM_FAIL:
      return {
        ...state,
        exchangeError: action.error
      };


    case FormExchangeActionTypes.SET_GIVE_VALUE:
      return {
        ...state,
        giveValue: action.value,
        exchangeError: null,
        calculateError: null
      };

    case FormExchangeActionTypes.SET_RECEIVE_VALUE:
      return {
        ...state,
        receiveValue: action.value,
        exchangeError: null,
        calculateError: null
      };


    case FormExchangeActionTypes.UPDATE_CURRENCY_VALUE:
      return {
        ...state,
        giveValue: action.mode === ECurrencyBlockTypes.GIVE ? action.value : state.giveValue,
        receiveValue: action.mode === ECurrencyBlockTypes.RECEIVE ? action.value : state.receiveValue
      };

    case FormExchangeActionTypes.UPDATE_FIELD_TRANSACTION:
      return {
        ...state,
        giveTransactionData: action.mode === ECurrencyBlockTypes.GIVE ? action.fields : state.giveTransactionData,
        receiveTransactionData: action.mode === ECurrencyBlockTypes.RECEIVE ? action.fields : state.receiveTransactionData
      };

    case FormExchangeActionTypes.CALCULATE_FAIL:
      return {
        ...state,
        calculateError: action.error && action.error.error ? action.error.error.errorDescription : 'Ошибка сервера'
      };


    default:
      return state;
  }
}


