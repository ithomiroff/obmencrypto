import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import {
  Calculate,
  CalculateFail,
  CalculateInput,
  ExchangeCurrency,
  ExchangeCurrencyFail,
  ExchangeCurrencySuccess,
  FormExchangeActions,
  FormExchangeActionTypes,
  GetFormFieldGive,
  GetFormFieldGiveFail,
  GetFormFieldGiveSuccess,
  GetFormFieldReceive,
  GetFormFieldReceiveFail,
  GetFormFieldReceiveSuccess,
  GetText,
  GetTextFail,
  GetTextSuccess,
  UpdateCurrencyValue,
  ValidateForm,
  ValidateFormFail,
  ValidateFormSuccess,
  SetPersonData
} from './form-exchange.actions';
import { HttpService } from '../../core/services/http.service';
import { EApiUrls } from '../../core/enums/api-urls.enum';
import { IFieldType } from '../../core/interfaces/field-type.interface';
import { IAppState } from '../reducer';
import { select, Store } from '@ngrx/store';
import * as formExchangeSelectors from './form-exchange.selectors';
import { fieldsGive, fieldsReceive, getForm } from './form-exchange.selectors';
import { ITextExchange } from '../../core/interfaces/text-exchange.interface';
import * as applicationActions from '../application/application.actions';
import * as exchangeSelectors from '../exchange/exchange.selectors';
import { activeCurrency } from '../exchange/exchange.selectors';
import * as authSelectors from '../auth/auth.selectors';
import { Router } from '@angular/router';
import { EAppUrls } from '@app/core/enums/app-urls.enum';
import { InfoSnackService } from '../../core/services/error-snack.service';
import { CryptoValidatorService } from '../../core/services/crypto-validator.service';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';
import { IPersonInfo } from '../../core/interfaces/exchange-make.interface';
import { IUser } from '../../core/interfaces/user.interface';
import { MatDialog } from '@angular/material';
import { showDefaultModal } from '@app/core/utils/modal-configs';
import { UpdateActiveCurrencyLimits } from '../exchange/exchange.actions';

@Injectable()
export class FormExchangeEffects {


  @Effect()
  getFormFieldReceive$ = this.actions$.pipe(
    ofType<GetFormFieldReceive>(FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE),
    switchMap(({currencyId}) => {
      return this.http.get(EApiUrls.GET_FIELD_CURRENCY_EXCHANGE, {currencyId, mode: ECurrencyBlockTypes.RECEIVE}).pipe(
        map((fields: IFieldType[]) => new GetFormFieldReceiveSuccess(fields)),
        catchError(() => of(new GetFormFieldReceiveFail()))
      );
    })
  );

  @Effect()
  getFormFieldGive$ = this.actions$.pipe(
    ofType<GetFormFieldGive>(FormExchangeActionTypes.GET_FORM_FIELD_GIVE),
    switchMap(({currencyId}) => {
      return this.http.get(EApiUrls.GET_FIELD_CURRENCY_EXCHANGE, {currencyId, mode: ECurrencyBlockTypes.GIVE}).pipe(
        map((fields: IFieldType[]) => new GetFormFieldGiveSuccess(fields)),
        catchError(() => of(new GetFormFieldGiveFail()))
      );
    })
  );

  @Effect({dispatch: false})
  getFormFieldSuccess$ = this.actions$.pipe(
    ofType<GetFormFieldGiveSuccess
      | GetFormFieldReceiveSuccess>(
      FormExchangeActionTypes.GET_FORM_FIELD_GIVE_SUCCESS,
      FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE_SUCCESS
    ),
    withLatestFrom(this.store.pipe(select(fieldsGive))),
    withLatestFrom(this.store.pipe(select(fieldsReceive))),
    map(([[_, give], recive]) => {
      if (give && recive) {
        this.store.dispatch(new GetText());
      }
    })
  );

  @Effect()
  getText$ = this.actions$.pipe(
    ofType<GetText>(FormExchangeActionTypes.GET_TEXT),
    withLatestFrom(this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    switchMap(([[_, giveCurrency], receiveCurrency]) => {
      if (!giveCurrency|| !giveCurrency.id || !receiveCurrency || !receiveCurrency.id) {
        return of(new GetTextFail());
      }
      return this.http.get(`${EApiUrls.GET_TEXT}/${giveCurrency.id}/${receiveCurrency.id}`).pipe(
        map((res: ITextExchange) => new GetTextSuccess(res)),
        catchError(() => of(new GetTextFail()))
      );
    })
  );


  @Effect()
  exchangeCurrency$ = this.actions$.pipe(
    ofType<ExchangeCurrency>(FormExchangeActionTypes.EXCHANGE_CURRENCY),
    withLatestFrom(this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    withLatestFrom(this.store.pipe(select(formExchangeSelectors.getGiveValue))),
    withLatestFrom(this.store.pipe(select(formExchangeSelectors.getReceiveValue))),
    withLatestFrom(this.store.pipe(select(formExchangeSelectors.getPersonInfo))),
    withLatestFrom(this.store.pipe(select(formExchangeSelectors.getTransationFields, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(formExchangeSelectors.getTransationFields, ECurrencyBlockTypes.RECEIVE))),
    withLatestFrom(this.store.pipe(select(authSelectors.getAuthUser))),
    switchMap(([[[[[[[[
      _, giveCurrency
    ], receiveCurrency], giveValue], receiveValue], personInfo], giveTransactionData], receiveTransactionData], user]) => {
      const body = {
        giveCurrency: {
          ...giveCurrency,
          value: giveValue
        },
        receiveCurrency: {
          ...receiveCurrency,
          value: receiveValue
        },
        personInfo: this.getFormatPersonInfo(personInfo, user),
        giveTransactionData,
        receiveTransactionData
      };
      return this.http.post(EApiUrls.EXCHANGE, body).pipe(
        map(({key}) => new ExchangeCurrencySuccess(key)),
        catchError((err) => of(new ExchangeCurrencyFail(err)))
      );
    })
  );

  @Effect({dispatch: false})
  exchangeCurrencyFail$ = this.actions$.pipe(
    ofType<ExchangeCurrencyFail>(FormExchangeActionTypes.EXCHANGE_CURRENCY_FAIL),
    tap(({error}) => {
      const msg = error && error.error ? error.error.errorDescription : 'Ошибка сервера';
      return showDefaultModal(this.dialog,
        {
          title: 'Сервис не работает',
          message: msg
        });
    })
  );

  @Effect({dispatch: false})
  exchangeCurrencySuccess$ = this.actions$.pipe(
    ofType<ExchangeCurrencySuccess>(FormExchangeActionTypes.EXCHANGE_CURRENCY_SUCCESS),
    map(({key}) => {
      this.store.dispatch(new applicationActions.SetKeyApplication(key));
      this.router.navigateByUrl(`${EAppUrls.APPLICATION}/${key}`);
    })
  );

  @Effect()
  validateForm$ = this.actions$.pipe(
    ofType<ValidateForm>(FormExchangeActionTypes.VALIDATE_FORM),
    withLatestFrom(this.store.pipe(select(getForm))),
    map(([_, form]) => {
      const {giveTransactionData, receiveTransactionData, giveCurrency, receiveCurrency} = form;
      const errors = [];
      if (receiveTransactionData.length > 0) {
        const valid = this.validatorWallet.validate(receiveTransactionData[0].value, receiveCurrency.currencyCode);
        if (!valid) {
          errors.push('Неккоректный номер кошелька');
        }
      }
      if (giveTransactionData.length > 0) {
        const valid = this.validatorWallet.validate(giveTransactionData[0].value, giveCurrency.currencyCode);
        if (!valid) {
          errors.push('Неккоректный номер кошелька');
        }
      }
      if (errors.length) {
        return new ValidateFormFail(errors);
      } else {
        return new ValidateFormSuccess();
      }
    })
  );

  @Effect({dispatch: false})
  calculate$ = this.actions$.pipe(
    ofType<Calculate>(FormExchangeActionTypes.CALCULATE),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    withLatestFrom(this.store.select(state => state.formExchange.giveValue)),
    withLatestFrom(this.store.select(state => state.formExchange.receiveValue)),
    switchMap(([[[[_, activeGive], activeReceive], giveVal], recVal]) => {
      if (giveVal && activeGive.id && activeReceive.id) {
        const params = {
          mode: ECurrencyBlockTypes.RECEIVE,
          giveCurrencyId: activeGive.id,
          receiveCurrencyId: activeReceive.id,
          currencyValue: giveVal
        };
        return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, params).pipe(
          map((response: { value: number }) =>
            this.store.dispatch(new UpdateCurrencyValue(ECurrencyBlockTypes.RECEIVE, response.value.toString()))
          ),
          catchError((err) => of(this.store.dispatch(new CalculateFail(err))))
        );
      }
      return EMPTY;
    })
  );

  @Effect({dispatch: false})
  calculateInput$ = this.actions$.pipe(
    ofType<CalculateInput>(FormExchangeActionTypes.CALCULATE_INPUT),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
    withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
    withLatestFrom(this.store.select(state => state.formExchange.giveValue)),
    withLatestFrom(this.store.select(state => state.formExchange.receiveValue)),
    switchMap(([[[[{mode}, activeGive], activeReceive], giveVal], recVal]) => {
      const params = {
        mode: mode === ECurrencyBlockTypes.RECEIVE ? ECurrencyBlockTypes.GIVE : ECurrencyBlockTypes.RECEIVE,
        giveCurrencyId: activeGive.id,
        receiveCurrencyId: activeReceive.id,
        currencyValue: mode === ECurrencyBlockTypes.RECEIVE ? recVal :  giveVal
      };
      if (params.currencyValue && params.giveCurrencyId && params.receiveCurrencyId) {
        return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, params).pipe(
          map((response: { value: number }) =>
            this.store.dispatch(new UpdateCurrencyValue(
              mode === ECurrencyBlockTypes.RECEIVE ? ECurrencyBlockTypes.GIVE : ECurrencyBlockTypes.RECEIVE, response.value.toString()
            ))
          ),
          catchError((err) => of(this.store.dispatch(new CalculateFail(err))))
        );
      }
      return EMPTY;
    })
  );

  // @Effect()
  // setGiveValue$ = this.actions$.pipe(
  //   ofType<SetGiveValue>(FormExchangeActionTypes.SET_GIVE_VALUE),
  //   withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
  //   withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
  //   filter(([[_, g], r]) => !!r && !!r.id),
  //   switchMap(([[{value}, give], receive]) => {
  //     const params = {
  //       mode: ECurrencyBlockTypes.RECEIVE,
  //       giveCurrencyId: give.id,
  //       receiveCurrencyId: receive.id,
  //       currencyValue: value
  //     };
  //     return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, params).pipe(
  //       map((response: { value: number }) => new UpdateCurrencyValue(ECurrencyBlockTypes.RECEIVE, response.value.toString())),
  //       catchError((err) => of(new CalculateFail(err)))
  //     );
  //   })
  // );

  // @Effect()
  // setReceiveValue$ = this.actions$.pipe(
  //   ofType<SetReceiveValue>(FormExchangeActionTypes.SET_RECEIVE_VALUE),
  //   withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.GIVE))),
  //   withLatestFrom(this.store.pipe(select(activeCurrency, ECurrencyBlockTypes.RECEIVE))),
  //   switchMap(([[{value}, give], receive]) => {
  //     const params = {
  //       mode: ECurrencyBlockTypes.GIVE,
  //       giveCurrencyId: give.id,
  //       receiveCurrencyId: receive.id,
  //       currencyValue: value
  //     };
  //     return this.http.get(EApiUrls.CALCULATE_CURRENCY_VALUE, params).pipe(
  //       map((response: { value: number }) => new UpdateCurrencyValue(ECurrencyBlockTypes.GIVE, response.value.toString())),
  //       catchError((err) => of(new CalculateFail(err)))
  //     );
  //   })
  // );

  getFormatPersonInfo(person: IPersonInfo, user: IUser): IPersonInfo {
    console.log('Person');
    console.log(person);
    console.log('user');
    console.log(user);
    let emailField: string | null = person ? person.email : (user ? user.email : null);
    if (person) {
      return {
        ...person,
        email: emailField,
        phone: person.phone || null,
        telegram: person.telegram || null
      };
    }
    return null;
  }


  constructor(
    private actions$: Actions<FormExchangeActions>,
    private http: HttpService,
    private store: Store<IAppState>,
    private router: Router,
    private errorService: InfoSnackService,
    private validatorWallet: CryptoValidatorService,
    private dialog: MatDialog
  ) {
  }

}
