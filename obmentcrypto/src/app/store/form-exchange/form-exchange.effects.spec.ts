import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { FormExchangeEffects } from './form-exchange.effects';

describe('FormExchangeEffects', () => {
  let actions$: Observable<any>;
  let effects: FormExchangeEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FormExchangeEffects,
        provideMockActions(() => actions$)
      ]
    });

    // effects = TestBed.get<FormExchangeEffects>(FormExchangeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
