import { Action } from '@ngrx/store';
import { ECurrencyBlockTypes } from '../../core/enums/currency-block-types.enum';
import { ITextExchange } from '../../core/interfaces/text-exchange.interface';
import { IPersonInfo } from '../../core/interfaces/exchange-make.interface';
import { IExchangeCurrency } from '../../core/interfaces/exchange-currency.interface';
import { IFieldType } from '../../core/interfaces/field-type.interface';
import { HttpErrorResponse } from '@angular/common/http';

export enum FormExchangeActionTypes {
  GET_FORM_FIELD_RECEIVE = '[FormExchange] GET FORM FIELD RECEIVE',
  GET_FORM_FIELD_RECEIVE_SUCCESS = '[FormExchange] GET FORM FIELD RECEIVE SUCCESS',
  GET_FORM_FIELD_RECEIVE_FAIL = '[FormExchange] GET FORM FIELD RECEIVE FAIL',

  GET_FORM_FIELD_GIVE = '[FormExchange] GET FORM FIELD GIVE',
  GET_FORM_FIELD_GIVE_SUCCESS = '[FormExchange] GET FORM FIELD GIVE SUCCESS',
  GET_FORM_FIELD_GIVE_FAIL = '[FormExchange] GET FORM FIELD GIVE FAIL',

  GET_TEXT = '[FormExchange] GET_TEXT',
  GET_TEXT_SUCCESS = '[FormExchange] GET_TEXT_SUCCESS',
  GET_TEXT_FAIL = '[FormExchange] GET_TEXT_FAIL',

  DROP_FIELD = '[FormExchange] DROP FIELD',
  DROP_TEXT = '[FormExchange] DROP TEXT',

  SET_TRANSACTION_DATA_GIVE = '[FormExchange] SET TRANSACTION DATA GIVE',
  SET_TRANSACTION_DATA_RECEIVE = '[FormExchange] SET TRANSACTION DATA RECEIVE',

  SET_GIVE_CURRENCY = '[FormExchange] SET GIVE CURRENCY',
  SET_RECEIVE_CURRENCY = '[FormExchange] SET RECEIVE CURRENCY',

  SET_PERSON_DATA = '[FormExchange] SET PERSON DATA',

  EXCHANGE_CURRENCY = '[FormExchange] EXCHANGE CURRENCY',
  EXCHANGE_CURRENCY_SUCCESS = '[FormExchange] EXCHANGE CURRENCY SUCCESS',
  EXCHANGE_CURRENCY_FAIL = '[FormExchange] EXCHANGE CURRENCY FAIL',

  SET_FORM_VALUE = '[FormExchange] SET FORM VALUE',

  VALIDATE_FORM = '[FormExchange] VALIDATE FORM',
  VALIDATE_FORM_SUCCESS = '[FormExchange] VALIDATE FORM SUCCESS',
  VALIDATE_FORM_FAIL = '[FormExchange] VALIDATE FORM FAIL',


  SET_GIVE_VALUE = '[FormExchange] SET GIVE VALUE',
  SET_RECEIVE_VALUE = '[FormExchange] SET RECEIVE VALUE',
  UPDATE_CURRENCY_VALUE = '[FormExchange] UPDATE CURRENCY VALUE',
  UPDATE_FIELD_TRANSACTION = '[FormExchange] UPDATE FIELD TRANSACTION',
  CALCULATE_FAIL = '[FormExchange] CALCULATE FAIL',

  CALCULATE = '[FormExchange] CALCULATE',
  CALCULATE_INPUT = '[FormExchange] CALCULATE INPUT',
}

export class Calculate implements Action {
  readonly type = FormExchangeActionTypes.CALCULATE;
}

export class CalculateInput implements Action {
  readonly type = FormExchangeActionTypes.CALCULATE_INPUT;

  constructor(
    public mode: ECurrencyBlockTypes
  ) {
  }
}

export class GetFormFieldReceive implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE;

  constructor(
    public currencyId: number
  ) {
  }
}

export class GetFormFieldReceiveSuccess implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE_SUCCESS;

  constructor(
    public fields: IFieldType[]
  ) {
  }
}


export class GetFormFieldGive implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_GIVE;

  constructor(
    public currencyId: number
  ) {
  }
}

export class GetFormFieldGiveSuccess implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_GIVE_SUCCESS;

  constructor(
    public fields: IFieldType[]
  ) {
  }
}


export class GetFormFieldGiveFail implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_GIVE_FAIL;
}

export class GetFormFieldReceiveFail implements Action {
  readonly type = FormExchangeActionTypes.GET_FORM_FIELD_RECEIVE_FAIL;
}


export class GetText implements Action {
  readonly type = FormExchangeActionTypes.GET_TEXT;
}

export class GetTextSuccess implements Action {
  readonly type = FormExchangeActionTypes.GET_TEXT_SUCCESS;

  constructor(
    public text: ITextExchange
  ) {
  }
}

export class GetTextFail implements Action {
  readonly type = FormExchangeActionTypes.GET_TEXT_FAIL;
}

export class DropText implements Action {
  readonly type = FormExchangeActionTypes.DROP_TEXT;
}

export class DropField implements Action {
  readonly type = FormExchangeActionTypes.DROP_FIELD;

  constructor(
    public mode: ECurrencyBlockTypes
  ) {
  }
}

export class SetTransactionDataGive implements Action {
  readonly type = FormExchangeActionTypes.SET_TRANSACTION_DATA_GIVE;

  constructor(
    public data: IFieldType
  ) {
  }
}

export class SetTransactionDataReceive implements Action {
  readonly type = FormExchangeActionTypes.SET_TRANSACTION_DATA_RECEIVE;

  constructor(
    public data: IFieldType
  ) {
  }
}

export class SetGiveCurrency implements Action {
  readonly type = FormExchangeActionTypes.SET_GIVE_CURRENCY;

  constructor(
    public currency: IExchangeCurrency
  ) {
  }
}

export class SetReceiveCurrency implements Action {
  readonly type = FormExchangeActionTypes.SET_RECEIVE_CURRENCY;

  constructor(
    public currency: IExchangeCurrency
  ) {
  }
}

export class SetPersonData implements Action {
  readonly type = FormExchangeActionTypes.SET_PERSON_DATA;

  constructor(
    public info: IPersonInfo
  ) {
  }
}

export class ExchangeCurrency implements Action {
  readonly type = FormExchangeActionTypes.EXCHANGE_CURRENCY;
}

export class ExchangeCurrencySuccess implements Action {
  readonly type = FormExchangeActionTypes.EXCHANGE_CURRENCY_SUCCESS;

  constructor(
    public key: string
  ) {
  }
}

export class ExchangeCurrencyFail implements Action {
  readonly type = FormExchangeActionTypes.EXCHANGE_CURRENCY_FAIL;

  constructor(
    public error: HttpErrorResponse
  ) {
  }
}


export class SetFormValue implements Action {
  readonly type = FormExchangeActionTypes.SET_FORM_VALUE;

  constructor(public value) {
  }
}

export class ValidateForm implements Action {
  readonly type = FormExchangeActionTypes.VALIDATE_FORM;
}


export class ValidateFormSuccess implements Action {
  readonly type = FormExchangeActionTypes.VALIDATE_FORM_SUCCESS;
}


export class ValidateFormFail implements Action {
  readonly type = FormExchangeActionTypes.VALIDATE_FORM_FAIL;

  constructor(
    public error: string[]
  ) {
  }
}

export class SetGiveValue implements Action {
  readonly type = FormExchangeActionTypes.SET_GIVE_VALUE;

  constructor(
    public value: string
  ) {
  }
}

export class SetReceiveValue implements Action {
  readonly type = FormExchangeActionTypes.SET_RECEIVE_VALUE;

  constructor(
    public value: string
  ) {
  }
}


export class UpdateCurrencyValue implements Action {
  readonly type = FormExchangeActionTypes.UPDATE_CURRENCY_VALUE;

  constructor(
    public mode: ECurrencyBlockTypes,
    public value: string
  ) {
  }
}

export class UpdateFieldTransaction implements Action {
  readonly type = FormExchangeActionTypes.UPDATE_FIELD_TRANSACTION;

  constructor(
    public mode: ECurrencyBlockTypes,
    public fields: IFieldType[]
  ) {
  }
}

export class CalculateFail implements Action {
  readonly type = FormExchangeActionTypes.CALCULATE_FAIL;

  constructor(
    public error: HttpErrorResponse
  ) {
  }
}


export type FormExchangeActions =
  | GetFormFieldGive
  | GetFormFieldGiveSuccess
  | GetFormFieldGiveFail

  | GetFormFieldReceive
  | GetFormFieldReceiveSuccess
  | GetFormFieldReceiveSuccess

  | GetText
  | GetTextSuccess
  | GetTextFail

  | DropText
  | DropField

  | SetTransactionDataGive
  | SetTransactionDataReceive

  | SetGiveCurrency
  | SetReceiveCurrency

  | SetPersonData

  | ExchangeCurrency
  | ExchangeCurrencySuccess
  | ExchangeCurrencyFail

  | SetFormValue

  | ValidateForm
  | ValidateFormSuccess
  | ValidateFormFail

  | SetGiveValue
  | SetReceiveValue
  | UpdateCurrencyValue
  | UpdateFieldTransaction
  | CalculateFail

  | Calculate
  | CalculateInput

  ;
