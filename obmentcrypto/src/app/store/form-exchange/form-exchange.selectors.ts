import { IAppState } from '../reducer';
import { createSelector } from '@ngrx/store';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';

export const formExchange = (state: IAppState) => state.formExchange;

export const formLoading = createSelector(
  formExchange,
  (state) => {
    return state.loadingForm;
  }
);

export const fieldsGive = createSelector(
  formExchange,
  (state) => {
    return state.giveFields;
  }
);

export const fieldsReceive = createSelector(
  formExchange,
  (state) => {
    return state.receiveFields;
  }
);

export const showFormExchange = createSelector(
  formExchange,
  (state) => {
    return true;
    // return state.receiveFields.length > 0 && state.giveFields.length > 0;
  }
);

export const getTitle = createSelector(
  formExchange,
  (state) => {
    return state.textForm;
  }
);

export const getForm = createSelector(
  formExchange,
  (state) => {
    return state.form;
  }
);

export const getExchangeError = createSelector(
  formExchange,
  (state) => {
    return state.exchangeError && state.exchangeError.error ? state.exchangeError.error.errorDescription : null;
  }
);

export const getReceiveValue = createSelector(
  formExchange,
  (state) => {
    return state.receiveValue;
  }
);


export const getGiveValue = createSelector(
  formExchange,
  (state) => {
    return state.giveValue;
  }
);

export const getCalculateError = createSelector(
  formExchange,
  (state) => {
    return state.calculateError;
  }
);

export const getPersonInfo = createSelector(
  formExchange,
  (state) => {
    return state.personInfo;
  }
);

export const getTransationFields = createSelector(
  formExchange,
  (state, mode: ECurrencyBlockTypes) => {
    if (mode === ECurrencyBlockTypes.GIVE) {
      return state.giveTransactionData;
    } else {
      return state.receiveTransactionData;
    }
  }
);
