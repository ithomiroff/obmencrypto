import * as fromCurrency from './currency/currency.reducer';
import * as fromExchange from './exchange/exchange.reducer';
import * as fromLayout from './layout/layout.reducer';
import * as fromFormExchange from './form-exchange/form-exchange.reducer';
import * as fromApplication from './application/application.reducer';
import * as fromCabinet from './cabinet/cabinet.reducer';
import * as fromAuth from './auth/auth.reducer';
import * as fromArticles from './articles/articles.reducer';
import { ActionReducerMap } from '@ngrx/store';


export interface IAppState {
  currency: fromCurrency.ICurrencyState;
  exchange: fromExchange.IExchangeState;
  layout: fromLayout.ILayoutState;
  formExchange: fromFormExchange.IFormExchangeState;
  application: fromApplication.IApplicationState;
  cabinet: fromCabinet.ICabinetState;
  auth: fromAuth.IAuthState;
  articles: fromArticles.ArticlesState;
}


export const appReducer: ActionReducerMap<IAppState> = {
  currency: fromCurrency.reducer,
  exchange: fromExchange.reducer,
  layout: fromLayout.reducer,
  formExchange: fromFormExchange.reducer,
  application: fromApplication.reducer,
  cabinet: fromCabinet.reducer,
  auth: fromAuth.reducer,
  articles: fromArticles.reducer
};
