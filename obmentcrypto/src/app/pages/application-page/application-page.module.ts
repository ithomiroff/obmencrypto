import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationPageComponent } from './application-page.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../core/material/material.module';
import { StatusModule } from '../../components/status/status.component';
import { SpinnerModule } from '../../components/spinner/spinner.component';
import { PromoCodeComponent } from './components/promo-code/promo-code.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldsInfoComponent } from './components/fields-info/fields-info.component';

const routes = [
  {
    path: '',
    component: ApplicationPageComponent
  }
];

@NgModule({
  declarations: [
    ApplicationPageComponent,
    PromoCodeComponent,
    FieldsInfoComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    StatusModule,
    SpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class ApplicationPageModule { }
