import { Component, OnInit, Input } from '@angular/core';
import { IFieldType } from '@app/core/interfaces/field-type.interface';

@Component({
  selector: 'app-fields-info',
  templateUrl: './fields-info.component.html',
  styleUrls: ['./fields-info.component.scss']
})
export class FieldsInfoComponent implements OnInit {
  @Input()
  fields: IFieldType[];

  @Input()
  title: string;

  constructor() { }

  ngOnInit() {
  }

}
