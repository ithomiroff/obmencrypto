import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsInfoComponent } from './fields-info.component';

describe('FieldsInfoComponent', () => {
  let component: FieldsInfoComponent;
  let fixture: ComponentFixture<FieldsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
