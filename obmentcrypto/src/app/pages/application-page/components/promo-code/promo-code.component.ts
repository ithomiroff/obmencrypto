import { EStatusesApplication } from './../../../../core/enums/statuses-application.enum';
import { IAppState } from './../../../../store/reducer';
import { Store } from '@ngrx/store';
import { Component, OnInit, Output, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SetPromocodeApplication } from '@app/store/application/application.actions';

@Component({
  selector: 'app-promo-code',
  templateUrl: './promo-code.component.html',
  styleUrls: ['./promo-code.component.scss']
})
export class PromoCodeComponent implements OnInit {
  control: FormControl;
  appStatuses = EStatusesApplication;

  @Input()
  appStatus: EStatusesApplication;
  @Input()
  promoCode: string | undefined;

  constructor(
    private store: Store<IAppState>
    ) { }

  ngOnInit() {
  }

  SetPromo(promoCodeValue: string) {
    if (promoCodeValue) {
      this.store.dispatch(new SetPromocodeApplication(promoCodeValue));
    }
  }
}
