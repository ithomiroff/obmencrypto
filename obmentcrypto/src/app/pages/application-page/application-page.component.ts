import { AfterViewInit, Component, OnDestroy, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as appSelectors from '@app/store/application/application.selectors';
import * as appActions from '@app/store/application/application.actions';
import {IAppState} from '../../store/reducer';
import {select, Store} from '@ngrx/store';
import {BehaviorSubject, interval, Observable, Subscription} from 'rxjs';
import {filter, map, takeWhile, tap} from 'rxjs/operators';
import {EStatusesApplication} from '../../core/enums/statuses-application.enum';
import {DomSanitizer} from '@angular/platform-browser';
import {copyToClipboard} from '../../core/utils/clipboar-copy.function';
import * as moment from 'moment';
import {Duration} from 'moment';
import {timeToUpdateOrder} from '@app/core/constants';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Component({
  selector: 'app-application-page',
  templateUrl: './application-page.component.html',
  styleUrls: ['./application-page.component.scss']
})
export class ApplicationPageComponent implements OnInit, OnDestroy, AfterViewInit {
  status$ = this.store.pipe(select(appSelectors.getStatus));
  application$ = this.store.pipe(select(appSelectors.getApplication));

  loading$ = this.store.pipe(select(appSelectors.getLoading));

  key$ = this.store.pipe(select(appSelectors.getSecretKey));

  error$ = this.store.pipe(select(appSelectors.getError));

  time$ = new BehaviorSubject<string>(null);

  alive: boolean = true;

  appStatuses = EStatusesApplication;

  qrCodeSrc$: Observable<any>;

  timerSubscription: Subscription;
  orderUpdateSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<IAppState>,
    private sanitizer: DomSanitizer
  ) {
    const {key} = this.route.snapshot.params;
    if (key && isPlatformBrowser(this.platformId)) {
      this.store.dispatch(new appActions.GetApplicationInfo(key));

      this.orderUpdateSubscription = interval(timeToUpdateOrder).pipe(
        tap(() => this.store.dispatch(new appActions.GetApplicationInfo(key))),
      ).subscribe();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    this.application$.pipe(
      filter((app) => !!app),
      map((app) => {
        this.killTimer();
        if (app.timeExecution &&
          app.status !== EStatusesApplication.DECLINED &&
          app.status !== EStatusesApplication.PAUSED &&
          app.timeExecution !== 0) {
          this.runTimer(app.timeExecution);
        }
      }),
      takeWhile(() => this.alive)
    ).subscribe();
  }

  ngAfterViewInit(): void {
    copyToClipboard('.copy');
  }

  getDate(timestamp: number) {
    return new Date(timestamp * 1000);
  }

  runTimer(diff: number) {
    let duration = moment.duration(diff * 1000, 'milliseconds');

    this.timerSubscription = interval(1000)
      .pipe(
        map(() => {
          // @ts-ignore
          duration = moment.duration(duration - 1000, 'milliseconds');
          this.time$.next(this.getFormateDate(duration));
        }),
      ).subscribe();
  }

  killTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.time$.next(null);
  }

  getImage(base64Img: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpg;base64,${base64Img}`);
  }

  getFormateDate(duration: Duration): string {
    let res = '';
    if (duration.hours()) {
      res += `${duration.hours()} ч. `;
    }
    res += `${duration.minutes()} мин. ${duration.seconds()} сек. `;
    return res;
  }

  ngOnDestroy(): void {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    this.orderUpdateSubscription.unsubscribe();
    this.killTimer();
    this.alive = false;
    this.store.dispatch(new appActions.DropApplication());
  }

  cancel() {
    this.store.dispatch(new appActions.CancelApplication());
  }

  paid() {
    this.store.dispatch(new appActions.PaidApplication());
  }

  makePayment(url: string) {
    window.open(url);
  }
}
