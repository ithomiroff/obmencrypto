import { mergeMap, map, catchError } from 'rxjs/operators';
import { HttpService } from './../../../core/services/http.service';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.scss']
})
export class FeedbackPageComponent implements OnInit {
  form: FormGroup;
  sendResultMessage = new BehaviorSubject<string>(null);

  constructor(
    private httpService: HttpService
  ) {
    this.form = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      text: new FormControl(null, [
        Validators.required,
        Validators.minLength(10)
      ])
    });
   }

  ngOnInit() {
  }

  sendFeedback() {
    console.log('check valid');
    if (this.form.valid) {
      console.log('try post');
      this.httpService
        .post('/feedback', this.form.value)
        .subscribe(
          data =>  this.sendResultMessage.next(data),
          response => {
            console.log(response);
            this.sendResultMessage.next(response.error);
          });
    }
  }
}
