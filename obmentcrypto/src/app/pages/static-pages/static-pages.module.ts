import { NgxMaskModule } from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../../core/material/material.module';
import { PartnerInfoPageComponent } from './partner-info-page/partner-info-page.component';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
export { FaqPageComponent, PartnerInfoPageComponent, ContactsPageComponent };
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule, MatButtonModule } from '@angular/material';
import { StaticPageComponent } from './components/static-page/static-page.component';
import { RouterModule } from '@angular/router';
import { PrivacyPageComponent } from './privacy-page/privacy-page.component';
import { FeedbackPageComponent } from './feedback-page/feedback-page.component';

const routes = [
  {
    path: 'partnerdesc',
    component: PartnerInfoPageComponent,
    name: 'partnerdesc'
  },
  {
    path: 'contacts',
    component: ContactsPageComponent,
    name: 'contacts'
  },
  {
    path: 'faq',
    component: FaqPageComponent,
    name: 'FAQ'
  },
  {
    path: 'privacy',
    component: PrivacyPageComponent
  },
  {
    path: 'feedback',
    component: FeedbackPageComponent
  }
];

@NgModule({
  declarations: [
    FaqPageComponent,
    PartnerInfoPageComponent,
    ContactsPageComponent,
    StaticPageComponent,
    PrivacyPageComponent,
    FeedbackPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatExpansionModule,
    MatButtonModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxMaskModule.forRoot()
  ],
  exports: [
    FaqPageComponent,
    PartnerInfoPageComponent,
    ContactsPageComponent,
  ]
})
export class StaticPagesModule { }
