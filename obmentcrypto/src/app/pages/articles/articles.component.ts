import { Component, OnInit, ChangeDetectionStrategy, ViewChild, OnDestroy, PLATFORM_ID, Inject, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import * as articlesSelectors from '@app/store/articles/articles.selectors';
import { LoadArticleDescriptions, LoadArticle } from '@app/store/articles/articles.actions';
import { MatSidenav, MatButton } from '@angular/material';
import { IArticleDescription } from '@app/core/interfaces/article.interface';
import { Subscription } from 'rxjs';
import { isPlatformServer } from '@angular/common';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class ArticlesComponent implements OnInit, OnDestroy {
  @ViewChild('articleMenuBtn') menuBtn: ElementRef;

  loading$ = this.store.pipe(select(articlesSelectors.isCurrentArticleLoading));
  articleDescriptions$ = this.store.pipe(select(articlesSelectors.getArticleDescriptions));
  currentArticle$  = this.store.pipe(select(articlesSelectors.getCurrentArticle));

  updatePageSub: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<IAppState>,
    private router: Router,
    private route: ActivatedRoute) {
      this.updatePageSub = this.router.events
      .subscribe(e => {
        if (e instanceof NavigationEnd) {
          const url = this.router.url;
          console.log(url);
          if (url.startsWith('/articles/')) {
            this.LoadArticleFromUrl();
          }
        }
      });
    }


  ngOnInit() {
    this.LoadArticleFromUrl();
  }

  private LoadArticleFromUrl() {
    const url: string = this.GetCurrentUrlParams();
    if (url) {
      this.store.dispatch(new LoadArticle(url));
    }
  }

  private GetCurrentUrlParams(): string {
    return this.route.snapshot.params.url;
  }

  ngOnDestroy(): void {
    this.updatePageSub.unsubscribe();
  }

  private GetArticleLink(desc: IArticleDescription) {
    return `/articles/${desc.urlKey}`;
  }
}
