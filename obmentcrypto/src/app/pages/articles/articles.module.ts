import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles.component';
import { MaterialModule } from '@app/core/material/material.module';
import { SpinnerModule } from '@app/components/spinner/spinner.component';

const routes: Routes = [
  { path: 'articles/:url', component: ArticlesComponent },
  { path: 'articles', component: ArticlesComponent }
];

@NgModule({
  declarations: [
    ArticlesComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SpinnerModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ArticlesModule { }
