import { Component, OnDestroy, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IAppState } from '../../store/reducer';
import { getAuthUser } from '../../store/auth/auth.selectors';
import { filter, takeWhile, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.scss']
})
export class CabinetComponent implements OnInit, OnDestroy {

  user$ = this.store.pipe(select(getAuthUser));

  alive = true;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<IAppState>,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.user$.pipe(
        filter((user) => !!user && !!!user.email && !!!user.username),
        tap(() => this.router.navigateByUrl('/')),
        takeWhile(() => this.alive)
      ).subscribe();
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

}
