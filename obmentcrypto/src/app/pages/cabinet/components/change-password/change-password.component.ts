import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as cabinetActions from '../../../../store/cabinet/cabinet.actions';
import * as cabinetSelectors from '../../../../store/cabinet/cabinet.selectors';
import { IAppState } from '../../../../store/reducer';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;

  loading$ = this.store.pipe(select(cabinetSelectors.getLoading));

  constructor(
    private store: Store<IAppState>
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  onClick() {
    if (this.form.valid) {
      this.store.dispatch(new cabinetActions.ChangePassword(this.form.value));
    }
  }

  createForm(): void {
    this.form = new FormGroup({
      currentPassword: new FormControl(null, Validators.required),
      newPassword: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
    });
  }
}
