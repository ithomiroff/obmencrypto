import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IAppState } from '@app/store/reducer';
import { select, Store } from '@ngrx/store';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as authSelectors from '@app/store/auth/auth.selectors';
import * as cabinetActions from '@app/store/cabinet/cabinet.actions';
import * as cabinetSelectors from '../../../../store/cabinet/cabinet.selectors';
import { IUser } from '@app/core/interfaces/user.interface';
import { map, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  form: FormGroup;
  loading$ = this.store.pipe(select(cabinetSelectors.getLoading));

  alive = true;

  constructor(
    private router: Router,
    private store: Store<IAppState>
  ) {
  }

  ngOnInit() {
    this.store.pipe(
      select(authSelectors.getAuthUser),
      map((user) => this.createForm(user)),
    ).subscribe().unsubscribe();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  createForm(userInfo: IUser): void {
    this.form = new FormGroup({
      email: new FormControl({value: userInfo.email, disabled: true}, Validators.required),
      username: new FormControl(userInfo.username, Validators.required),
      phone: new FormControl(userInfo.phone, Validators.required),
      telegram: new FormControl(userInfo.telegram, Validators.required)
    });
  }

  onClick() {
    if (this.form.valid) {
      this.store.dispatch(new cabinetActions.ChangeUserData(this.form.value));
    }
  }

  get errorMessage(): string {
    return 'Обязательно';
  }
}
