import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import * as cabinetSelectors from '@app/store/cabinet/cabinet.selectors';
import * as cabinetActions from '@app/store/cabinet/cabinet.actions';
import { IAppState } from '../../../../store/reducer';
import { select, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-my-applications',
  templateUrl: './my-applications.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class MyApplicationsComponent implements OnInit {

  applications$ = this.store.pipe(select(cabinetSelectors.userApplications));

  loading$ = this.store.pipe(select(cabinetSelectors.loadingMyApplications));

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<IAppState>,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.store.dispatch(new cabinetActions.LoadApplications());
    }
  }

  onClick() {
    this.router.navigateByUrl('/');
  }
}
