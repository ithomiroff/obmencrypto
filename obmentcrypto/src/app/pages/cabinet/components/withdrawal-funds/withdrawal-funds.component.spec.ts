import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalFundsComponent } from './withdrawal-funds.component';

describe('WithdrawalFundsComponent', () => {
  let component: WithdrawalFundsComponent;
  let fixture: ComponentFixture<WithdrawalFundsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithdrawalFundsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalFundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
