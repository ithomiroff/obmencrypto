import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as cabinetSelectors from '../../../../store/cabinet/cabinet.selectors';
import { IAppState } from '../../../../store/reducer';
import * as cabinetActions from '../../../../store/cabinet/cabinet.actions';

@Component({
  selector: 'app-withdrawal-funds',
  templateUrl: './withdrawal-funds.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class WithdrawalFundsComponent implements OnInit {

  affiliateWithdral$ = this.store.pipe(select(cabinetSelectors.affiliateWithdral));

  apps$ = this.store.pipe(select(cabinetSelectors.withdrawalApplication));
  lockCreateWithdrawal$ = this.store.pipe(select(cabinetSelectors.lockCreateWithdrawal));

  constructor(
    private store: Store<IAppState>
  ) {
    this.store.dispatch(new cabinetActions.LoadAffiliateInfo());
    this.store.dispatch(new cabinetActions.LoadWithdrawalApplications());
  }

  ngOnInit() {
  }

  onClick(wallet: string) {
    this.store.dispatch(new cabinetActions.CreateWithdrawal(wallet));
  }
}
