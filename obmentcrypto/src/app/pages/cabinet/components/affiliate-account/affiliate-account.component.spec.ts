import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliateAccountComponent } from './affiliate-account.component';

describe('AffiliateAccountComponent', () => {
  let component: AffiliateAccountComponent;
  let fixture: ComponentFixture<AffiliateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliateAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
