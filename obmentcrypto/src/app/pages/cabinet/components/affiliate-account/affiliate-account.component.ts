import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as cabinetSelectors from '@app/store/cabinet/cabinet.selectors';
import * as cabinetActions from '@app/store/cabinet/cabinet.actions';
import { IAppState } from '../../../../store/reducer';
import { select, Store } from '@ngrx/store';
import { copyToClipboard } from '../../../../core/utils/clipboar-copy.function';

@Component({
  selector: 'app-affiliate-account',
  templateUrl: './affiliate-account.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class AffiliateAccountComponent implements OnInit, AfterViewInit {

  applications$ = this.store.pipe(select(cabinetSelectors.affiliateUserApplications));

  personLink$ = this.store.pipe(select(cabinetSelectors.personLink));
  partnerText$ = this.store.pipe(select(cabinetSelectors.partnerText));
  affiliateWithdral$ = this.store.pipe(select(cabinetSelectors.affiliateWithdral));

  loading$ = this.store.pipe(select(cabinetSelectors.loadingAffiliate));

  constructor(
    private store: Store<IAppState>
  ) {
    this.store.dispatch(new cabinetActions.LoadApplicationsAffiliate());
    this.store.dispatch(new cabinetActions.LoadAffiliateInfo());
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    copyToClipboard('.copy');
  }
}
