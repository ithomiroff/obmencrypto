import {Component, Input, OnInit} from '@angular/core';
import {IApplicationInfo} from '../../../../core/interfaces/application-info.interface';
import {MatTableDataSource} from '@angular/material';
import {IWithdrawalApplication} from '../../../../core/interfaces/cabinet/withdrawal-application.interface';
import { ApplicationsTableMode } from '@app/core/enums/applications-table-mode';


@Component({
  selector: 'app-applications-table',
  templateUrl: './applications-table.component.html',
  styleUrls: ['../../cabinet.component.scss']
})
export class ApplicationsTableComponent implements OnInit {

  displayedColumnsUser: string[] = ['date', 'give', 'receive', 'status'];
  displayedColumnsPartners: string[] = ['date', 'give', 'receive', 'status', 'reward'];
  displayedColumnsWithdrawal: string[] = ['date', 'purse', 'total', 'status'];

  tableData;

  @Input()
  mode: ApplicationsTableMode = ApplicationsTableMode.UserApps;

  @Input()
  set data(value: IApplicationInfo[] | IWithdrawalApplication[] | any) {
    if (value) {
      this.tableData = new MatTableDataSource(value);
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

  getDateUtc(date: number): Date {
    return new Date(date* 1000);
  }

}
