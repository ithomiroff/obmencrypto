import { NgxMaskModule } from 'ngx-mask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabinetComponent } from './cabinet.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../core/material/material.module';
import { MyApplicationsComponent } from './components/my-applications/my-applications.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AffiliateAccountComponent } from './components/affiliate-account/affiliate-account.component';
import { WithdrawalFundsComponent } from './components/withdrawal-funds/withdrawal-funds.component';
import { StatusModule } from '../../components/status/status.component';
import { ApplicationsTableComponent } from './components/applications-table/applications-table.component';
import { SpinnerModule } from '@app/components/spinner/spinner.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CabinetGuard } from '@app/core/guards/cabinet.guard';

const routes = [
  {
      path: '',
      component: CabinetComponent,
      canActivate: [CabinetGuard]
  }
];

@NgModule({
  declarations: [
    CabinetComponent,
    MyApplicationsComponent,
    ChangePasswordComponent,
    SettingsComponent,
    AffiliateAccountComponent,
    WithdrawalFundsComponent,
    ApplicationsTableComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NgxMaskModule,
    StatusModule,
    SpinnerModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ]
})
export class CabinetModule {
}
