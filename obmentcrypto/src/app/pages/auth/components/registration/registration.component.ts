import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IAppState } from '../../../../store/reducer';
import { select, Store } from '@ngrx/store';
import * as authSelectors from '@app/store/auth/auth.selectors';
import * as authActions from '@app/store/auth/auth.actions';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  loading$ = this.store.pipe(select(authSelectors.getLoadingLogin));

  isRegister$ = this.store.pipe(select(authSelectors.getRegisterStatus));

  form: FormGroup;

  constructor(
    private store: Store<IAppState>
  ) {
    this.form = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ]),
      passwordConfirm: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ]),
      phone: new FormControl(null),
      telegram: new FormControl(null),
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.form.valid) {
      this.store.dispatch(new authActions.Registration(this.form.value));
    }
  }

  get errorMessage(): string {
    return 'Поле введено некоректно';
  }
}
