import { Component, OnInit } from '@angular/core';
import * as authSelectors from '@app/store/auth/auth.selectors';
import * as authActions from '@app/store/auth/auth.actions';
import { IAppState } from '@app/store/reducer';
import { select, Store } from '@ngrx/store';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { filter, map, tap } from 'rxjs/operators';
import { EAppUrls } from '../../../../core/enums/app-urls.enum';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  loading$ = this.store.pipe(select(authSelectors.getLoadingLogin));

  isLoggedIn = this.store.pipe(select(authSelectors.isUserLoggedIn));

  form: FormGroup;

  constructor(
    private store: Store<IAppState>,
    private router: Router
  ) {
    this.form = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  ngOnInit() {
    this.isLoggedIn.pipe(
      filter((status) => !!status),
      tap(() => this.router.navigateByUrl('/'))
    )
    .subscribe()
    .unsubscribe();
  }

  onClick() {
    if (this.form.valid) {
      this.store.dispatch(new authActions.Logging(this.form.value));
    }
  }

  goToForgot() {
    this.router.navigateByUrl(`${EAppUrls.USER}/${EAppUrls.RESET}`);
  }

  goToRegistration() {
    this.router.navigateByUrl(`${EAppUrls.USER}/${EAppUrls.REG}`);
  }
}
