import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IAppState} from '../../../../store/reducer';
import { select, Store } from '@ngrx/store';
import * as authActions from '../../../../store/auth/auth.actions';
import * as authSelectors from '../../../../store/auth/auth.selectors';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  template$ = new BehaviorSubject<'alive' | 'reset'>(null);

  loadingReset$ = this.store.pipe(select(authSelectors.getLoadingReset));

  formAlive = new FormControl(null, [
    Validators.required,
    Validators.email
  ]);

  formReset: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private store: Store<IAppState>
  ) {
  }

  ngOnInit() {
    const {code} = this.route.snapshot.params;
    if (code) {
      this.template$.next('reset');
      this.createForm(code);
    } else {
      this.template$.next('alive');
    }
  }

  createForm(code: string): void {
    this.formReset = new FormGroup({
      code: new FormControl(code),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, Validators.required),
      passwordConfirm: new FormControl(null, Validators.required),
    });
  }

  onReset() {
    if (this.formReset && this.formReset.valid) {
      this.store.dispatch(new authActions.ResetPassword(this.formReset.value));
    }
  }

  onAlive() {
    if (this.formAlive.valid) {
      this.store.dispatch(new authActions.AlivePassword(this.formAlive.value));
    }
  }
}
