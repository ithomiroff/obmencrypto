import { encodeUriQuery } from '@angular/router/src/url_tree';
import { Component, OnInit } from '@angular/core';
import * as authActions from '@app/store/auth/auth.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<IAppState>,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    let {code, userId} = this.route.snapshot.params;
    code = encodeURIComponent(code);
    if (code && userId) {
      this.store.dispatch(new authActions.ConfirmEmail({code, userId}));
    } else {
      this.router.navigateByUrl('/');
    }
  }

}
