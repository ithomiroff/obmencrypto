import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthComponent } from './components/auth/auth.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../core/material/material.module';
import { NgxMaskModule } from 'ngx-mask';
import { SpinnerModule } from '@app/components/spinner/spinner.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ConfirmEmailComponent } from './components/confirm-email/confirm-email.component';

const routes = [
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'resetpassword',
    component: ResetPasswordComponent
  },
  {
    path: 'resetpassword/:code',
    component: ResetPasswordComponent
  },
  {
    path: 'confirm-email/:userId/:code',
    component: ConfirmEmailComponent
  },
];

@NgModule({
  declarations: [
    RegistrationComponent,
    AuthComponent,
    ResetPasswordComponent,
    ConfirmEmailComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SpinnerModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskModule.forRoot()
  ],
  entryComponents: [
  ]
})
export class AuthModule {
}
