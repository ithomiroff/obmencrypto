import { ApplicationModule, NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@app/core/material/material.module';
import { CurrencyBlogModule } from '@app/components/currency-blog/currency-blog.component';
import { CurrencyFormModule } from '@app/components/currency-form/currency-form.component';
import { CurrencySearchModule } from '@app/components/currency-search/currency-search.component';
import { InputModule } from '@app/components/input/input.component';
import { FiltersModule } from '@app/components/filters/filters.component';
import { ReactiveFormsModule } from '@angular/forms';
import {DirectivesModule} from '@app/core/directives/directives.module';
import {PipesModule} from '@app/core/pipes/pipes.module';

const routes = [
  {
    path: '',
    component: MainPageComponent,
    name: 'home'
  },
  {
    path: ':params',
    component: MainPageComponent,
    name: 'home'
  },

];

@NgModule({
  declarations: [
    MainPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    CurrencyBlogModule,
    CurrencyFormModule,
    CurrencySearchModule,
    InputModule,
    FiltersModule,
    ReactiveFormsModule,
    ApplicationModule,
    DirectivesModule,
    PipesModule
  ]
})
export class MainModule {}
