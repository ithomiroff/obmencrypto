import { CurrencyFormComponent } from './../../components/currency-form/currency-form.component';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild, PLATFORM_ID, Inject } from '@angular/core';
import { HttpService } from '../../core/services/http.service';
import { select, Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import * as currencySelectors from '@app/store/currency/currency.selectors';
import * as currencyActions from '@app/store/currency/currency.actions';
import * as exchangeSelectors from '@app/store/exchange/exchange.selectors';
import * as exchangeActions from '@app/store/exchange/exchange.actions';
import * as layoutActions from '@app/store/layout/layout.actions';
import * as layoutSelectors from '@app/store/layout/layout.selectors';
import * as formExchangeSelectors from '@app/store/form-exchange/form-exchange.selectors';
import * as formExchangeActions from '@app/store/form-exchange/form-exchange.actions';
import { IFilters } from '@app/core/interfaces/filters.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { combineLatest, interval, Subscription, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, takeWhile, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { minMaxValidator, emailOrTelegramValidator } from '@app/core/utils/validators.functions';
import { timeToUpdateCalculatedValues, timeToUpdateCurrency } from '../../core/constants';
import { MobileListComponent } from '@app/components/mobile-list/mobile-list.component';
import { MatBottomSheet, MatDialog } from '@angular/material';
import { isPlatformBrowser } from '@angular/common';
import { showNotWorkingTimeModal } from '@app/components/modals/modal-not-working-time/modal-not-working-time.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {

  modesBlock = ECurrencyBlockTypes;

  receiveFilters$ = this.store.pipe(select(currencySelectors.getFilters, ECurrencyBlockTypes.RECEIVE));
  giveFilters$ = this.store.pipe(select(currencySelectors.getFilters, ECurrencyBlockTypes.GIVE));

  activeReceiveFilter$ = this.store.pipe(select(currencySelectors.getActiveFilters, ECurrencyBlockTypes.RECEIVE));
  activeGiveFilter$ = this.store.pipe(select(currencySelectors.getActiveFilters, ECurrencyBlockTypes.GIVE));

  currenciesReceive$ = this.store.pipe(select(currencySelectors.getCurrencies, ECurrencyBlockTypes.RECEIVE));
  currenciesGive$ = this.store.pipe(select(currencySelectors.getCurrencies, ECurrencyBlockTypes.GIVE));


  activeReceiveCurrency$ = this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.RECEIVE));
  activeGiveCurrency$ = this.store.pipe(select(exchangeSelectors.activeCurrency, ECurrencyBlockTypes.GIVE));

  valueInputGive$ = this.store.pipe(select(exchangeSelectors.activeAmountValue, ECurrencyBlockTypes.GIVE));
  valueInputReceive$ = this.store.pipe(select(exchangeSelectors.activeAmountValue, ECurrencyBlockTypes.RECEIVE));

  validateInputGive$ = this.store.pipe(select(exchangeSelectors.validateField, ECurrencyBlockTypes.GIVE));
  validateInputReceive$ = this.store.pipe(select(exchangeSelectors.validateField, ECurrencyBlockTypes.RECEIVE));

  isMobile$ = this.store.pipe(select(layoutSelectors.isMobile));

  showForm$ = this.store.pipe(select(exchangeSelectors.showFormExchange));

  calculateError$ = this.store.pipe(select(formExchangeSelectors.getCalculateError));

  giveValue$ = this.store.pipe(select(formExchangeSelectors.getGiveValue));
  receiveValue$ = this.store.pipe(select(formExchangeSelectors.getReceiveValue));

  alive = true;

  exhangeForm: FormGroup;

  calculateSubscription: Subscription;

  @ViewChild('inputGive') inputGive: ElementRef;

  @ViewChild('inputReceive') inputReceive: ElementRef;

  @ViewChild('dataForm') dataForm: CurrencyFormComponent;

  timer$: Observable<number>;

  timerSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private httpService: HttpService,
    private store: Store<IAppState>,
    private route: ActivatedRoute,
    private router: Router,
    private bottomList: MatBottomSheet
  ) {
    this.createForm();
    this.CheckNotFound();
    if (isPlatformBrowser(this.platformId)) {
      this.timer$ = interval(timeToUpdateCalculatedValues).pipe(
        filter(() => !!this.giveControl.value && !!this.receiveControl.value),
        tap(() => this.store.dispatch(new formExchangeActions.Calculate()))
      );

      this.calculateSubscription = interval(timeToUpdateCurrency).pipe(
        map(() => this.store.dispatch(new currencyActions.LoadCurrency()))
      ).subscribe();
    }
    this.store.dispatch(new currencyActions.LoadCurrency());
    this.store.dispatch(new layoutActions.DeviceSet());
    // console.log('main page loaded');
    // this.store.dispatch(new currencyActions.LoadToolbarCurrency());

    // interval(timeToUpdateCalculatedValues).pipe(
    //   tap(() => {
    //     if (this.giveControl.value && this.receiveControl.value) {
    //       this.store.dispatch(new formExchangeActions.Calculate());
    //     }
    //   }),
    //   takeWhile(() => this.alive)
    // ).subscribe();
  }

  ngOnInit() {
    if (this.isBrowser()) {
      this.setActiveValuesFromUrl();
      this.runTimer();
    }
  }

  ngOnDestroy(): void {
    if (this.isBrowser()) {
      this.alive = false;
      this.calculateSubscription.unsubscribe();
      this.dropTimer();
    }
  }

  private isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  runTimer(): void {
    if (this.isBrowser()) {
      this.timerSubscription = this.timer$.subscribe();
    }
  }

  dropTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
  private CheckNotFound() {
    const {params} = this.route.snapshot.params;
    if (!params) {
      return;
    }
    const args = params.toUpperCase().split('-');
    if (args.length !== 2) {
      this.router.navigateByUrl('/notfound');
    }
  }

  get giveControl(): AbstractControl {
    return this.exhangeForm && this.exhangeForm.get('giveCurrency');
  }

  get receiveControl(): AbstractControl {
    return this.exhangeForm && this.exhangeForm.get('receiveCurrency');
  }

  get giveArrayControls(): AbstractControl {
    return this.exhangeForm && this.exhangeForm.get('giveTransactionData');
  }

  get receiveArrayControls(): AbstractControl {
    return this.exhangeForm && this.exhangeForm.get('receiveTransactionData');
  }


  createForm(): void {
    this.exhangeForm = new FormGroup({
      giveCurrency: new FormControl(null, [
        Validators.required
      ]),
      receiveCurrency: new FormControl(null, [
        Validators.required
      ]),
      giveTransactionData: new FormArray([]),
      receiveTransactionData: new FormArray([])
    });
    this.activeGiveCurrency$.pipe(
      filter((cur) => !!cur && !!cur.id && !!cur.minValue && !!cur.maxValue),
      map((cur) => {
        //console.log('give changed');
        this.giveControl.setValidators([
          Validators.required,
          minMaxValidator(cur.minValue, cur.maxValue)
        ]);
      }),
      takeWhile(() => this.alive)
    ).subscribe();

    this.activeReceiveCurrency$.pipe(
      filter((cur) => !!cur && !!cur.id && !!cur.minValue && !!cur.maxValue),
      map((cur) => {
        this.receiveControl.setValidators([
          Validators.required,
          minMaxValidator(cur.minValue, cur.maxValue)
        ]);
      }),
      takeWhile(() => this.alive)
    ).subscribe();

    this.receiveValue$.pipe(
      filter((v) => !!v),
      tap((value) => {
        this.receiveControl.setValue(value);
        this.receiveControl.markAsTouched();
      }),
      takeWhile(() => this.alive)
    ).subscribe();

    this.giveValue$.pipe(
      filter((v) => !!v),
      tap((value) => {
        this.giveControl.setValue(value);
        this.giveControl.markAsTouched();
      }),
      takeWhile(() => this.alive)
    ).subscribe();

    this.giveArrayControls.valueChanges.pipe(
      distinctUntilChanged(),
      //debounceTime(500),
      tap((ar) => this.store.dispatch(new formExchangeActions.UpdateFieldTransaction(ECurrencyBlockTypes.GIVE, ar))),
      takeWhile(() => this.alive)
    ).subscribe();

    this.receiveArrayControls.valueChanges.pipe(
      distinctUntilChanged(),
      //debounceTime(500),
      tap((ar) => this.store.dispatch(new formExchangeActions.UpdateFieldTransaction(ECurrencyBlockTypes.RECEIVE, ar))),
      takeWhile(() => this.alive)
    ).subscribe();

    this.calculateError$.pipe(
      tap((err) => {
        if (err) {
          this.giveControl.setErrors({serverError: err});
          this.receiveControl.setErrors({serverError: err});
        } else {
          this.giveControl.setErrors({
            ...this.giveControl.errors,
            serverError: null
          });
          this.receiveControl.setErrors({
            ...this.receiveControl.errors,
            serverError: null
          });
        }
      }),
      takeWhile(() => this.alive)
    ).subscribe();
  }

  setActiveValuesFromUrl(): void {
    const sub = combineLatest(
      this.currenciesGive$,
      this.currenciesReceive$
    ).pipe(
      map(([give, receive]) => {
        if (give.length && receive.length) {
          const {params} = this.route.snapshot.params;
          if (params) {
            const ar = params.toUpperCase().split('-');
            if (ar.length === 2) {
              const giveCur = give.filter(item => item.currencyCode === ar[0])[0];
              const receiveCur = receive.filter(item => item.currencyCode === ar[1])[0];
              if (giveCur) {
                this.onSelectCurrency(giveCur, ECurrencyBlockTypes.GIVE);
              }
              if (receiveCur) {
                this.onSelectCurrency(receiveCur, ECurrencyBlockTypes.RECEIVE);
              }
              this.deleteSub(sub);
            }
          }
          this.deleteSub(sub);
        }
      })
    ).subscribe();
  }

  deleteSub(sub: Subscription): void {
    if (sub) {
      sub.unsubscribe();
    }
  }

  updateControls(mode: ECurrencyBlockTypes): void {
    if (mode === ECurrencyBlockTypes.GIVE && this.giveControl.value) {
      this.store.dispatch(new formExchangeActions.SetGiveValue(this.giveControl.value));
      this.giveControl.updateValueAndValidity({onlySelf: true});
    }
    if (mode === ECurrencyBlockTypes.RECEIVE && this.receiveControl.value) {
      this.store.dispatch(new formExchangeActions.SetReceiveValue(this.receiveControl.value));
      this.receiveControl.updateValueAndValidity({onlySelf: true});
    }

    if (this.giveControl.value) {
      this.giveControl.markAsTouched();
    }
    this.store.dispatch(new formExchangeActions.CalculateInput(mode));
    this.dropTimer();
    setTimeout(() => this.runTimer(), 0);
  }

  onSelectCurrency(currency: IExchangeCurrency, mode: ECurrencyBlockTypes) {
    if (!currency.isActive) {
      return;
    }
    const getFieldAction = mode === ECurrencyBlockTypes.GIVE ?
    new formExchangeActions.GetFormFieldGive(currency.id) :
    new formExchangeActions.GetFormFieldReceive(currency.id);
    this.store.dispatch(getFieldAction);
    this.store.dispatch(new exchangeActions.SetActiveCurrency(mode, currency));
    if (mode === ECurrencyBlockTypes.GIVE) {
      this.store.dispatch(new currencyActions.LoadCurrency());
    }
    this.updateControls(mode);
  }

  onOpenMobileList(mode: ECurrencyBlockTypes) {
    this.bottomList.open(MobileListComponent, {
      data: {mode}
    })
    .afterDismissed()
    .pipe(
      filter((res) => !!res),
      tap(({currency}) => this.onSelectCurrency(currency, mode))
    ).subscribe();
  }

  onInputAmountValue($event: number, mode: ECurrencyBlockTypes) {
    this.updateControls(mode);
  }

  // tslint:disable-next-line:no-shadowed-variable
  onClickFilter(filter: IFilters, mode: ECurrencyBlockTypes) {
    if (mode === ECurrencyBlockTypes.RECEIVE) {
      return this.store.dispatch(new currencyActions.SetActiveReceiveFilter(filter));
    }
    if (mode === ECurrencyBlockTypes.GIVE) {
      return this.store.dispatch(new currencyActions.SetActiveGiveFilter(filter));
    }
  }
}
