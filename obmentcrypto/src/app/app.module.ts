import { AddCsrfHeaderInterceptorService } from './core/csrf.interceptor';
import { StaticPagesModule } from './pages/static-pages/static-pages.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './components/app/app.component';
import { MaterialModule } from './core/material/material.module';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HeaderModule } from '@app/components/header/header.component';
import { AppStoreModule } from '@app/store/app-store.module';
import { MobileListModule } from './components/mobile-list/mobile-list.component';
import { CurrencyToolbarModule } from './components/currency-toolbar/currency-toolbar.component';
import { SpinnerModule } from './components/spinner/spinner.component';
import { AuthInterceptor } from './core/auth.interceptor';
import { PartnerLinkComponent } from './components/partner-link/partner-link.component';
import { FooterModule } from './components/footer/footer.component';
import { MenuModule } from './components/menu/menu.component';
import { CookieService } from 'ngx-cookie-service';
import { ArticlesModule } from './pages/articles/articles.module';
import { NotFoundModule } from './pages/not-found/not-found.module';
import { ModalsModule } from './components/modals/modals.module';

const routing: Routes = [
  {
    path: '',
    loadChildren: './pages/main/main.module#MainModule'
  },
  {
    path: 'order-info/:key',
    loadChildren: './pages/application-page/application-page.module#ApplicationPageModule'
  },
  {
    path: 'setpartner/:partnerId',
    component: PartnerLinkComponent
  },
  {
    path: 'user',
    loadChildren: './pages/auth/auth.module#AuthModule'
  },
  {
    path: 'user/cabinet',
    loadChildren: './pages/cabinet/cabinet.module#CabinetModule'
  },
  {
    path: 'articles',
    loadChildren: './pages/articles/articles.module#ArticlesModule'
  },
  {
    path: '**',
    redirectTo: '/notfound'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PartnerLinkComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    RouterModule.forRoot(routing),
    HeaderModule,
    AppStoreModule,
    MobileListModule,
    StaticPagesModule,
    ArticlesModule,
    CurrencyToolbarModule,
    SpinnerModule,
    FooterModule,
    MenuModule,
    ModalsModule,
    NotFoundModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddCsrfHeaderInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
