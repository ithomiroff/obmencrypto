import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  NgModule,
  OnInit,
  Output,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { NgxMaskModule } from 'ngx-mask';
import { Subject } from 'rxjs';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { DirectivesModule } from '../../core/directives/directives.module';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { showNotEnoughReserveModal } from '../modals/modal-not-enough-reserve/modal-not-enough-reserve.component';
import { MatDialog } from '@angular/material';
import { NotEnoughReserveLinkModule } from '../not-enough-reserve-link/not-enough-reserve-link.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnInit {

  @Input()
  activeCurrency: IExchangeCurrency;

  @Input()
  control: FormControl;

  @Input()
  amountValue: number;

  @Input()
  validate: boolean;

  @Input()
  error: string;

  @Input()
  mode: ECurrencyBlockTypes;

  modes = ECurrencyBlockTypes;

  @Output()
  inputValueAmount = new EventEmitter<number>();
  debounce: Subject<any> = new Subject();

  visibleTooltip: boolean;
  constructor(
    private dialog: MatDialog) {
    this.debounce.pipe(
      //debounceTime(500),
      //filter(() => !!this.control.valid),
      tap((value) => this.inputValueAmount.emit(value))
    ).subscribe();
  }

  ngOnInit() {
  }

  get title(): string {
    return this.mode === ECurrencyBlockTypes.GIVE ? 'Отдаю' : 'Получаю';
  }

  get placeholder(): string {
    return 'Введите сумму';
  }

  onInputCurrencyValue(target: { value: string }) {
    this.debounce.next(target.value);
  }

  onHover(position) {
    if (position && !this.activeCurrency.currencyCode) {
      this.visibleTooltip = true;
    } else {
      this.visibleTooltip = false;
    }
  }

  notEnoughReserveClick(e: Event) {
    e.preventDefault();
    showNotEnoughReserveModal(this.dialog, this.activeCurrency);
  }
}

@NgModule({
  declarations: [
    InputComponent,
  ],
  imports: [
    CommonModule,
    NotEnoughReserveLinkModule,
    MaterialModule,
    NgxMaskModule.forChild(),
    DirectivesModule,
    ReactiveFormsModule
  ],
  exports: [
    InputComponent
  ]
})
export class InputModule {
}
