import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyToolbarComponent } from './currency-toolbar.component';

describe('CurrencyToolbarComponent', () => {
  let component: CurrencyToolbarComponent;
  let fixture: ComponentFixture<CurrencyToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
