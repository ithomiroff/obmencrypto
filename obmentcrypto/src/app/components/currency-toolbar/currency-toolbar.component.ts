import { Component, NgModule, OnInit } from '@angular/core';
import { MaterialModule } from '@app/core/material/material.module';
import { CommonModule } from '@angular/common';
import * as currencySelectors from '@app/store/currency/currency.selectors';
import { IAppState } from '@app/store/reducer';
import { select, Store } from '@ngrx/store';
import { ECurrencyToolbarStatus } from '@app/core/enums/currency-tollbar-status.enum';

@Component({
  selector: 'app-currency-toolbar',
  templateUrl: './currency-toolbar.component.html',
  styleUrls: ['./currency-toolbar.component.scss']
})
export class CurrencyToolbarComponent {

  currencies$ = this.store.pipe(select(currencySelectors.getToolbarCurrencies));

  statuses = ECurrencyToolbarStatus;

  constructor(
    private store: Store<IAppState>
  ) {
  }

}


@NgModule({
  declarations: [
    CurrencyToolbarComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    CurrencyToolbarComponent
  ]
})
export class CurrencyToolbarModule {
}
