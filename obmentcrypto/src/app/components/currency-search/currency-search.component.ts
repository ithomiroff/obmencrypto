import { Component, OnInit, NgModule, Input, ViewChild, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import * as currencySelectors from '@app/store/currency/currency.selectors';
import { CurrencyActionTypes,
  SetGiveSearchStr, SetReceiveSearchStr } from '@app/store/currency/currency.actions';

@Component({
  selector: 'app-currency-search',
  templateUrl: './currency-search.component.html',
  styleUrls: ['./currency-search.component.scss']
})
export class CurrencySearchComponent implements OnInit, OnDestroy {
  @Input()
  searchType: ECurrencyBlockTypes;
  @ViewChild('currencySearchInput') searchInput: HTMLInputElement;

  constructor(
    private store: Store<IAppState>) {
  }
  ngOnDestroy(): void {
    this.setSearchStr('');
  }

  ngOnInit() {
  }

  setSearchStrHandler(event: any) {
    const searchStr = event.target.value;
    this.setSearchStr(searchStr);
  }

  setSearchStr(searchStr: string) {
    if (this.searchType === ECurrencyBlockTypes.GIVE) {
      return this.store.dispatch(new SetGiveSearchStr(searchStr));
    } else {
      return this.store.dispatch(new SetReceiveSearchStr(searchStr));
    }
  }
}

@NgModule({
  declarations: [
    CurrencySearchComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    CurrencySearchComponent
  ]
})
export class CurrencySearchModule {
}
