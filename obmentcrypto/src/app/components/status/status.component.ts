import { Component, Input, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../core/material/material.module';
import { EStatusesApplication } from '../../core/enums/statuses-application.enum';
import { ApplicationsTableMode } from '@app/core/enums/applications-table-mode';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {

  @Input()
  status: EStatusesApplication;

  @Input()
  mode: ApplicationsTableMode;

  modes = ApplicationsTableMode;

  statues = EStatusesApplication;

  constructor() {
  }

  ngOnInit() {
  }

}

@NgModule({
  declarations: [
    StatusComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    StatusComponent
  ],
})
export class StatusModule {
}
