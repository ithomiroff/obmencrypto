import { LoadAntiforgery } from '@app/store/auth/auth.actions';
import { Component, OnInit, OnDestroy, PLATFORM_ID, Inject} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import * as authActions from '@app/store/auth/auth.actions';
import * as authSelectors from '@app/store/auth/auth.selectors';
import { isMenuOpen } from '@app/store/layout/layout.selectors';
import { ToggleMenu } from '@app/store/layout/layout.actions';
import { Router, NavigationEnd } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { SeoService, ISeoData } from '@app/core/services/seo.service';
import { map, catchError } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { LoadArticleDescriptions } from '@app/store/articles/articles.actions';
import { WorkingTimeService } from '@app/core/services/working-time.service';
import { showNotWorkingTimeModal } from '../modals/modal-not-working-time/modal-not-working-time.component';
import { MatDialog } from '@angular/material';
import { isPlatformBrowser } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  loading$ = this.store.pipe(select(authSelectors.getAntiforgeryLoading));

  stateMenu$ = this.store.pipe(select(isMenuOpen));
  updateSeoSub: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private store: Store<IAppState>,
    private router: Router,
    private seoService: SeoService,
    private workingTimeService: WorkingTimeService,
    private title: Title,
    private meta: Meta,
    private dialog: MatDialog
  ) {
    this.store.dispatch(new authActions.LoadAntiforgery());
    if (isPlatformBrowser(this.platformId)) {
      workingTimeService
        .GetWorkingMode().subscribe(mode => {
          showNotWorkingTimeModal(dialog, mode);
        });
    }
  }

  ngOnInit() {
    this.store.dispatch(new LoadArticleDescriptions());
    this.updateSeoSub = this.router.events
      .subscribe(e => {
        if (e instanceof NavigationEnd) {
          this.loadSeoData();
        }
      });
  }
  ngOnDestroy(): void {
    this.updateSeoSub.unsubscribe();
  }

  private loadSeoData() {
    let url = this.router.url;
    this.seoService.loadSeoByUrl(url)
      .subscribe(seo => {
        this.updateSeo(seo);
      });
  }

  private updateSeo(seo: ISeoData) {
    this.title.setTitle(seo.title);
    this.meta.updateTag({ name: 'description', content: seo.description });
  }


  onCLoseMenu() {
    this.store.dispatch(new ToggleMenu());
  }
}
