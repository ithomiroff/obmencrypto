import { map, catchError } from "rxjs/operators";
import { EApiUrls } from "src/app/core/enums/api-urls.enum";
import { HttpService } from "./../../core/services/http.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { of } from "rxjs";

const PartnerIdKey = "PartnerId";
const CookiesPath = "/";
const ExpirationTime = new Date("2029-04-21T14:32:15.473Z");

@Component({
  selector: "app-partner-link",
  templateUrl: "./partner-link.component.html",
  styleUrls: ["./partner-link.component.scss"]
})
export class PartnerLinkComponent implements OnInit {
  ngOnInit() {}

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    private http: HttpService
  ) {
    const partnerId = this.route.snapshot.params.partnerId;
    if (partnerId) {
      const {cur_from, cur_to} = this.route.snapshot.queryParams;
      const url = `${EApiUrls.SET_PARTNER}/${partnerId}`;
      this.http.get(url).subscribe({
        complete: () => {
          const codeParams = this.route.snapshot.params.code;
          this.redirectToHome(codeParams);
        },
        error: () => console.log('Unable to set partner id')
      });
      if (cur_from && cur_to) {
        this.redirectToHome(`${cur_from}-${cur_to}`);
      }
    }
  }

  private redirectToHome(codeParams: string) {
    const url = codeParams ? codeParams : '';

    this.router.navigateByUrl(url);
  }
}
