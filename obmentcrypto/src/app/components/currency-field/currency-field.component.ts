import { Component, Input, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import {EFilters} from '@app/core/enums/filters.enum';
import { NotEnoughReserveLinkModule } from '../not-enough-reserve-link/not-enough-reserve-link.component';

@Component({
  selector: 'app-currency-field',
  templateUrl: './currency-field.component.html',
  styleUrls: ['./currency-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurrencyFieldComponent {

  @Input()
  mode: ECurrencyBlockTypes;

  @Input()
  currency: IExchangeCurrency;

  @Input()
  selected: boolean;

  showNotEnough: boolean = false;

  modes = ECurrencyBlockTypes;
  constructor() {
  }

  get subtitle(): string {
    return 'Резерв: ';
  }

  get keyType(): string {
    return 'reserve';
  }

  get currencyCaption(): string {
    if (this.currency.currencyCode.startsWith('USDT')) {
       return 'USDT';
    }
    return this.currency.currencyType === EFilters.COIN ? this.currency.currencyCode : this.currency.currencyType;
  }
}

@NgModule({
  declarations: [
    CurrencyFieldComponent
  ],
  imports: [
    CommonModule,
    NotEnoughReserveLinkModule
  ],
  exports: [
    CurrencyFieldComponent
  ]
})
export class CurrencyFieldModule {
}
