import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmSuccessComponent } from './modal-confirm-success.component';

describe('ModalConfirmSuccessComponent', () => {
  let component: ModalConfirmSuccessComponent;
  let fixture: ComponentFixture<ModalConfirmSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfirmSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
