import { Component, Inject, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IModalConfigInterface } from '@app/core/interfaces/modal-config.interface';

@Component({
  selector: 'app-modal-confirm-success',
  templateUrl: './modal-confirm-success.component.html',
  styleUrls: ['./modal-confirm-success.component.scss']
})
export class ModalConfirmSuccessComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalConfirmSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IModalConfigInterface
  ) {
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }
}
