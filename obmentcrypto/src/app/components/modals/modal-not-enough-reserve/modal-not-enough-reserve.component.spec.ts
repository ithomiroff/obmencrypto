import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNotEnoughReserveComponent } from './modal-not-enough-reserve.component';

describe('ModalNotEnoughReserveComponent', () => {
  let component: ModalNotEnoughReserveComponent;
  let fixture: ComponentFixture<ModalNotEnoughReserveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalNotEnoughReserveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNotEnoughReserveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
