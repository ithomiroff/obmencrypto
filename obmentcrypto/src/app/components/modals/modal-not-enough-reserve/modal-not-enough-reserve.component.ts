import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { telegramValidator, emailValidator, possitiveNumberValidator } from '@app/core/utils/validators.functions';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { IPersonInfo } from '@app/core/interfaces/exchange-make.interface';
import { HttpService } from '@app/core/services/http.service';
import { MarkAsTouchedAllFields, clearErrors } from '@app/core/utils/form-utils';
import { EApiUrls } from '@app/core/enums/api-urls.enum';
import { map, catchError, tap } from 'rxjs/operators';
import { showDefaultModal, showDefaultErrorModal } from '@app/core/utils/modal-configs';

export interface INotEnoughReserveData {
  currency: IExchangeCurrency;
  currencyId?: number;
  personInfo: IPersonInfo;
  volume: number;
}

@Component({
  selector: 'app-modal-not-enough-reserve',
  templateUrl: './modal-not-enough-reserve.component.html',
  styleUrls: ['./modal-not-enough-reserve.component.scss']
})
export class ModalNotEnoughReserveComponent implements OnInit {
  notEnoughForm: FormGroup;
  error: string;

  constructor(
    public dialogRef: MatDialogRef<ModalNotEnoughReserveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IExchangeCurrency,
    private http: HttpService,
    private dialog: MatDialog) {
    console.log(data);
  }

  ngOnInit() {
    this.notEnoughForm = new FormGroup({
      personInfo: new FormGroup({
        email: new FormControl(null),
        telegram: new FormControl(null),
        phone: new FormControl(null),
      }),
      volume: new FormControl(null, [
        Validators.required,
        possitiveNumberValidator()
      ]),
    });
  }

  get PersonInfo(): FormGroup {
    return this.notEnoughForm.get('personInfo') as FormGroup;
  }

  get EmailControl(): FormGroup {
    return this.PersonInfo.get('email') as FormGroup;
  }

  get TelegramControl(): FormGroup {
    return this.PersonInfo.get('telegram') as FormGroup;
  }

  get PhoneControl(): FormGroup {
    return this.PersonInfo.get('phone') as FormGroup;
  }

  get VolumeControl(): FormGroup {
    return this.notEnoughForm.get('volume') as FormGroup;
  }

  onSubmit() {
    const formValue = {
      currency: this.data,
      currencyId: this.data.id,
      ...this.notEnoughForm.value
    } as INotEnoughReserveData;
    clearErrors(this.notEnoughForm);
    if (formValue.personInfo.email ||
      formValue.personInfo.phone ||
      formValue.personInfo.telegram) {
      if (formValue.personInfo.email) {
        this.EmailControl.setErrors(emailValidator()(this.EmailControl));
      }
      if (formValue.personInfo.telegram) {
        this.TelegramControl.setErrors(telegramValidator()(this.TelegramControl));
      }
      this.notEnoughForm.setErrors(possitiveNumberValidator()(this.VolumeControl));
      if (this.notEnoughForm.valid) {
        return this.PostNotEnoughReserve(formValue);
      }
    } else {
      this.EmailControl.setErrors({ contact: true });
      this.TelegramControl.setErrors({ contact: true });
      this.PhoneControl.setErrors({ contact: true });
    }
    MarkAsTouchedAllFields(this.notEnoughForm);
  }

  private PostNotEnoughReserve(formValue: INotEnoughReserveData) {
    this.http.post(EApiUrls.POST_NOT_ENOUGH_RESERVE, formValue)
      .pipe(map(() => showDefaultModal(this.dialog, {
          title: 'Ваш запрос успешно отправлен',
          message: 'Оператор свяжется с вами.'
        }),
        catchError((err) => showDefaultErrorModal(this.dialog, err))),
        tap(() => this.close())
      ).subscribe();
  }

  close() {
    return this.dialogRef.close();
  }
}

const defaultConfig = (data: IExchangeCurrency) => {
  return {
    data,
    width: '500px',
    disableClose: true
  };
};

export const showNotEnoughReserveModal = (dialog: MatDialog, data: IExchangeCurrency) => {
  return dialog.open(ModalNotEnoughReserveComponent, defaultConfig(data)).afterClosed();
};
