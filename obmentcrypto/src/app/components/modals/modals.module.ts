import { NgModule } from '@angular/core';
import { ModalNotWorkingTimeComponent } from './modal-not-working-time/modal-not-working-time.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalNotEnoughReserveComponent } from './modal-not-enough-reserve/modal-not-enough-reserve.component';
import { ModalBaseComponent } from './modal-base/modal-base.component';
import { ModalConfirmSuccessComponent } from './modal-confirm-success/modal-confirm-success.component';

@NgModule({
  declarations: [
    ModalBaseComponent,
    ModalNotWorkingTimeComponent,
    ModalNotEnoughReserveComponent,
    ModalConfirmSuccessComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ModalNotWorkingTimeComponent,
    ModalNotEnoughReserveComponent,
    ModalConfirmSuccessComponent
  ]
})
export class ModalsModule {}
