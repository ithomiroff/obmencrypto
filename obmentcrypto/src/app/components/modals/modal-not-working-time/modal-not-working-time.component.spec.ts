import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNotWorkingTimeComponent } from './modal-not-working-time.component';

describe('ModalNotWorkingTimeComponent', () => {
  let component: ModalNotWorkingTimeComponent;
  let fixture: ComponentFixture<ModalNotWorkingTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalNotWorkingTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNotWorkingTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
