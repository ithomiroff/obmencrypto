import { Component, OnInit, NgModule, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import * as tz from 'moment-timezone';
import { of } from 'rxjs';
import { WorkingMode } from '@app/core/services/working-time.service';


const start = '10:00:00';
const end = '24:00:00';

@Component({
  selector: 'app-modal-not-working-time',
  templateUrl: './modal-not-working-time.component.html',
  styleUrls: ['./modal-not-working-time.component.scss']
})
export class ModalNotWorkingTimeComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public mode: WorkingMode,
    public dialogRef: MatDialogRef<ModalNotWorkingTimeComponent>,
    private dialog: MatDialog) { }

  ngOnInit() {
  }

  get title() {
    if (this.mode === WorkingMode.NotWorkingTime) {
      return `Время работы сервиса с ${tz(start, 'HH:mm:ss').format(
        'HH:mm'
      )} до ${tz(end, 'HH:mm:ss').format('HH:mm')}`;
    } else if (this.mode === WorkingMode.Off) {
      return 'Сервис временно не работает';
    }
  }

  close() {
    return this.dialogRef.close();
  }
}
const defaultConfig = (workingMode: WorkingMode) => {
  return {
    data: workingMode,
    width: '500px',
    disableClose: true
  };
};

export const showNotWorkingTimeModal = (dialog: MatDialog, workingMode: WorkingMode) => {
  if (workingMode === WorkingMode.On)  {
    return of();
  }
  return dialog.open(ModalNotWorkingTimeComponent, defaultConfig(workingMode)).afterClosed();
};
