import { Component, OnInit, NgModule, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-exchanger-modal-base',
  templateUrl: './modal-base.component.html',
  styleUrls: ['./modal-base.component.scss']
})
export class ModalBaseComponent implements OnInit {
  @Input()
  dialogRef: MatDialogRef<any>;

  @Input()
  title: string;

  @Input()
  buttonTitle: string;

  @Output()
  buttonClick: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  close() {
    return this.dialogRef.close();
  }

  onSubmit(event) {
    this.buttonClick.emit(event);
  }

}
