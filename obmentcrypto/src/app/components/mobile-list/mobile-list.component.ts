import {Component, Inject, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../core/material/material.module';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {IExchangeCurrency} from '../../core/interfaces/exchange-currency.interface';
import {CurrencyFieldModule} from '../currency-field/currency-field.component';
import {FiltersModule} from '@app/components/filters/filters.component';
import {IFilters} from '@app/core/interfaces/filters.interface';
import * as exchangeSelectors from '@app/store/exchange/exchange.selectors';
import * as exchangeActions from '@app/store/exchange/exchange.actions';
import * as currencySelectors from '@app/store/currency/currency.selectors';
import * as currencyActions from '@app/store/currency/currency.actions';
import {IAppState} from '@app/store/reducer';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {ECurrencyBlockTypes} from '@app/core/enums/currency-block-types.enum';
import * as formExchangeActions from '../../store/form-exchange/form-exchange.actions';
import { CurrencySearchModule } from '../currency-search/currency-search.component';

@Component({
  selector: 'app-mobile-list',
  templateUrl: './mobile-list.component.html',
  styleUrls: ['./mobile-list.component.scss']
})
export class MobileListComponent {

  mode: ECurrencyBlockTypes;

  activeCurrency$: Observable<IExchangeCurrency>;

  currencies$: Observable<IExchangeCurrency[]>;

  filters$: Observable<IFilters[]>;

  activeFilter$: Observable<IFilters>;

  constructor(
    private listRef: MatBottomSheetRef<MobileListComponent>,
    private store: Store<IAppState>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: {
      mode: ECurrencyBlockTypes,
    }
  ) {
    this.mode = data.mode;
    this.activeCurrency$ = this.store.pipe(select(exchangeSelectors.activeCurrency, this.mode));
    this.currencies$ = this.store.pipe(select(currencySelectors.getCurrencies, this.mode));
    this.filters$ = this.store.pipe(select(currencySelectors.getFilters, this.mode));
    this.activeFilter$ = this.store.pipe(select(currencySelectors.getActiveFilters, this.mode));
  }

  onSelect(cur: IExchangeCurrency, event: MouseEvent) {
    // this.store.dispatch(new exchangeActions.SetActiveCurrency(this.mode, cur));
    // if (this.mode === ECurrencyBlockTypes.GIVE) {
    //   this.store.dispatch(new formExchangeActions.GetFormFieldGive(cur.id));
    // } else {
    //   this.store.dispatch(new formExchangeActions.GetFormFieldReceive(cur.id));
    // }
    this.listRef.dismiss({currency: cur});
    event.preventDefault();
  }

  onChangeFilter($event: IFilters) {
    this.store.dispatch(new currencyActions.SetActiveFilter($event, this.mode));
  }

  onClose() {
    this.listRef.dismiss();
    event.preventDefault();
  }
}

@NgModule({
  declarations: [
    MobileListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CurrencyFieldModule,
    CurrencySearchModule,
    FiltersModule
  ],
  exports: [
    MobileListComponent
  ],
  entryComponents: [
    MobileListComponent
  ]
})
export class MobileListModule {
}
