import { ChangeDetectionStrategy, Component, EventEmitter, Input, NgModule, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { IFilters } from '@app/core/interfaces/filters.interface';
import { CurrencyFieldModule } from '../currency-field/currency-field.component';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';

@Component({
  selector: 'app-currency-blog',
  templateUrl: './currency-blog.component.html',
  styleUrls: ['./currency-blog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurrencyBlogComponent implements OnInit {

  @Input()
  mode: ECurrencyBlockTypes;

  @Input()
  filters: IFilters[];

  @Input()
  currencies: IExchangeCurrency[] = [];

  @Input()
  activeCurrency: IExchangeCurrency;

  @Input()
  amountValue: number;

  @Input()
  isMobile: boolean;

  @Output()
  exchangeCurrencyChanged = new EventEmitter<IExchangeCurrency>();

  @Output()
  openMobileList = new EventEmitter();

  @Input()
  isActive: boolean = true;

  constructor(
  ) {
  }

  ngOnInit() {
  }

  onChooseCurrency(cur: IExchangeCurrency) {
    this.exchangeCurrencyChanged.emit(cur);
  }

  onOpenList() {
    this.openMobileList.emit();
  }

  trackByFn(index, item: IExchangeCurrency) {
    return item.id;
  }
}


@NgModule({
  declarations: [
    CurrencyBlogComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CurrencyFieldModule
  ],
  exports: [
    CurrencyBlogComponent
  ]
})
export class CurrencyBlogModule {
}
