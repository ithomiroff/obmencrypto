import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyBlogComponent } from './currency-blog.component';

describe('CurrencyBlogComponent', () => {
  let component: CurrencyBlogComponent;
  let fixture: ComponentFixture<CurrencyBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyBlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
