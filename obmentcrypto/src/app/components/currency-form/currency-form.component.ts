import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, NgModule, OnDestroy, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { NgxMaskModule } from 'ngx-mask';
import { IAppState } from '@app/store/reducer';
import * as formExchangeSelectors from '@app/store/form-exchange/form-exchange.selectors';
import * as formExchangeActioins from '@app/store/form-exchange/form-exchange.actions';
import * as authSelectors from '@app/store/auth/auth.selectors';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { ECurrencyBlockTypes } from '@app/core/enums/currency-block-types.enum';
import { IFieldType } from '@app/core/interfaces/field-type.interface';
import { RouterModule } from '@angular/router';
import { debounceTime, filter, map, takeWhile, tap, catchError } from 'rxjs/operators';
import { FieldComponent } from '@app/components/currency-form/components/field/field.component';
import { FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators, ValidationErrors } from '@angular/forms';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { EFilters } from '@app/core/enums/filters.enum';
import { walletCryptoValidator,
  walletYAMoneyValidator,
  walletADVCValidator,
  validateEmail,
  emailOrTelegramValidator} from '../../core/utils/validators.functions';
import { EFieldCurrencyType } from '@app/core/enums/field-type.enum';
import {SpinnerModule} from '../spinner/spinner.component';
import { IPersonInfo } from '@app/core/interfaces/exchange-make.interface';
import { MarkAsTouchedAllFields, UpdateValidityAll } from '@app/core/utils/form-utils';
import { PersonDataComponent } from './components/person-data/person-data.component';
import {QueryList} from '@angular/core';

@Component({
  selector: 'app-currency-form',
  templateUrl: './currency-form.component.html',
  styleUrls: ['./currency-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class CurrencyFormComponent implements OnInit, OnDestroy {

  @Input()
  exchangeForm: FormGroup;

  @Input()
  activeGiveCurrency$: Observable<IExchangeCurrency>;

  @Input()
  activeReceiveCurrency$: Observable<IExchangeCurrency>;

  titles$ = this.store.pipe(select(formExchangeSelectors.getTitle));

  fieldGives$: Observable<IFieldType[]> = this.store.pipe(select(formExchangeSelectors.fieldsGive));

  fieldReceives$: Observable<IFieldType[]> = this.store.pipe(select(formExchangeSelectors.fieldsReceive));

  exchangeError$: Observable<string> = this.store.pipe(select(formExchangeSelectors.getExchangeError));

  loading$: Observable<boolean> = this.store.pipe(select(formExchangeSelectors.formLoading));

  showPersonForm$ = new Subject<boolean>();

  alive = true;

  constructor(
    private store: Store<IAppState>,
    private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    combineLatest(
      this.fieldGives$,
      this.fieldReceives$,
      this.activeGiveCurrency$,
      this.activeReceiveCurrency$
    ).pipe(
      map(([give, receive, activeGive, activeReceive]) => {
        if (give && activeGive) {
          this.fillFormControlsArray(give, ECurrencyBlockTypes.GIVE, activeGive);
        }
        if (receive && activeReceive) {
          this.fillFormControlsArray(receive, ECurrencyBlockTypes.RECEIVE, activeReceive);
        }
      }),
      takeWhile(() => this.alive)
    ).subscribe();
  }

  fillFormControlsArray(fields: IFieldType[] = [], mode: ECurrencyBlockTypes, activeCurrency: IExchangeCurrency): void {
    const formArray = mode === ECurrencyBlockTypes.RECEIVE ? this.receiveFormArray : this.giveFormArray;
    if (fields.length > 0 && activeCurrency) {
      this.clearArrayControls(formArray);
      fields.forEach((field: IFieldType) => {
        const validators = this.getValidators(activeCurrency, field);
        formArray.push(this.getFormGroupByObject({...field, value: null}, validators));
      });
      this.cd.detectChanges();
      this.showPersonForm$.next(true);
    }
    if (fields.length < 1) {
      this.clearArrayControls(formArray);
    }
  }

  getFormGroupByObject(obj, validators): FormGroup {
    const group = new FormGroup({});
    for (const key in obj) {
      if (key === 'value') {
        group.addControl(key, new FormControl(obj[key], validators));
      } else {
        group.addControl(key, new FormControl(obj[key]));
      }
    }
    return group;
  }

  clearArrayControls(array: FormArray): void {
    while (array.length !== 0) {
      array.removeAt(0);
    }
  }


  getValidators(currency: IExchangeCurrency, field: IFieldType): Validators {
    const val: ValidationErrors[] = [];
    if (field.required) {
      val.push(Validators.required);
    }
    if (!currency.currencyCode) {
      return val;
    }
    if (currency.currencyType === EFilters.COIN && field.fieldType === EFieldCurrencyType.WALLET) {
      val.push(walletCryptoValidator(currency.currencyCode));
    }
    if (currency.currencyCode === 'YAMRUB') {
      val.push(walletYAMoneyValidator());
    }
    if (currency.currencyCode.startsWith('ADVC')) {
      val.push(walletADVCValidator(currency.currencyCode));
    }
    return val;
  }


  ngOnDestroy(): void {
    this.alive = false;
  }

  get giveFormArray(): FormArray {
    return this.exchangeForm.get('giveTransactionData') as FormArray;
  }

  get receiveFormArray(): FormArray {
    return this.exchangeForm.get('receiveTransactionData') as FormArray;
  }

  get personForm(): FormGroup {
    return this.exchangeForm.get('personInfo') as FormGroup;
  }


  onClick(): void {
    if (this.exchangeForm.invalid) {
      this.validateAndMarkFormArray(this.receiveFormArray);
      this.validateAndMarkFormArray(this.giveFormArray);
      UpdateValidityAll(this.exchangeForm);
      MarkAsTouchedAllFields(this.exchangeForm);
      this.detectChanges();
    } else {
      this.store.dispatch(new formExchangeActioins.ExchangeCurrency());
    }
  }
  private validateAndMarkFormArray(formArray: FormArray) {
    formArray.controls.forEach(control => {
      const valueInput = control.get('value');
      valueInput.updateValueAndValidity({onlySelf: true});
      valueInput.markAsTouched({onlySelf: true});
    });
  }
  public detectChanges() {
    this.cd.detectChanges();
  }
}


@NgModule({
  declarations: [
    CurrencyFormComponent,
    FieldComponent,
    PersonDataComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    SpinnerModule
  ],
  exports: [
    CurrencyFormComponent
  ]
})
export class CurrencyFormModule {
}
