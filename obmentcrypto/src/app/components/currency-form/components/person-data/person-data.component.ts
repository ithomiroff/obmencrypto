import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import * as authSelectors from '@app/store/auth/auth.selectors';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import { filter, tap, takeWhile, debounceTime } from 'rxjs/operators';
import {
  anyContactRequiredValidator,
  validateEmail,
  isTelegram,
  validatePhone } from '@app/core/utils/validators.functions';
import { IPersonInfo } from '@app/core/interfaces/exchange-make.interface';
import * as formExchangeActioins from '@app/store/form-exchange/form-exchange.actions';
import { MatTabGroup } from '@angular/material';
import { UpdateValidityAll, MarkAsTouchedAllFields } from '@app/core/utils/form-utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-person-data',
  templateUrl: './person-data.component.html',
  styleUrls: ['./person-data.component.scss'],
})
export class PersonDataComponent implements OnInit, OnDestroy {
  @Input()
  exchangeForm: FormGroup;

  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

  telegramPattern = new RegExp('\[a-zA-Z0-9_\]{5,}');
  user$ = this.store.pipe(select(authSelectors.getAuthUser));
  subs: Subscription[] = [];
  alive = true;
  constructor(private store: Store<IAppState>, private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.сreatePersonForm();
    this.fillUserFormData();
    this.subs.push(this.personForm.valueChanges
      .pipe(
        debounceTime(500),
        tap((value: IPersonInfo) => {
          // UpdateValidityAll(this.personForm);
          MarkAsTouchedAllFields(this.personForm);
          // this.cd.detectChanges();
          console.log('value changed');
          if (this.personForm.valid) {
            const clearedPersonInfo = this.clearPersonInfo(value);
            console.log(value);
            return this.store.dispatch(
              new formExchangeActioins.SetPersonData(clearedPersonInfo)
            );
          }
        }),
        takeWhile(() => this.alive)
      )
      .subscribe()
    );
  }

  clearPersonInfo(value: IPersonInfo) {
    if (validateEmail(value.email) === false) {
      value.email = null;
    }
    if (isTelegram(value.telegram) === false) {
      value.telegram = null;
    }
    if (validatePhone(value.phone) === false) {
      value.phone = null;
    }
    return value;
  }

  private сreatePersonForm() {
    this.exchangeForm.addControl(
      'personInfo',
      new FormGroup(
        {
          email: new FormControl(null, []),
          telegram: new FormControl(null, []),
          phone: new FormControl(null, []),
        },
        anyContactRequiredValidator()
      )
    );
  }

  ngOnDestroy(): void {
    this.alive = false;
    this.subs.forEach(sub => sub.unsubscribe());
  }

  get personForm(): FormGroup {
    return this.exchangeForm.get('personInfo') as FormGroup;
  }

  get emailControl(): FormGroup {
    return this.personForm.get('email') as FormGroup;
  }
  get telegramControl(): FormGroup {
    return this.personForm.get('telegram') as FormGroup;
  }

  get phoneControl(): FormGroup {
    return this.personForm.get('phone') as FormGroup;
  }

  fillUserFormData(): void {
    this.subs.push(this.user$
      .pipe(
        filter((u) => !!u),
        tap((user) => {
          if (user.email) {
            this.personForm.get('email').setValue(user.email);
            this.personForm.get('email').disable();
          }
          if (user.phone) {
            this.personForm.get('phone').setValue(user.phone);
            this.personForm.get('phone').disable();
          }
          if (user.telegram) {
            user.telegram = user.telegram.indexOf('@') !== 1 ? '@' + user.telegram : user.telegram;
            this.personForm.get('telegram').setValue(user.telegram);
            this.personForm.get('telegram').disable();
          }
          this.store.dispatch(new formExchangeActioins.SetPersonData(user));
        }),
        takeWhile(() => this.alive)
      )
      .subscribe()
    );
  }

  public detectChanges() {
    this.cd.detectChanges();
  }
}
