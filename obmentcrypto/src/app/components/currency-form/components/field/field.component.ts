import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { IFieldType } from '@app/core/interfaces/field-type.interface';
import { EFieldCurrencyType } from '@app/core/enums/field-type.enum';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {

  @Input() fieldControl: FormControl;

  fieldTypes = EFieldCurrencyType;

  constructor(
    private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
  }

  get fieldControlValue(): IFieldType {
    return this.fieldControl.value as IFieldType;
  }

  public detectChanges() {
    this.cd.detectChanges();
  }
}
