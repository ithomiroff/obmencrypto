import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerButtonsComponent } from './partner-buttons.component';

describe('PartnerButtonsComponent', () => {
  let component: PartnerButtonsComponent;
  let fixture: ComponentFixture<PartnerButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
