
import { ContactType } from './../../core/interfaces/contact-url.interface';
import { Component, NgModule, OnInit } from '@angular/core';
import {PartnerButtonsComponent} from './components/partner-buttons/partner-buttons.component';
import { DirectivesModule } from '@app/core/directives/directives.module';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  readonly contactTypes = ContactType;
  constructor() { }

  ngOnInit() {
  }

}


@NgModule({
  declarations: [
    PartnerButtonsComponent,
    FooterComponent,
  ],
  imports: [
    DirectivesModule
  ],
  exports: [
    FooterComponent
  ],
})
export class FooterModule {
}
