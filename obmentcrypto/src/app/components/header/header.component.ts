import { ContactsService } from './../../core/services/contacts.service';
import { ContactType } from './../../core/interfaces/contact-url.interface';
import { AfterViewInit, Component, ElementRef, EventEmitter,
  NgModule, OnInit, Output, Renderer2, ViewChild, PLATFORM_ID, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { Router } from '@angular/router';
import { EAppUrls } from '@app/core/enums/app-urls.enum';
import * as authSelectors from '@app/store/auth/auth.selectors';
import { select, Store } from '@ngrx/store';
import { IAppState } from '@app/store/reducer';
import * as authActions from '@app/store/auth/auth.actions';
import { BehaviorSubject } from 'rxjs';
import * as layoutSelectors from '../../store/layout/layout.selectors';
import * as layoutActions from '../../store/layout/layout.actions';
import { tap } from 'rxjs/operators';
import { DirectivesModule } from '@app/core/directives/directives.module';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  readonly contactTypes = ContactType;
  user$ = this.store.pipe(select(authSelectors.getAuthUser));

  isLoggedIn$ = this.store.pipe(select(authSelectors.isUserLoggedIn));

  stateMenu$ = this.store.pipe(select(layoutSelectors.isMenuOpen));

  @ViewChild('buttonAccount', {read: ElementRef}) buttonAccount: ElementRef;
  @ViewChild('menu', {read: ElementRef}) matMenu: ElementRef;

  widthButton$ = new BehaviorSubject<number>(210);
  workingTime$ = new BehaviorSubject<string>('Работаем с 10:00 до 00:00 по мск.');

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private router: Router,
    private store: Store<IAppState>,
    private contactsService: ContactsService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.widthButton$.next(this.buttonAccount.nativeElement.offsetWidth);
    }, 1000);

    this.stateMenu$.pipe(
      tap((state) => {
        if (isPlatformBrowser(this.platformId)) {
          document.body.className = state ? 'no-scroll' : '';
        }
      })
    ).subscribe();
  }

  onRegClick() {
    this.router.navigateByUrl(`${EAppUrls.USER}/${EAppUrls.REG}`);
  }

  onEnter() {
    this.router.navigateByUrl(`${EAppUrls.USER}/${EAppUrls.AUTH}`);
  }

  onLogout() {
    this.store.dispatch(new authActions.Logout());
  }

  onCabinetClick() {
    this.router.navigateByUrl(`${EAppUrls.USER}/${EAppUrls.CABINET}`);
  }

  onNavigate() {
    this.router.navigateByUrl(`/`);
  }

  onOpenMenu() {
    this.store.dispatch(new layoutActions.ToggleMenu());
  }

}


@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    DirectivesModule,
    MaterialModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule {
}
