import { Component, OnInit, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { showNotEnoughReserveModal } from '../modals/modal-not-enough-reserve/modal-not-enough-reserve.component';
import { IExchangeCurrency } from '@app/core/interfaces/exchange-currency.interface';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-not-enough-reserve-link',
  templateUrl: './not-enough-reserve-link.component.html',
  styleUrls: ['./not-enough-reserve-link.component.scss']
})
export class NotEnoughReserveLinkComponent implements OnInit {
  @Input()
  currency: IExchangeCurrency;

  @Input()
  needOpacityClass = false;

  constructor(
    private dialog: MatDialog) { }

  ngOnInit() {
  }

  notEnoughReserveClick(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    showNotEnoughReserveModal(this.dialog, this.currency);
  }
}


@NgModule({
  declarations: [
    NotEnoughReserveLinkComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NotEnoughReserveLinkComponent
  ]
})
export class NotEnoughReserveLinkModule {
}
