import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotEnoughReserveLinkComponent } from './not-enough-reserve-link.component';

describe('NotEnoughReserveLinkComponent', () => {
  let component: NotEnoughReserveLinkComponent;
  let fixture: ComponentFixture<NotEnoughReserveLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotEnoughReserveLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotEnoughReserveLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
