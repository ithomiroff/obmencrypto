import { Component, EventEmitter, Input, NgModule, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../core/material/material.module';
import { RouterModule, RouterLink } from '@angular/router';
import { LinkData } from '@app/core/interfaces/link-data.interface';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input()
  menuState: boolean;

  @Output()
  closeMenu = new EventEmitter();

  linkDatas: LinkData[] = [
    new LinkData('/', 'Главная'),
    new LinkData('/', 'Обмен'),
    new LinkData('/partnerdesc', 'Партнерам'),
    new LinkData('/faq', 'FAQ'),
    new LinkData('/contacts', 'Контакты'),
    new LinkData('/feedback', 'Оставить отзыв'),
    new LinkData('/privacy', 'Политика конфиденциальности'),
    new LinkData('/articles/obmen-eth-na-rubli-bitkoin', 'Статьи'),
  ]

  constructor() {
  }

  ngOnInit() {
  }

  onOpenMenu() {

  }
}

@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule {
}
