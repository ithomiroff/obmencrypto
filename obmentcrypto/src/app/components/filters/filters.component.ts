import { Component, EventEmitter, Input, NgModule, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/core/material/material.module';
import { IFilters } from '@app/core/interfaces/filters.interface';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Input()
  filters: IFilters[];

  @Input()
  activeFilter: IFilters;

  @Output()
  changeFilter = new EventEmitter<IFilters>();

  constructor() {
  }

  ngOnInit() {
  }

  onClick(filter: IFilters) {
    if (filter.code !== this.activeFilter.code) {
      this.changeFilter.emit(filter);
    }
  }
}

@NgModule({
  declarations: [
    FiltersComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    FiltersComponent
  ]
})
export class FiltersModule {
}
