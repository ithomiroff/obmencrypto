import { EStatusesApplication } from '../enums/statuses-application.enum';
import { IExchangeCurrency } from './exchange-currency.interface';
import { IFieldType } from './field-type.interface';

export interface IApplicationInfo {
  key: string;
  orderNumber: number;
  status: EStatusesApplication;
  timeExecution: number;
  date: number;
  giveCurrency: IExchangeCurrency;
  receiveCurrency: IExchangeCurrency;
  applicationText: string;
  moneyBackText: string;
  qrCode: string;
  payUrl: string;
  withdrawInWallet: IWithdrawInWallet;
  promoCode: string;
  fixedRate: boolean;
  feePrice: number;
  telegramInfo: string;

  fieldsFrom: IFieldType[];
  fieldsTo: IFieldType[];
}

export interface IWithdrawInWallet {
  address: string;
  additionalInfoTitle: string;
  additionalInfo: string;
}
