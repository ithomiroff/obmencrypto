import { EStatusesApplication } from '../../enums/statuses-application.enum';

export interface IWithdrawalApplication {
  date: string;
  purse: string;
  total: number;
  status: EStatusesApplication;
}
