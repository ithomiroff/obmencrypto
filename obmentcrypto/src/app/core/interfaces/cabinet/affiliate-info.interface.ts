export interface IAffiliateInfo {
  partnerText: string;
  personLink: string;
  availableAffiliate: {
    value: number;
    currencyCode: string;
    fee: number;
  };
}
