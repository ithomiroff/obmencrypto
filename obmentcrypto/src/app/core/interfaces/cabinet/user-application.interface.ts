import { EStatusesApplication } from '../../enums/statuses-application.enum';
import { IExchangeCurrency } from '../exchange-currency.interface';

export interface IUserApplication {
  date: string;
  giveCurrency: IExchangeCurrency;
  receiveCurrency: IExchangeCurrency;
  status: EStatusesApplication;
  earnings: number;
  statusText: string;
}
