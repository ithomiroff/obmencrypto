export interface ITextExchange {
  topText: string;
  bottomText: string;
}
