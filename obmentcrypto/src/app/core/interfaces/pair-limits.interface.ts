import { ICurrencyLimit } from './exchange-currency.interface';

export interface IPairLimits {
  giveLimit: ICurrencyLimit;
  receiveLimit: ICurrencyLimit;
}
