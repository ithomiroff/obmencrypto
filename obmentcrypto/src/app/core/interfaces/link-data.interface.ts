export interface ILinkData {
  url: string;
  title: string;
}

export class LinkData implements ILinkData {
  constructor(
    public url: string,
    public title: string) {}
}
