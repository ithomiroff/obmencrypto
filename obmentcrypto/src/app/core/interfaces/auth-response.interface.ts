import { IUser } from '@app/core/interfaces/user.interface';

export interface IAuthResponse extends IUser {
  token: string;
}
