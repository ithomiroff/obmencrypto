export interface IArticleDescription {
  urlKey: string;
  title: string;
}

export interface IArticle extends IArticleDescription {
  htmlText: string;
}
