import { EFieldCurrencyType } from '../enums/field-type.enum';

export interface IFieldType {
  id: number;
  fieldType: EFieldCurrencyType;
  title: string;
  icon: string;
  value?: string;
  required: boolean;
}
