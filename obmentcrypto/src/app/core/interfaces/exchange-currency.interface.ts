import { EFilters } from '@app/core/enums/filters.enum';
import { EFieldCurrencyType } from '@app/core/enums/field-type.enum';

export interface ICurrencyLimit {
  minValue: string;
  maxValue: string;
}

export interface IExchangeCurrency {
  id: number;
  icon: string;
  name: string;
  currencyCode: string;
  currencyType: EFilters;
  currencyField: EFieldCurrencyType;
  minValue?: string;
  maxValue?: string;
  reserve?: string;
  value?: number;
  isActive: boolean;
}
