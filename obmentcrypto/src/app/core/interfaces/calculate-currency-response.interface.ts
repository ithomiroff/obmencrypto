export interface ICalculateCurrencyResponse {
  value: number;
  warnings: {
    give: string;
    receive: string;
  };
}
