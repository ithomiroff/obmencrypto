import { ECurrencyToolbarStatus } from '../enums/currency-tollbar-status.enum';

export interface IToolbarCurrency {
  currencyCode: string;
  rublesValue: number;
  currencyStatusPercentValue: number;
  currencyStatus: ECurrencyToolbarStatus;
}
