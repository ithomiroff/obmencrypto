export interface IModalConfigInterface {
  title: string;
  message: string;
}
