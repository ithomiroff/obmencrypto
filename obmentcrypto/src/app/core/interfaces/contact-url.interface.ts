
export interface IContact {
  data: string;
  type: ContactType;
}

export enum ContactType {
  WhatsApp,
  Telegram,
  Phone
}
