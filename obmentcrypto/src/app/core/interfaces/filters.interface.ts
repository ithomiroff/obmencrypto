import { EFilters } from '@app/core/enums/filters.enum';

export interface IFilters {
  code: EFilters;
}
