export interface ICalculatedValue {
  calculatedCurrencyCode: string;
  calculatedCurrencyValue: number;
}
