import { EFieldCurrencyType } from '../enums/field-type.enum';
import { IExchangeCurrency } from './exchange-currency.interface';
import { IFieldType } from './field-type.interface';

export interface IExchangeForm {
  giveCurrency: IExchangeCurrency;
  receiveCurrency: IExchangeCurrency;
  giveTransactionData: IFieldType[];
  receiveTransactionData: IFieldType[];
  personInfo: IPersonInfo;
}

export interface ITransactionData {
  type: EFieldCurrencyType;
  valuePurseNumber?: number;
  valueCardHolderName?: string;
  valueCardNumber?: number;
}

export interface IPersonInfo {
  email?: string;
  telegram?: string;
  phone?: string;
}

export interface IPersonForm {
  emailOrTelegram: string;
}
