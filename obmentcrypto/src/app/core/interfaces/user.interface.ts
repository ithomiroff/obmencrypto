export interface IUser {
  email: string;
  telegram: string;
  phone: string;
  username: string;
}
