import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { IAppState } from '../../store/reducer';
import { select, Store } from '@ngrx/store';
import * as authSelectors from '../../store/auth/auth.selectors';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CabinetGuard implements CanActivate {

  user$ = this.store.pipe(select(authSelectors.getAuthUser));

  constructor(
    private store: Store<IAppState>
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.user$.pipe(
      map((user) => !!user.username)
    );
  }

}
