import { TestBed, async, inject } from '@angular/core/testing';

import { CabinetGuard } from './cabinet.guard';

describe('CabinetGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CabinetGuard]
    });
  });

  it('should ...', inject([CabinetGuard], (guard: CabinetGuard) => {
    expect(guard).toBeTruthy();
  }));
});
