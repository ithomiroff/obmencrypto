import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { antiforgeryHeaderTokenKey, antiforgeryTokenKey } from './constants';
import { CookieService } from 'ngx-cookie-service';
import { isPlatformServer, isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AddCsrfHeaderInterceptorService implements HttpInterceptor {

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const requestToken = this.cookieService.get(antiforgeryTokenKey);
    let headers = {};
    if (requestToken) {
      headers = {
        headers: req.headers.set(antiforgeryHeaderTokenKey, requestToken)
      };
    }
    return next.handle(req.clone(headers));
  }
  public constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private cookieService: CookieService
  ) {}
}
