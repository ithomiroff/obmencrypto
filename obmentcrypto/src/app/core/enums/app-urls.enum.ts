export enum EAppUrls {
  APPLICATION = 'order-info',
  CABINET = 'cabinet',
  USER = 'user',
  AUTH = 'auth',
  REG = 'registration',
  RESET = 'resetpassword',
}
