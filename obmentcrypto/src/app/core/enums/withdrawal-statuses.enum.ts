export enum EWithdrawalStatuses {
  PENDING = 'Pending',
  COMPLETED = 'Completed',
  DECLINED = 'Declined'
}
