export enum EStatusesApplication {
  NEW = 'New',
  PAID = 'Paid',
  CANCELED = 'CANCELED',
  SERVED = 'SERVED',
  PAY_PENDING = 'PayPending',
  WITHDRAW = 'WithdrawOut',
  COMPLETED = 'Completed',
  DECLINED = 'Declined',
  PENDING = 'Pending',
  HOLD = 'Hold',
  PAUSED = 'Paused'
}
