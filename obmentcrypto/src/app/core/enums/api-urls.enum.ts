export enum EApiUrls {

  GET_CURRENCY = '/get-currency',
  GIVE_CURRENCY = '/give-currency',
  GET_TEXT = '/get-text',
  CHECK_SESSION = '/session',

  SET_PARTNER = '/setpartner',
  INFO = '/info',

  WITHDRAWAL = '/withdrawal',

  AUTH = '/auth',

  REG = '/registration',

  GET_USER_INFO = '/user-info',

  ORDER = '/order',

  USER_PAID = '/user-paid',

  /*
  * Получение полей для заполнения
  * **/
  GET_FIELD_CURRENCY_EXCHANGE = '/field-exchange',

  /*
  * Получение валют для столбцов(отдаю, получаю)
  * **/
  GET_EXHANGE_CURRENCY = '/exchange-currency',

  /*
  * Перевод валют при вводе значений
  * **/
  TOOLBAT_CURRENCY = '/toolbar-currency',

  /*
  * Перевод валют при вводе значений
  * **/
  CALCULATE_CURRENCY_VALUE = '/calculate-currency-value',

  GET_CURRENCY_LIMITS = '/currency-limits',

  /*
  * Получить информацию по заявке
  * **/
  APPLICATION = '/application-info',

  /*
  * Обменять валюту
  * **/
  EXCHANGE = '/exchange-currency',

  /*
  * заявки пользователя
  * **/
  USER_APPLICATIONS = '/user-applications',

  /*
  * заявки пользователя
  * **/
  AFFILIATE = '/affiliate',


  /*
  * кнопка оплатить
  * **/
  BTN_PAID = '/i-paid-order',

  /*
  * кнопка отменить
  * **/
  DECLINE = '/decline',

  SET_PROMO = '/setpromo',

  ACCOUNT = '/account',

  FORGOT_PASS = '/forgotpassword',

  RESET_PASS = '/resetpassword',

  CHANGE_PASS = '/changepassword',

  CONFIRM_EMAIL = '/confirm-email',

  CREATE_WITHDRAWAL = '/partner/withdrawal',

  LOGOUT = '/logout',

  ANTIFORGERY = '/antiforgery',

  SEO = '/seo',

  GET_ARTICLE = '/article',

  GET_ALL_ARTICLES = '/articles',

  POST_NOT_ENOUGH_RESERVE = '/not-enough-reserve',

  WORKING_MODE = '/working-time-mode'
}
