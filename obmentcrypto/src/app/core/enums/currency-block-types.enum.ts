export enum ECurrencyBlockTypes {
  GIVE = 'GIVE',
  RECEIVE = 'RECEIVE'
}

export function GetReversedBlockType(mode: ECurrencyBlockTypes): ECurrencyBlockTypes {
  return mode === ECurrencyBlockTypes.GIVE ? ECurrencyBlockTypes.RECEIVE : ECurrencyBlockTypes.GIVE;
}

