export enum EEarningStatuses {
  NOT_PAID = 'NotPaid',
  PENDING = 'Pending',
  PAID = 'Paid'
}
