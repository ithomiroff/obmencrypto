export enum EFieldCurrencyType {
  PURSE = 'PURSE',
  CARD = 'CARD',
  BANKCARD = 'BankCard',
  WALLET = 'Wallet',
  PHONE = 'Phone',
  TEXT = 'Text',
  PAYMENT_ID = 'PaymentId',
}
