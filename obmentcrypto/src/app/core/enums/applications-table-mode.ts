export enum ApplicationsTableMode {
  UserApps = 'userApps',
  WithdrawalApps = 'withdrawalApps',
  PartnersApps = 'partnersApps'
}
