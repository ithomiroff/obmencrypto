export enum EFilters {
  ALL = 'ALL',
  COIN = 'COIN',
  RUB = 'RUB',
  USD = 'USD',
  EUR = 'EUR',
}
