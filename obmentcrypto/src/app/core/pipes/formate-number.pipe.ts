import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  space = ' ';

  transform(value: any): string {
    let resStr = '';
    if (value) {
      let arr = value.toString().split('.');
      if (arr.length > 0) {
        let fullDigit = arr[0];
        let reverse = fullDigit.split('').reverse();
        let res = [];
        reverse.forEach((symbol, i) => {
          let index = i + 1;
          res.push(symbol);
          if (index % 3 === 0) {
            res.push(this.space);
          }
        });
        if (res[res.length - 1] === this.space) {
          res.pop();
        }
        resStr = res.reverse().join('');
      }
    } else {
      if (value == 0) {
        resStr = '0';
      } else {
        resStr = '';
      }
    }
    return resStr;
  }


}
