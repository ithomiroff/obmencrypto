import { TestBed } from '@angular/core/testing';

import { CryptoValidatorService } from './crypto-validator.service';

describe('CryptoValidatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CryptoValidatorService = TestBed.get(CryptoValidatorService);
    expect(service).toBeTruthy();
  });
});
