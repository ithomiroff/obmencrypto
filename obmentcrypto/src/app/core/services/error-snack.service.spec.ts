import { TestBed } from '@angular/core/testing';

import { InfoSnackService } from './error-snack.service';

describe('ErrorSnackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfoSnackService = TestBed.get(InfoSnackService);
    expect(service).toBeTruthy();
  });
});
