import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {HttpErrorResponse} from '@angular/common/http';
import * as layoutSelectors from '@app/store/layout/layout.selectors';
import {IAppState} from '../../store/reducer';
import {select, Store} from '@ngrx/store';
import {filter, take, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InfoSnackService {

  isMobile: boolean;

  isMobile$ = this.store.pipe(select(layoutSelectors.isMobile));

  constructor(
    private snackBar: MatSnackBar,
    private store: Store<IAppState>
  ) {
    this.isMobile$.pipe(
      filter(v => !!v),
      take(1),
      tap((value) => this.isMobile = value)
    ).subscribe();
  }

  showError(error: HttpErrorResponse): void {
    const msg = this.parseHttpError(error);
    this.showInfoMsg(msg);
  }

  showInfoMsg(msg: string): void {
    this.snackBar.open(msg, 'OK', {
      duration: 4000,
      horizontalPosition: 'center',
      verticalPosition: this.isMobile ? 'top' : 'bottom'
    });
  }

  private parseHttpError(error: HttpErrorResponse): string {
    return error && error.error ? error.error.errorDescription : null;
  }
}
