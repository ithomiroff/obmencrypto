import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private readonly prefix = environment.prefix;

  constructor(
    private http: HttpClient,
  ) {
  }

  get(url: string, params?): Observable<any> {
    return this.http.get(this.prefix + url, {params});
  }

  post(url: string, body?: any): Observable<any> {
    return this.http.post(this.prefix + url, body);
  }


  postForm(url: string, json?: any): Observable<any> {
    const body = new HttpParams({
      fromObject: json
    });
    return this.http.post(this.prefix + url, body);
  }

  postFormData(url: string, formData: FormData): Observable<any> {
    return this.http.post(this.prefix + url, formData);
  }
}
