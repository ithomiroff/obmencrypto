import { TestBed } from '@angular/core/testing';

import { WorkingTimeService } from './working-time.service';

describe('WorkingTimeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkingTimeService = TestBed.get(WorkingTimeService);
    expect(service).toBeTruthy();
  });
});
