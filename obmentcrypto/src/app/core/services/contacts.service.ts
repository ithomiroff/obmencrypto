import { Contacts } from './../constants';
import { ContactType, IContact } from './../interfaces/contact-url.interface';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  contacts: IContact[] = Contacts;

  constructor(private domSanitizer: DomSanitizer) { }

  public GetContactUrl(type: ContactType) {
    switch (type) {
      case ContactType.Phone:
        const phone = this.contacts.find(c => c.type === ContactType.Phone).data;
        return this.transform(`tel:${phone}`);
      case ContactType.Telegram:
        const telegram = this.contacts.find(c => c.type === ContactType.Telegram).data;
        return this.transform(`tg://resolve?domain=${telegram}`);
      case ContactType.WhatsApp:
        const whatsapp = this.contacts.find(c => c.type === ContactType.WhatsApp).data;
        return this.transform(`https://api.whatsapp.com/send?phone=${whatsapp}`);
    }
  }
  private transform(url) {
    return url;
  }
}
