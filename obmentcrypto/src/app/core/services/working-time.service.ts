import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable, of } from 'rxjs';
import { EApiUrls } from '../enums/api-urls.enum';
import { map, catchError } from 'rxjs/operators';

export enum WorkingMode {
  On = 'On',
  NotWorkingTime = 'NotWorkingTime',
  Off = 'Off'
}

@Injectable({
  providedIn: 'root'
})
export class WorkingTimeService {

  constructor(private http: HttpService) { }

  public GetWorkingMode(): Observable<WorkingMode> {
    console.log('get working mode');
    return this.http.get(EApiUrls.WORKING_MODE)
    .pipe(
      map(res => {
        return res;
      }),
      catchError(error => of(WorkingMode.On))
    );
  }
}
