import {Injectable} from '@angular/core';

declare const WAValidator;

@Injectable({
  providedIn: 'root'
})
export class CryptoValidatorService {
  constructor() {}

  validate(walletNumber: string, type: string): boolean {
    type = type.toLowerCase();
    if (type === 'xmr') {
      return true;
    }
    if (type === 'usdt') {
      type = 'btc';
    }
    if (type === 'usdterc') {
      type = 'eth';
    }
    try {
      return WAValidator.validate(walletNumber, type);
    } catch (exc) {
      return true;
    }
  }
}
