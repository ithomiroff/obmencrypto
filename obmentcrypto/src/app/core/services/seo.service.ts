import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { EApiUrls } from '../enums/api-urls.enum';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

export interface ISeoData {
  title: string;
  description: string;
}


const seo = {
  default: {
    title: 'Обменник криптовалют | ObmenCrypto',
    description: ''
  }
};

@Injectable({
  providedIn: 'root'
})
export class SeoService {
  private prevUrl: string;
  private prevSeo: Observable<ISeoData> = of(seo.default);

  constructor(private http: HttpService) {
  }

  getDefaultSeo(): ISeoData { return; }
  loadSeoByUrl(url: string): Observable<ISeoData> {
    if (this.isMainPage(url)) {
      url = '/';
    }
    this.prevUrl = url;
    const postAsync = this.http.post(EApiUrls.SEO, { url })
      .pipe(
        map(res => res),
        catchError(error => of(seo.default))
      );
    return postAsync;
  }

  private isMainPage(url: string): boolean {
    const mainRegex = /^\/(\w+-\w+)?$/;
    return mainRegex.test(url);
  }
}
