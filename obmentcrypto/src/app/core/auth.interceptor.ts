import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EApiUrls } from './enums/api-urls.enum';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isPlatformBrowser(this.platformId)) {
      let req = request;
      const url = req.url.toString();
      const token = localStorage.getItem('cryptoToken');
      if (token) {
        req = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
          }
        });
      }
      if (
          url.indexOf(EApiUrls.AUTH) > 0
      ) {
        req = request.clone({
          setHeaders: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      }

      if (token && url.indexOf(EApiUrls.LOGOUT) > 0) {
        req = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      }
      return next.handle(req);
    }
    return next.handle(request);
  }

}
