import { ContactsService } from '../services/contacts.service';
import { ContactType } from '../interfaces/contact-url.interface';
import { Directive, Input, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Directive({
  selector: '[appContactLink]'
})
export class ContactLinkDirective implements OnInit {
  @Input()
  contactType: ContactType;

  constructor(
    private linkRef: ElementRef<Element>,
    private renderer: Renderer2,
    private contactsService: ContactsService,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    const safeTypeUrl = this.contactsService.GetContactUrl(this.contactType);
    this.renderer.setAttribute(this.linkRef.nativeElement, 'href', safeTypeUrl);
    this.renderer.setAttribute(this.linkRef.nativeElement, 'target', '_blank');
  }
}
