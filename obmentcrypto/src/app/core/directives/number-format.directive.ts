import {AfterViewInit, Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[appNumberFormat]'
})
export class NumberFormatDirective implements AfterViewInit {

  masked: string = '';
  unmasked: string = '';

  private unmaskedIntegerPart: string = '';

  @Input() digitSeparator: string = ' '; // разделитель разрядов в целой части числа
  @Input() decimalSeparator: string = ','; // разделитель для отделени целой части от дробной
  @Input() integerMaxLength = 20; // максимальная длина целой части
  @Input() decimalMaxLength = 10; // максимальная длина дробной части
  @Input() maxValue: number; // максимальное значение числа, по умолчанию неограниченно

  // количество символов в целой части числа, через которое будет вставляться разделитель digitSeparator
  @Input() divideByCount: number = 3;

  allowedKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace',
    'ArrowLeft', 'ArrowRight', 'Left', 'Right', '.', ','];

  constructor(
    private elRef: ElementRef,
    private renderer: Renderer2,
    private control: NgControl
  ) {
  }

  ngAfterViewInit(): void {
    this.setInitMaskedValue();
  }

  @HostListener('keydown', ['$event'])
  OnKeyDown(event: KeyboardEvent) {
    this.handleFirstZeroCase(event.key);

    if (!this.keyAllowed(event.key)) {
      event.preventDefault();
    }
  }

  @HostListener('input', ['$event'])
  OnInput(event: any) {
    // let input = event.target.value;
    // if (input[0] === '0') {
    //   input += ',';
    // }
    // this.processing(input);
    // this.writeValue();
  }

  @HostListener('blur', ['$event'])
  OnBlur() {
    if (this.masked.length > 0) {
      // if (this.masked.indexOf('.') !== -1) {
      //   this.masked = this.masked.replace('.', this.decimalSeparator);
      // }
      // if (this.unmasked.indexOf('.') !== -1) {
      //   const arr = this.unmasked.split('.');
      //   if (arr[1].length === 0) {
      //     this.unmasked += '00';
      //     this.masked += '00';
      //   } else {
      //     if (arr[1].length > 2) {
      //       let newValue = this.formatInsignificantZeros(arr[1]);
      //       this.unmasked = this.unmasked.replace(arr[1], newValue);
      //       this.masked = this.masked.replace(arr[1], newValue);
      //     }
      //   }
      // } else {
      //   this.unmasked += '.00';
      //   this.masked += this.decimalSeparator + '00';
      // }
      // if (+this.unmasked === 0) {
      //   this.unmasked = '';
      //   this.masked = '';
      // }
      // this.writeValue();
    }
  }

  @HostListener('paste', ['$event'])
  onPaste($event) {
    $event.preventDefault();
  }


  writeValue(): void {
    this.control.control.setValue(this.unmasked);
    this.renderer.setAttribute(this.elRef.nativeElement, 'value', this.masked);
  }

  separate(input: string): string {
    let res = '';
    if (this.divideByCount && this.divideByCount > 0) {
      let counter = 0;
      for (let i = input.length - 1; i > -1; i--) {
        if (counter !== this.divideByCount) {
          res = input.charAt(i) + res;
          counter++;
        } else {
          counter = 1;
          res = input.charAt(i) + this.digitSeparator + res;
        }
      }
    }
    return res;
  }

  processing(input: string) {
    let array: string[];
    let decimalSeparator;
    if (input && input != '') {
      if (input.indexOf(this.decimalSeparator) !== -1) {
        decimalSeparator = this.decimalSeparator;
        array = input.split(this.decimalSeparator);
      } else if (input.indexOf('.') !== -1) {
        decimalSeparator = '.';
        array = input.split('.');
      } else {
        array = [input];
      }
      let unmasked;
      let masked;
      array[0] = array[0].split(this.digitSeparator).join('');
      if (array[0].length < this.integerMaxLength) {
        unmasked = array[0];
        masked = this.separate(array[0]);
      } else {
        return;
      }
      if (array.length > 1) {
        array[1] = array[1].split(this.digitSeparator).join('');
        unmasked += '.' + array[1].substring(0, this.decimalMaxLength);
        masked += decimalSeparator + array[1].substring(0, this.decimalMaxLength);
      }
      if (!(this.maxValue && parseFloat(unmasked) > this.maxValue) && unmasked && masked) {
        this.unmasked = unmasked;
        this.masked = masked;
      }
    } else {
      this.unmasked = '';
      this.masked = '';
    }
  }


  keyAllowed(key: string): boolean {
    if (this.allowedKeys.indexOf(key) !== -1) {
      // if (this.unmasked.length === 0 && (key === this.decimalSeparator || key === '.')) {
      //   return false;
      // } else if (this.unmasked.length > 0 && (key === '.' || key === this.decimalSeparator) &&
      //   (this.unmasked.indexOf(this.decimalSeparator) !== -1 || this.unmasked.indexOf('.') !== -1)) {
      //   return false;
      // }
      return true;
    } else {
      return false;
    }
  }

  setInitMaskedValue(): void {
    if (this.control.control.value) {
      this.processing(this.control.control.value.toString());
      this.OnBlur();
      this.elRef.nativeElement.value = this.masked;
    }
  }

  handleFirstZeroCase(key: string): void {
    if (key === 'Backspace' && this.masked[0] === '0' && this.masked[1] === ',' && this.masked.length === 2) {
      this.masked = '';
      this.unmasked = '';
      this.writeValue();
    }
  }


}
