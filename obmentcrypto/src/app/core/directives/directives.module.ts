import { ContactLinkDirective } from './contact-link.directive';
import { NgModule } from '@angular/core';
import { NumberFormatDirective } from './number-format.directive';

@NgModule({
  declarations: [
    ContactLinkDirective,
    NumberFormatDirective
  ],
  imports: [
  ],
  exports: [
    ContactLinkDirective,
    NumberFormatDirective
  ]
})
export class DirectivesModule {
}
