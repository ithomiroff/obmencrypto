import { IContact, ContactType } from './interfaces/contact-url.interface';
import { IFilters } from '@app/core/interfaces/filters.interface';
import { EFilters } from '@app/core/enums/filters.enum';

export const FILTERS: IFilters[] = [
  {
    code: EFilters.ALL
  },
  {
    code: EFilters.COIN
  },
  {
    code: EFilters.RUB
  },
  {
    code: EFilters.USD
  },
  {
    code: EFilters.EUR
  }
];

export const antiforgeryTokenKey = 'X-REQUEST-XSRF-TOKEN';
export const antiforgeryHeaderTokenKey = 'X-XSRF-TOKEN';
/*
* Время обновления валют в миллесекундах
* */
export const timeToUpdateCurrency = 120000;

/*
* Время обновления расчета введенных валют в миллесекундах
* */
export const timeToUpdateCalculatedValues = 10000;

/*
* Время обновления расчета введенных валют в миллесекундах
* */
export const timeToUpdateOrder = 30000;


export const Contacts: IContact[] = [
  {
    data: '79313915763',
    type: ContactType.WhatsApp
  },
  {
    data: 'obmencryptocom',
    type: ContactType.Telegram
  },
  {
    data: '79313915763',
    type: ContactType.Phone
  }
]
