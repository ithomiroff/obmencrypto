import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatChipsModule, MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSelectModule, MatSidenavModule, MatSnackBarModule, MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

const modules = [
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatDividerModule,
  MatMenuModule,
  MatSelectModule,
  LayoutModule,
  MatIconModule,
  MatTabsModule,
  MatListModule,
  MatBottomSheetModule,
  MatTooltipModule,
  MatChipsModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatTableModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatDialogModule,
];

@NgModule({
  declarations: [],
  imports: modules,
  exports: modules
})
export class MaterialModule { }
