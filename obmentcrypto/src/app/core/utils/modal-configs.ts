import { IModalConfigInterface } from '@app/core/interfaces/modal-config.interface';
import { MatDialog } from '@angular/material';
import { ModalConfirmSuccessComponent } from '@app/components/modals/modal-confirm-success/modal-confirm-success.component';

const defaultConfig = (data: IModalConfigInterface) => {
  return {
    data,
    width: '360px',
    disableClose: true
  };
};

export const showDefaultModal = (dialog: MatDialog, data: IModalConfigInterface) => {
  return dialog.open(ModalConfirmSuccessComponent, defaultConfig(data)).afterClosed();
};

export const showDefaultErrorModal = (dialog: MatDialog, error: any) => {
  const msg = error && error.error ? error.error.errorDescription : 'Ошибка сервера';
  return showDefaultModal(this.dialog,
    {
      title: msg,
      message: ''
    });
};
