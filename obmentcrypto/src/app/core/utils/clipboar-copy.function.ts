declare const ClipboardJS;
export const copyToClipboard = (element: string) => {
  const clipboard = new ClipboardJS(element);
};
