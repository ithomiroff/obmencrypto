import { FormControl, FormGroup } from '@angular/forms';

export function MarkAsTouchedAllFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      control.markAsTouched({ onlySelf: true });
      MarkAsTouchedAllFields(control);
    }
  });
}

export function UpdateValidityAll(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      if (control.validator && control.validator.length) {
        control.updateValueAndValidity({ onlySelf: true });
      }
    } else if (control instanceof FormGroup) {
      control.updateValueAndValidity({ onlySelf: true });
      UpdateValidityAll(control);
    }
  });
}

export function clearErrors(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.updateValueAndValidity({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      clearErrors(control);
    }
  });
}
