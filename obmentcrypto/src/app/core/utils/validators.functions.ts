import { CryptoValidatorService } from './../services/crypto-validator.service';
import {AbstractControl, FormGroup} from '@angular/forms';
import { MarkAsTouchedAllFields } from './form-utils';
const cryptoValidatorService = new CryptoValidatorService();

export const minMaxValidator = (min: number, max: number) => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value && control.value < min) {
      return {minValue: true};
    }
    if (!!control.value && control.value > max) {
      return {maxValue: true};
    }
    return null;
  };
};

export const validateEmail = (value: string) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const result =  emailRegex.test(value);
  return result;
};

export const isTelegram = (value: string) => {
  const telegramRegex = /^@[\d\w_-]{5,}$/;
  const result = telegramRegex.test(value);
  return result;
};

export const validatePhone = (value: string) => {
  const phoneRegex = /^\d{11,}$/;
  const result =  phoneRegex.test(value);
  return result;
};

export const emailValidator = () => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const value = control.value;
      const checkEmail = validateEmail(value);
      return checkEmail ? null : { email: !checkEmail };
    }
  };
};

export const telegramValidator = () => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const value = control.value;
      const checkTelegram = isTelegram(value);
      return checkTelegram ? null : { telegram: !checkTelegram };
    }
  };
};

export const anyContactRequiredValidator = () => {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      const personInfo = control.value;
      const checkEmail = validateEmail(personInfo.email);
      const checkTelegram = isTelegram(personInfo.telegram);
      const checkPhone = validatePhone(personInfo.phone);
      if (checkEmail || checkTelegram || checkPhone) {
        setPersonDataError(control, null);
        return null;
      }
      const result: any = { anyContactRequired: true };

      if (personInfo.email && !!!checkEmail) {
        result.email = true;
      }
      if (personInfo.phone && !!!checkPhone) {
        result.phone = true;
      }
      if (personInfo.telegram && !!!checkTelegram) {
        result.telegram = true;
      }
      setPersonDataError(control, {error: true});

      return result;
    };
  };

export const possitiveNumberValidator = () => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const value = control.value as number;
      if (value > 0) {
        return null;
      }
    }
    return {nonPositiveNumber: true};
  };
};

export const emailOrTelegramValidator = () => {

  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const value = control.value;
      const atSign: number = value.indexOf('@');
      if (atSign < 0) {
        return { emailOrTelegram: true };
      } else if (atSign == 0) {
        const checkTelegram = isTelegram(value);
        return checkTelegram ? null : { telegram: !checkTelegram };
      } else {
        const checkEmail = validateEmail(value);
        return checkEmail ? null : { email: !checkEmail };
      }
      return null;
    }
  };
};


export const walletCryptoValidator = (currencyCode: string) => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const code = currencyCode;
      const cryptoWallet = cryptoValidatorService.validate(control.value, code);
      if (cryptoWallet) {
        return null;
      } else {
        return {cryptoWallet: !cryptoWallet};
      }
    }
    return null;
  };
};

export const walletYAMoneyValidator = () => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value && !control.value.startsWith('4100')) {
      return {yaMoneyWallet: true};
    }
    return null;
  };
};

const getAdvcSymbol = (currencyCode: string) => {
  switch (currencyCode.toUpperCase()) {
    case 'ADVCEUR':
      return 'E';
    case 'ADVCRUB':
      return 'R';
    case 'ADVCUSD':
      return 'U';
  }
};

export const walletADVCValidator = (currencyCode: string) => {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!!control.value) {
      const symbol = getAdvcSymbol(currencyCode);
      const regexStr = `[${symbol}]{1}\\s?\\d{4}\\s?\\d{4}\\s?\\d{4}`;
      //console.log(regexStr);
      const regex = new RegExp(regexStr);
      const result = regex.exec(control.value);
      //console.log(result);
      const valid = !!result;
      if (valid) {
        return null;
      } else {
        return {advCashWallet: true};
      }
    }
    return null;
  };
};
function setPersonDataError(control: AbstractControl, error: any) {
  control.get('email').setErrors(error);
  control.get('phone').setErrors(error);
  control.get('telegram').setErrors(error);
}

