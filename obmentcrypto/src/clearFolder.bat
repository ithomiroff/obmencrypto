@echo off
if %~1 == "" goto exit

set "d=%~1"
set "d1=shd_new"
set "d2=shd"
set "d3=shd_new-backup"
set "d4=sitemap.xml"

pushd "%d%"
  for /f "delims=" %%a in ('2^>nul dir /b^|findstr /xv /c:"%d1%" /c:"%d2%" /c:"%d3%" /c:"%d4%"') do >nul 2>&1 rd /s /q "%%a"& >nul 2>&1 del /q "%%a"
popd
pause

:exit
