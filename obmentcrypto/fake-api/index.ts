// @ts-ignore
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

// @ts-ignore
const app = express();
const router = express.Router();
app.use(morgan(morganHandle));
app.use(bodyParser.json());


// @ts-ignore
require('./routes')(router);
app.use('/api', router);


app.listen(3000, () => console.log('Server starts on port ' + 3000));


function morganHandle(tokens, req, res) {
  return [
    tokens.method(req, res),
    tokens.url(req, res),
  ];
}
