// @ts-ignore

module.exports = (router) => {
  router.get('/toolbar-currency', (req, res) => {
    res.send([
      {
        'currencyCode': 'BTC',
        'rublesValue': 47.333113,
        'currencyStatusPercentValue': 15,
        'currencyStatus': 'FALL',
      },
      {
        'currencyCode': 'LTC',
        'rublesValue': 47.333113,
        'currencyStatusPercentValue': 25,
        'currencyStatus': 'FALL',
      },
      {
        'currencyCode': 'ETH',
        'rublesValue': 47.333113,
        'currencyStatusPercentValue': 5,
        'currencyStatus': 'GROW',
      }
    ]);
  });

  router.get('/exchange-currency', (req, res) => {
    const {mode} = req.query;
    if (mode.toString() === 'GIVE') {
      setTimeout(() => {
        res.send([
          {
            id: 1,
            icon: 'http://obmencrypto.com/resources/btc.png',
            name: 'Bitcoin',
            currencyCode: 'BTN',
            currencyType: 'CRYPT',
            currencyField: 'PURSE',
            minValue: getRandomArbitrary(),
            maxValue: getRandomArbitrary()
          },
          {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            currencyField: 'CARD',
            minValue: getRandomArbitrary(),
            maxValue: getRandomArbitrary()
          },
          {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            minValue: getRandomArbitrary(),
            maxValue: getRandomArbitrary()
          },
        ]);
      }, 1000);
      return;
    } else {
      setTimeout(() => {
        res.send([
          {
            id: 1,
            icon: 'http://obmencrypto.com/resources/btc.png',
            name: 'Bitcoin',
            currencyCode: 'BTN',
            currencyType: 'CRYPT',
            reserve: getRandomArbitrary()
          },
          {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            reserve: getRandomArbitrary()
          },
          {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            reserve: getRandomArbitrary()
          }
        ]);
      }, 1200);
      return;
    }

  });

  router.get('/get-text', (req, res) => {
    res.send({
      topText: 'Здесь текст из админки 1',
      bottomText: 'Здесь текст из админки 2'

    });
  });

  /**
   @queryParams
   @mode - тип GIVE или RECEIVE
   @currencyId - id выбранной валюты
   **/
  router.get('/field-exchange', (req, res) => {
    const {currencyId, mode = ''} = req.query;
    if (mode.toString() == 'GIVE') {
      return res.send([
        {
          id: 1,
          title: 'Здесь тайтл GIVE',
          fieldType: 'wallet',
          icon: 'http://obmencrypto.com/resources/yamrub.png'
        }
      ]);
    } else {
      return res.send([
        {
          id: 2,
          title: 'Здесь тайтл RECEIVE 1',
          fieldType: 'bankcard',
          icon: 'http://obmencrypto.com/resources/yamrub.png'
        },
        {
          id: 4,
          title: 'Здесь тайтл RECEIVE 2',
          fieldType: 'text',
        }
      ]);
    }
  });


  /**
   @queryParams
   @mode - код валюты которую хотим перевести
   @giveCurrencyId - значение валюты которую хотим перевести
   @receiveCurrencyId - код валюты в которую хотим перевести
   @currencyValue - код валюты в которую хотим перевести
   **/
  router.get('/calculate-currency-value', (req, res) => {
    res.send({
      value: Math.random(),
      warnings: {
        give: 'Слишком большой объем',
        receive: 'Слишком большой объем'
      }
    });
  });

  /**
   @queryParams
   @key - ключ заявки
   **/
  router.get('/application-info', (req, res) => {
    const {key} = req.query;
    res.send({
      key,
      applicationNumber: 798273459293,
      status: 'SERVED',
      timeExecution: 1566431708,
      giveCurrency: {
        id: 2,
        icon: 'http://obmencrypto.com/resources/yamrub.png',
        name: 'Яндекс.Деньги',
        currencyCode: 'YA',
        currencyType: 'RUB',
        value: '100000'
      },
      receiveCurrency: {
        id: 3,
        icon: 'http://obmencrypto.com/resources/eth.png',
        name: 'Ethereum',
        currencyCode: 'ETH',
        currencyType: 'CRYPT',
        value: '0.4441'
      },
      applicationText: 'Здесь текст с бэкенда',
    });
  });

  router.post('/exchange-currency', (req, res) => {
    setTimeout(() => {
      res.send({
        key: '39582709345702'
      });
    }, 1500);
  });

  router.get('/user-applications', (req, res) => {
    setTimeout(() => {
      res.send([
        {
          date: '12.12.2016',
          giveCurrency: {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            value: 20000
          },
          receiveCurrency: {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            value: 0.3331
          },
          status: 'SERVED'

        },
        {
          date: '12.12.2016',
          giveCurrency: {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            value: 20000
          },
          receiveCurrency: {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            value: 0.3331
          },
          status: 'CANCELED'
        }
      ]);
    }, 1500);
  });

  router.get('/user-applications/affiliate', (req, res) => {
    setTimeout(() => {
      res.send([
        {
          date: '12.12.2016',
          giveCurrency: {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            value: 20000
          },
          receiveCurrency: {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            value: 0.3331
          },
          status: 'SERVED'

        },
        {
          date: '12.12.2016',
          giveCurrency: {
            id: 2,
            icon: 'http://obmencrypto.com/resources/yamrub.png',
            name: 'Яндекс.Деньги',
            currencyCode: 'YA',
            currencyType: 'RUB',
            value: 20000
          },
          receiveCurrency: {
            id: 3,
            icon: 'http://obmencrypto.com/resources/eth.png',
            name: 'Ethereum',
            currencyCode: 'ETH',
            currencyType: 'CRYPT',
            value: 0.3331
          },
          status: 'CANCELED'
        }
      ]);
    }, 1500);
  });

  router.get('/info/affiliate', (req, res) => {
    setTimeout(() => {
      res.send({
        personLink: 'http://obmencrypto.com/setpartner/Ke9HSVb1EEOvRXWG7Pw1FQ',
        availableAffiliate: {
          value: 0.23331,
          currencyCode: 'BTC',
        }
      });
    }, 1500);
  });

  router.get('/user-applications/withdrawal', (req, res) => {
    setTimeout(() => {
      res.send([
        {
          date: '04.12.2018',
          purse: '1204128491092-0192-01',
          total: '2000',
          status: 'SERVED'
        }
      ]);
    }, 1500);
  });

  router.post('/auth', (req, res) => {
    setTimeout(() => {
      res.send({
        username: 'test',
        email: 'test@mail.ru',
        token: 'aksjdhhgjqgdj2g1u23t812t3ghjgjhgh'
      });
    }, 1000);
  });

  router.get('/user-info', (req, res) => {
    setTimeout(() => {
      res.send({
        username: 'test',
        email: 'test@mail.ru',
        token: 'aksjdhhgjqgdj2g1u23t812t3ghjgjhgh'
      });
    }, 1000);
  });

  router.post('/registration', (req, res) => {
    setTimeout(() => {
      res.send({
        status: 'success'
      });
    }, 1000);
  });


};

function getRandomArbitrary() {
  return (Math.random() * (5000 - 0.33) + 0.33).toFixed(2);
}
