import 'zone.js/dist/zone-node';
import {enableProdMode} from '@angular/core';
// Express Engine
import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';

import * as express from 'express';
import {join} from 'path';
import * as proxy from 'http-proxy-middleware';

const fs = require('fs');

interface IConfig {
  enableProxy: boolean;
  apiProxyUrl: string;
}


// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();
const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist/browser');
const rawdata = fs.readFileSync(join(DIST_FOLDER, 'assets/config/config.json'));
const config: IConfig = JSON.parse(rawdata);

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', (_, options, callback) => {
  return ngExpressEngine({
    bootstrap: AppServerModuleNgFactory,
    providers: [
      provideModuleMap(LAZY_MODULE_MAP)
    ]
  })(_, options, callback);
});

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

if (config.enableProxy) {
  console.log(`ApiProxyUrl: ${config.apiProxyUrl}`)
  const apiProxy = proxy('/api', {
    target: config.apiProxyUrl,
    secure: false,
    logLevel: 'debug',
    changeOrigin: true,
    pathRewrite: {
      '^/api': ''
    }
  });
  app.use('/api', apiProxy);
}
// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => {

//   // let newUrl = 'https://obmencrypto.com/api';
//   console.log('api request!!');
//  });
// Serve static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req , res});
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});
